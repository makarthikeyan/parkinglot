package com.titaniamlabs.log;

import org.testng.annotations.Test;

import java.io.Serializable;

import static org.testng.Assert.assertNotNull;

public class LogTest {

    @Test
    public void testSimple() {
        Log log = Log.getLogger(LogTest.class);

        Serializable tag = log.traceEntry("testSimple");
        log.traceExit(tag);
    }

    @Test
    public void testDirectLog() {

        DirectFileLog fileLog = new DirectFileLog();
        //fileLog.debug("test msg");
        assertNotNull(fileLog);
    }
}
