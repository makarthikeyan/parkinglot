package com.titaniamlabs.crypto;

import org.elasticsearch.common.inject.Guice;
import org.elasticsearch.common.inject.Injector;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class NativeCryptoEngineJNITest {

    private CryptoEngine nativeCryptoEngine;

    @BeforeSuite
    public void setup() {
        Injector injector = Guice.createInjector(new CryptoModule());
        nativeCryptoEngine = injector.getInstance(CryptoEngine.class);
    }

    @Test
    public void testSimpleJNICall() throws Exception {
        CryptoResult result = nativeCryptoEngine.encrypt("SSN");
        //TODO: ensure this is the right expected value
        assertEquals(result.getForward(), "SSSSSSSSSSSSNNNNNN");
        assertEquals(result.getShuffled(), "TTTTTTTTTTTTOOOOOO");

//        String decrypted = nativeCryptoEngine.decrypt(result.getForward(), false);
//        assertEquals(decrypted, "SSN");
    }

}
