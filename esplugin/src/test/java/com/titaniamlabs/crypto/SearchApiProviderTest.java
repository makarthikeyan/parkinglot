package com.titaniamlabs.crypto;

import com.titaniamlabs.engine.api.core.TitaniamApi;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertNotNull;

public class SearchApiProviderTest {

    @Test
    public void testSimple() {

        SearchApiProvider provider = new SearchApiProvider();
        provider.setTitaniamApi(mock(TitaniamApi.class));

        assertNotNull(provider.get());
    }
}
