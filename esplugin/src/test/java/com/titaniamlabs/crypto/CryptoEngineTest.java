package com.titaniamlabs.crypto;

import net.bytebuddy.implementation.bytecode.Throw;
import org.mockito.Mock;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CryptoEngineTest {

    @Test
    public void testShuffle() {

        MockCryptoEngine engine  = new MockCryptoEngine();

        assertEquals(engine.shuffle("abc"), "bcd");
        assertEquals(engine.shuffle("012"), "123");
        assertEquals(engine.shuffle("z-b"), "{.c");
        assertEquals(engine.shuffle("???"), "@@@");

        assertEquals(engine.unshuffle("bcd"), "abc");
        assertEquals(engine.unshuffle("123"), "012");
        assertEquals(engine.unshuffle("{.c"), "z-b");
        assertEquals(engine.unshuffle("@@@"), "???");
    }

    @Test
    public void testCryptoExceptions() {

        CryptoException ex = new CryptoException();
        ex = new CryptoException("test msg", new Throwable());
        assertEquals(ex.getMessage(),"test msg");

        ex = new CryptoException("test 2 msg", new Throwable(), true, true);
        assertEquals(ex.getMessage(), "test 2 msg");
    }
}
