package com.titaniamlabs.crypto;

import com.titaniamlabs.api.ISearchApi;
import com.titaniamlabs.api.SearchType;
import org.elasticsearch.common.inject.AbstractModule;
import org.elasticsearch.common.inject.Guice;
import org.elasticsearch.common.inject.Injector;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

public class NativeCryptoEngineTest {

    private NativeCryptoEngine nativeCryptoEngine;
    private ISearchApi mockSearchApi;

    @BeforeSuite
    public void setup() {

        mockSearchApi = mock(ISearchApi.class);

        AbstractModule testModule = new AbstractModule() {

            @Override
            protected void configure() {
                bind(CryptoEngine.class).to(NativeCryptoEngine.class);
                bind(ISearchApi.class).toInstance(mockSearchApi);
            }
        };

        Injector injector = Guice.createInjector(testModule);
        nativeCryptoEngine = (NativeCryptoEngine) injector.getInstance(CryptoEngine.class);
    }

    @Test
    public void testEncyptBasic() throws Exception {

        when(mockSearchApi.entangle(anyString(),anyBoolean(),anyBoolean())).thenReturn(new String[]{"","shuffled:SSN",""});

        CryptoResult result = nativeCryptoEngine.encrypt("SSN");
        assertEquals(result.getShuffled(), "shuffled:SSN");

        result = nativeCryptoEngine.encrypt(null);
        assertEquals(result.getShuffled(), "");

        result = nativeCryptoEngine.encrypt("");
        assertEquals(result.getShuffled(), "");
    }

    @Test(expectedExceptions = CryptoException.class)
    public void testSearchReturnsSingleValuedArray() throws Exception {
        when(mockSearchApi.entangle(anyString(),anyBoolean(),anyBoolean())).thenReturn(new String[]{"one value"});
        nativeCryptoEngine.encrypt("SSN");
    }

    @Test(expectedExceptions = CryptoException.class)
    public void testSearchReturnsNullArray() throws Exception {
        when(mockSearchApi.entangle(anyString(),anyBoolean(),anyBoolean())).thenReturn(null);
        nativeCryptoEngine.encrypt("SSN");
    }

    @Test(expectedExceptions = CryptoException.class)
    public void testSearchThrowsException() throws Exception {
        when(mockSearchApi.entangle(anyString(),anyBoolean(),anyBoolean())).thenThrow(new NullPointerException("testSearchThrowsException"));
        nativeCryptoEngine.encrypt("SSN");
    }

    @Test
    public void testDecrypt() throws Exception {
        when(mockSearchApi.untangle("SSN", false)).thenReturn("NSS");
        String result = nativeCryptoEngine.decrypt("SSN", false);
        assertEquals(result, "NSS");
    }

    @Test(expectedExceptions = CryptoException.class)
    public void testDecryptNegative() throws Exception {
        when(mockSearchApi.untangle("failure", true)).thenThrow(new IllegalArgumentException("testing"));
        nativeCryptoEngine.decrypt("failure", true);
    }

    @Test
    public void testEncryptFor1toNPositions() throws Exception {
        when(mockSearchApi.getWildCardSearchTerms("*ssn*", SearchType.Contains)).thenReturn(new String[]{"a","b"});
        String[] result = nativeCryptoEngine.encryptFor1toNPositions("*ssn*",WildCardSearchType.CONTAINS,2);
        assertEquals(result.length, 2);
        assertEquals(result[0],"a");
        assertEquals(result[1],"b");
    }

    @Test(expectedExceptions = CryptoException.class)
    public void testEncryptFor1toNPositionsFailure() throws Exception {
        when(mockSearchApi.getWildCardSearchTerms("*failure*", SearchType.Contains)).thenThrow(new Exception("testing"));
        nativeCryptoEngine.encryptFor1toNPositions("*failure*",WildCardSearchType.CONTAINS,2);
    }

    @Test
    public void testSearchTypeMapping() {
        assertEquals(nativeCryptoEngine.mapSearchType(WildCardSearchType.CONTAINS), SearchType.Contains);
        assertEquals(nativeCryptoEngine.mapSearchType(WildCardSearchType.ENDS_WITH), SearchType.EndsWith);
        assertEquals(nativeCryptoEngine.mapSearchType(WildCardSearchType.STARTS_WITH), SearchType.StartsWith);
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void testSearchTypeMappingFailure() {
        assertEquals(nativeCryptoEngine.mapSearchType(WildCardSearchType.TERM), SearchType.Contains);
    }

    @Test
    public void testCryptoResult() {
        CryptoResult result = new CryptoResult("aaaaaa","bbbbbb","cccccc");
        assertNotNull(result.toString());
        assertTrue(result.toString().contains("aaaaaa"));
        assertTrue(result.toString().contains("bbbbbb"));
        assertTrue(result.toString().contains("cccccc"));
    }

}
