package com.titaniamlabs.es.mapper;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.analysis.NamedAnalyzer;
import org.elasticsearch.index.query.QueryShardContext;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.*;

public class IndexFragmentFieldTypeTest {

    @Test
    public void testClone() {

        //_0
        IndexFragmentFieldType type = new IndexFragmentFieldType("name", "name_0", 6, 18);
        type.setTokenized(false);

        IndexFragmentFieldType cloned = type.clone();

        assertFalse(cloned.tokenized());
        assertNull(cloned.indexAnalyzer());

        assertTrue(type.equals(cloned));
        assertEquals(type.hashCode(), cloned.hashCode());

        //_1
        type = new IndexFragmentFieldType("name", "name_1", 6, 18);
        type.setTokenized(true);
        type.setIndexAnalyzer(mock(NamedAnalyzer.class));

        cloned = type.clone();
        assertTrue(cloned.tokenized());
        assertNotNull(cloned.indexAnalyzer());
    }

    @Test
    public void testXContent() throws Exception {
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();

        IndexFragmentFieldType type = new IndexFragmentFieldType("name", "name_0", 6, 18);
        type.doXContent(builder);

        builder.endObject();

        assertEquals(builder.string(), "{\"index_prefixes\":{\"min_chars\":6,\"max_chars\":18}}");
    }

    @Test
    public void testSimple() {
        IndexFragmentFieldType type = new IndexFragmentFieldType("name", "name_0", 6, 18);
        assertNotNull(type.toString());

    }

    @Test
    public void testTermQuery() {
        IndexFragmentFieldType type = new IndexFragmentFieldType("name", "name_0", 6, 18);
        assertNotNull(type.termQuery("408", mock(QueryShardContext.class)));
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testUnsupported() {
        IndexFragmentFieldType type = new IndexFragmentFieldType("name", "name_0", 6, 18);
        type.existsQuery(mock(QueryShardContext.class));
    }
}
