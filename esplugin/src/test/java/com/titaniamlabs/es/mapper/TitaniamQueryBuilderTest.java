package com.titaniamlabs.es.mapper;

import com.titaniamlabs.crypto.MockCryptoEngine;
import com.titaniamlabs.crypto.WildCardSearchType;
import org.apache.lucene.search.*;
import org.apache.lucene.util.BytesRef;
import org.elasticsearch.index.query.QueryShardContext;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static com.titaniamlabs.es.mapper.MocksCreator.mockFragmentMapper;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.testng.Assert.*;

public class TitaniamQueryBuilderTest {

    private TitaniamQueryBuilder queryBuilder;
    private Map<String, IndexFragmentFieldMapper> fragments;
    private QueryShardContext mockShardContext;

    @BeforeMethod
    public void setup() {

        fragments = new HashMap<>();

        fragments.put("ssn_0", mockFragmentMapper("ssn_0"));
        fragments.put("ssn_1", mockFragmentMapper("ssn_1"));
        fragments.put("ssn_2", mockFragmentMapper("ssn_2"));
        fragments.put("ssn_3", mockFragmentMapper("ssn_3"));
        fragments.put("ssn_rev", mockFragmentMapper("ssn_rev"));

        mockShardContext = mock(QueryShardContext.class);

        this.queryBuilder = new TitaniamQueryBuilder(
                fragments,
                null,
                18,
                32
        );

        this.queryBuilder.setCryptoEngine(new MockCryptoEngine());
    }

    @Test
    public void testSimpleTermQuery() {

        BytesRef queryValue = new BytesRef("monkey");

//        TermQuery query1 = this.queryBuilder.termQuery("name", queryValue, mockShardContext);
//
//        assertEquals(query1.getTerm().text(), "mmmmmmoooooonnnnnnkkkkkkeeeeeeyyyyyy");
    }

    @Test
    public void testTermQueryNegative() {

        queryBuilder.termQuery("name",2, mockShardContext);
        queryBuilder.termQuery( "name", null, mockShardContext);
        queryBuilder.termQuery("name", new BytesRef("monkey"), null);
        queryBuilder.termQuery("", new BytesRef("monkey"), null);
        queryBuilder.termQuery(null, new BytesRef("monkey"), null);
    }

    @Test
    public void testPrefixQuery() {

        Query query = this.queryBuilder.prefixQuery(
                "ssn_0",
                "111-222-3333",
                mock(MultiTermQuery.RewriteMethod.class),
                mockShardContext);
        assertNotNull(query);

        query = this.queryBuilder.prefixQuery(
                "ssn_rev",
                "111-222-3333",
                mock(MultiTermQuery.RewriteMethod.class),
                mockShardContext);
        assertNotNull(query);

        query = this.queryBuilder.prefixQuery(
                "ssn_1",
                "111-222-3333",
                mock(MultiTermQuery.RewriteMethod.class),
                mockShardContext);
        assertNotNull(query);
        Mockito.verify(fragments.get("ssn_1").fieldType(), times(1)).fragmentedPrefixQuery(any(), any(), any());
    }

    @Test
    public void testWildcardPrefixQuery() {

        Query query = this.queryBuilder.wildCardQuery(
                "ssn",
                "123*",
                mock(MultiTermQuery.RewriteMethod.class),
                mockShardContext
                );

        assertNotNull(query);
        assertTrue(query instanceof PrefixQuery);
        PrefixQuery prefixQuery = (PrefixQuery) query;
        assertEquals(prefixQuery.getPrefix().text(), "111111222222333333");
        assertEquals(prefixQuery.getPrefix().field(), "ssn_0");

    }

    @Test
    public void testWildcardSuffixQuery() {

        Query query = this.queryBuilder.wildCardQuery(
                "ssn",
                "*123",
                mock(MultiTermQuery.RewriteMethod.class),
                mockShardContext
                );

        assertNotNull(query);
        assertTrue(query instanceof PrefixQuery);
        PrefixQuery prefixQuery = (PrefixQuery) query;
        assertEquals(prefixQuery.getPrefix().text(), "333333222222111111");
        assertEquals(prefixQuery.getPrefix().field(), "ssn_rev");

    }

    @Test
    public void testParseSearchType() {
        assertEquals(queryBuilder.parseSearchType("*a*"), WildCardSearchType.CONTAINS);
        assertEquals(queryBuilder.parseSearchType("*a"), WildCardSearchType.ENDS_WITH);
        assertEquals(queryBuilder.parseSearchType("a*"), WildCardSearchType.STARTS_WITH);
        assertEquals(queryBuilder.parseSearchType(null), WildCardSearchType.TERM);
        assertEquals(queryBuilder.parseSearchType("a"), WildCardSearchType.TERM);
    }

    @Test
    public void testWildcardFragmentQuerySimple() {

        TermQuery query = (TermQuery) this.queryBuilder.generateSplitQueryOnFragmentIndex("ssn", "222222333333", 1);
        assertNotNull(query);
        assertEquals(query.getTerm().field(), "ssn_1");
    }

    @Test
    public void testWildcardFragmentQueryBoolean() {

        BooleanQuery query = (BooleanQuery) this.queryBuilder.generateSplitQueryOnFragmentIndex(
                "ssn",
                "222222333333444444555555666666",
                2);

        assertNotNull(query);
        assertEquals(query.clauses().size(), 2);

        TermQuery termQuery1 = (TermQuery) query.clauses().get(0).getQuery();
        assertNotNull(termQuery1);
        assertEquals(termQuery1.getTerm().field(), "ssn_2");
        assertEquals(termQuery1.getTerm().text(), "222222333333444444");

        assertEquals(query.clauses().get(0).getOccur(), BooleanClause.Occur.MUST);

        TermQuery termQuery2 = (TermQuery) query.clauses().get(1).getQuery();
        assertNotNull(termQuery2);
        assertEquals(termQuery2.getTerm().field(), "ssn_5");
        assertEquals(termQuery2.getTerm().text(), "555555666666");

    }

    @Test
    public void testWildCardGenerateQuerySimple() {

        BooleanQuery booleanQuery = this.queryBuilder.generateWildCardQuery("ssn",
                new String[]{"222222333333444444","666666777777888888"}
                );

        assertNotNull(booleanQuery);
        assertEquals(booleanQuery.clauses().size(), 2);

        TermQuery termQuery1 = (TermQuery)booleanQuery.clauses().get(0).getQuery();
        TermQuery termQuery2 = (TermQuery)booleanQuery.clauses().get(1).getQuery();

        assertNotNull(termQuery1);
        assertNotNull(termQuery2);

        assertEquals(termQuery1.getTerm().field(), "ssn_1");
        assertEquals(termQuery1.getTerm().text(), "222222333333444444");

        assertEquals(booleanQuery.clauses().get(0).getOccur(), BooleanClause.Occur.SHOULD);

        assertEquals(termQuery2.getTerm().field(), "ssn_2");
        assertEquals(termQuery2.getTerm().text(), "666666777777888888");
    }

    @Test
    public void testWildCardGenerateQueryMultiple() {

        BooleanQuery booleanQuery = this.queryBuilder.generateWildCardQuery("ssn",
                new String[]{"111111222222333333444444555555","aaaaaabbbbbbccccccddddddeeeeee"}
                );

        assertNotNull(booleanQuery);
        assertEquals(booleanQuery.clauses().size(), 2);

        BooleanQuery subQuery1 = (BooleanQuery)booleanQuery.clauses().get(0).getQuery();
        BooleanQuery subQuery2 = (BooleanQuery)booleanQuery.clauses().get(1).getQuery();

        assertNotNull(subQuery1);
        assertNotNull(subQuery2);

        assertEquals(booleanQuery.clauses().get(0).getOccur(), BooleanClause.Occur.SHOULD);
    }

    @Test
    public void testWildCardQuery() throws Exception {

        BooleanQuery query = (BooleanQuery) this.queryBuilder.wildCardQuery("ssn",
                "*666*",
                mock(MultiTermQuery.RewriteMethod.class),
                mockShardContext);
        assertNotNull(query);

        assertEquals(query.clauses().size(), 32);
    }
}
