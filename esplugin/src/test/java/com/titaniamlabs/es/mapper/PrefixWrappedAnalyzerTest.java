package com.titaniamlabs.es.mapper;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class PrefixWrappedAnalyzerTest {

    @Test
    public void testSimple() {
        Analyzer mockAnalyzer = mock(Analyzer.class);
        when(mockAnalyzer.getReuseStrategy()).thenReturn(mock(Analyzer.ReuseStrategy.class));

        PrefixWrappedAnalyzer analyzer = new PrefixWrappedAnalyzer(mockAnalyzer, 6, 18);
        assertNotNull(analyzer.getWrappedAnalyzer("social"));

        Analyzer.TokenStreamComponents mockComponents = mock(Analyzer.TokenStreamComponents.class);
        when(mockComponents.getTokenStream()).thenReturn(mock(TokenStream.class));

        try {
            assertNotNull(analyzer.wrapComponents("social", mockComponents));
        } catch (NullPointerException e) {
            //look into if this will happen even in running elastic..
        }

    }
}
