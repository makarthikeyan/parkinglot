package com.titaniamlabs.es.mapper;

import com.titaniamlabs.util.MapperUtils;
import org.apache.lucene.index.IndexOptions;
import org.elasticsearch.Version;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.analysis.NamedAnalyzer;
import org.elasticsearch.index.mapper.ContentPath;
import org.elasticsearch.index.mapper.Mapper;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;


public class CryptoFieldMapperBuilderTest {

    public static final int MAX_INDEX_FRAGMENTS = 100;
    private CryptoFieldMapperBuilder builder;
    private NamedAnalyzer mockAnalyzer;

    @BeforeSuite
    public void setUp() {
        builder = new CryptoFieldMapperBuilder("id", Defaults.FIELD_TYPE, Defaults.FIELD_TYPE);
    }

    @Test
    public void testDoValidateMapping() {

        CryptoFieldType mockFieldType = mock(CryptoFieldType.class);
        when(mockFieldType.clone()).thenReturn(mockFieldType);
        when(mockFieldType.isSearchable()).thenReturn(Boolean.TRUE);

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);

        Mapper.BuilderContext mockContext = mock(Mapper.BuilderContext.class);
        when(mockContext.indexCreatedVersion()).thenReturn(Version.V_6_2_2);

        builder.doValidateMapping();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testDoValidateMappingFailure() {

        CryptoFieldType mockFieldType = mock(CryptoFieldType.class);
        when(mockFieldType.clone()).thenReturn(mockFieldType);
        when(mockFieldType.isSearchable()).thenReturn(Boolean.FALSE);

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);

        Mapper.BuilderContext mockContext = mock(Mapper.BuilderContext.class);
        when(mockContext.indexCreatedVersion()).thenReturn(Version.V_6_2_2);

        builder.doValidateMapping();
    }

    @Test
    public void testBuildFragmentMapper() {

        CryptoFieldType mockFieldType = createMockFieldType();

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);

        IndexFragmentFieldMapper fragmentFieldMapper = builder.buildFragmentMapper(0, "id","id_11", createMockContext());
        assertNotNull(fragmentFieldMapper);
        assertEquals(fragmentFieldMapper.fieldType().getClass(), IndexFragmentFieldType.class);
    }

    @Test
    public void testInitializeFragments() {

        CryptoFieldType mockFieldType = createMockFieldType();

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);
        builder.setMaxIndexFragments(MAX_INDEX_FRAGMENTS);

        Map<String, IndexFragmentFieldMapper> fragments = builder.buildFragmentMappers(createMockContext());
        assertEquals(fragments.size(), MAX_INDEX_FRAGMENTS +1 );
    }

    public CryptoFieldType createMockFieldType() {

        CryptoFieldType mockFieldType = mock(CryptoFieldType.class);
        when(mockFieldType.clone()).thenReturn(mockFieldType);
        when(mockFieldType.isSearchable()).thenReturn(Boolean.TRUE);
        when(mockFieldType.indexOptions()).thenReturn(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);

        mockAnalyzer = mock(NamedAnalyzer.class);
        when(mockAnalyzer.analyzer()).thenReturn(mockAnalyzer);

        when(mockFieldType.indexAnalyzer()).thenReturn(mockAnalyzer);

        return mockFieldType;
    }

    public Mapper.BuilderContext createMockContext() {

        ContentPath mockPath = mock(ContentPath.class);
        when(mockPath.pathAsText(anyString())).thenReturn("mock/path");

        Settings mockSettings = MocksCreator.mockSettings();
//        Settings mockSettings = mock(Settings.class);
//        when(mockSettings.get(any())).thenReturn("0","1","2","3","4");
//        when(mockSettings.get(anyString(), anyString())).thenReturn("6");

        Mapper.BuilderContext mockContext = mock(Mapper.BuilderContext.class);
        when(mockContext.indexCreatedVersion()).thenReturn(Version.V_6_2_2);
        when(mockContext.indexSettings()).thenReturn(mockSettings);
        when(mockContext.path()).thenReturn(mockPath);

        return mockContext;
    }

    @Test
    public void testBuild() {

        CryptoFieldType mockFieldType = createMockFieldType();

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);
        builder.setMaxIndexFragments(MAX_INDEX_FRAGMENTS);
        builder.indexPrefixes(12,24);

        CryptoFieldMapper mapper = builder.build(createMockContext());
        assertNotNull(mapper);
        assertEquals(mapper.getMinPrefixChars(), 12);
        assertEquals(mapper.getMaxPrefixChars(), 24);
        assertEquals(mapper.getMaxIndexFragments(), MAX_INDEX_FRAGMENTS);

        Map<String, IndexFragmentFieldMapper> fragments = mapper.getFragmentFields();

        for(int i=0; i<mapper.getMaxIndexFragments(); i++) {
            String fieldName = MapperUtils.generateIndexFragmentName("id", String.valueOf(i));
            assertTrue(fragments.containsKey(fieldName));

            //forward/reverse fields should not be tokenized.
            //should have no analyzers set.
            if(MapperUtils.isForwardOrReverseName(fieldName)) {
                assertFalse(fragments.get(fieldName).fieldType().tokenized());
                assertNull(fragments.get(fieldName).fieldType().indexAnalyzer());
            } else {
                assertTrue(fragments.get(fieldName).fieldType().tokenized());
                assertNotNull(fragments.get(fieldName).fieldType().indexAnalyzer());
            }
        }

        assertFalse(fragments.get("id_rev").fieldType().tokenized());
        assertNull(fragments.get("id_rev").fieldType().indexAnalyzer());
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testBuildInvalidPrefixLengths() {

        CryptoFieldType mockFieldType = createMockFieldType();

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);
        builder.setMaxIndexFragments(MAX_INDEX_FRAGMENTS);
        builder.indexPrefixes(12,1000);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testInvalidIndexPrefixes() {

        CryptoFieldType mockFieldType = createMockFieldType();

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);
        builder.indexPrefixes(-1,-1);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testValidate() {

        CryptoFieldType mockFieldType = createMockFieldType();

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);
        builder.doValidate();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testValidateIndexPrefixes() {

        CryptoFieldType mockFieldType = createMockFieldType();

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);
        builder.doValidateIndexPrefix();
    }

    @Test
    public void testSimple() {

        CryptoFieldType mockFieldType = createMockFieldType();

        builder = new CryptoFieldMapperBuilder("id", mockFieldType, mockFieldType);

        builder.positionIncrementGap(10);
        builder.fielddata(false);
        builder.indexPhrases(true);
        builder.docValues(false);
        builder.eagerGlobalOrdinals(true);
        builder.fielddataFrequencyFilter(1,2,10);


    }

}
