package com.titaniamlabs.es.mapper;

import org.elasticsearch.Version;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.common.settings.Settings;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class MocksCreator {

    public static Settings mockSettings() {

        Settings mockSettings = mock(Settings.class);
        when(mockSettings.get(any())).thenReturn("0","1","2","3","4");
        when(mockSettings.get(anyString(), anyString())).thenReturn("6");
        when(mockSettings.getAsVersion(IndexMetaData.SETTING_VERSION_CREATED, null)).thenReturn(Version.V_6_2_2);

        return mockSettings;
    }

    public static IndexFragmentFieldMapper mockFragmentMapper(String name) {

        IndexFragmentFieldType mockFragmentType = spy(new IndexFragmentFieldType(name, name, 6, 18));

        IndexFragmentFieldMapper mockFragmentMapper = mock(IndexFragmentFieldMapper.class);


        when(mockFragmentMapper.name()).thenReturn(name);
        when(mockFragmentMapper.fieldType()).thenReturn( mockFragmentType );

        return mockFragmentMapper;
    }
}
