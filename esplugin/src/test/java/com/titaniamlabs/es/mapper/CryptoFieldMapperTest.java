package com.titaniamlabs.es.mapper;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.crypto.MockCryptoEngine;
import com.titaniamlabs.util.MapperUtils;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexableField;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.index.mapper.FieldMapper;
import org.elasticsearch.index.mapper.ParseContext;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class CryptoFieldMapperTest {

    private CryptoFieldMapper mapper;
    private Settings mockSettings;
    private Map<String, IndexFragmentFieldMapper> fragmentMappers;
    private Map<String, IndexFragmentFieldMapper> fragmentMappersSpy;
    private FieldMapper.MultiFields mockMultiFields;
    private FieldMapper.CopyTo mockCopyTo;
    private CryptoFieldType mockFieldType;

    @BeforeMethod
    public void setUp() {

        AbstractModule testModule = new AbstractModule() {

            @Override
            protected void configure() {
                bind(CryptoEngine.class).to(MockCryptoEngine.class);
            }
        };

        Injector injector = Guice.createInjector(testModule);

        mockSettings = MocksCreator.mockSettings();
        fragmentMappers = new HashMap<>();
        fragmentMappersSpy = spy(fragmentMappers);
        mockMultiFields = mock(FieldMapper.MultiFields.class);
        mockCopyTo = mock(FieldMapper.CopyTo.class);
        mockFieldType = mock(CryptoFieldType.class);

        mapper = new CryptoFieldMapper("id",
                mockFieldType,
                mockFieldType,
                100,
                6,
                18,
                fragmentMappersSpy,
                mockSettings,
                mockMultiFields,
                1000,
                mockCopyTo
        );

        mapper.setCryptoEngine(injector.getInstance(CryptoEngine.class));
    }

    @Test
    public void testSimple() {
        assertEquals(mapper.contentType(), CryptoFieldType.TYPE_NAME);
    }

    @Test
    public void testDoValidateConfiguration() throws Exception {

        boolean valid = mapper.doValidateConfiguration(null);
        assertFalse(valid);

        when(mockFieldType.indexOptions()).thenReturn(IndexOptions.NONE);
        when(mockFieldType.stored()).thenReturn(Boolean.FALSE);

        valid = mapper.doValidateConfiguration("monkey");
        assertFalse(valid);

        when(mockFieldType.indexOptions()).thenReturn(IndexOptions.DOCS);
        valid = mapper.doValidateConfiguration("donkey");
        assertTrue(valid);
    }

    @Test(expectedExceptions = IOException.class)
    public void testDoValidateConfigurationException() throws Exception {

        mapper = new CryptoFieldMapper("id",
                mockFieldType,
                mockFieldType,
                100,
                5,
                22,
                fragmentMappers,
                mockSettings,
                mockMultiFields,
                1000,
                mockCopyTo
        );

        mapper.doValidateConfiguration("monkey");
    }

    @Test
    public void testGetIndexString() throws Exception {

        ParseContext mockParseContext = mock(ParseContext.class);
        XContentParser mockParser = mock(XContentParser.class);

        when(mockParseContext.externalValueSet()).thenReturn(true);
        when(mockParseContext.externalValue()).thenReturn("monkey");
        assertEquals(mapper.getIndexString(mockParseContext), "monkey");

        when(mockParseContext.externalValueSet()).thenReturn(false);
        when(mockParseContext.parser()).thenReturn(mockParser);
        when(mockParser.textOrNull()).thenReturn("donkey");
        assertEquals(mapper.getIndexString(mockParseContext), "donkey");
    }

    @Test
    public void testGenerateFragmentFields() {

        List<IndexableField> fields = new ArrayList<>();

        IndexFragmentFieldMapper mockFragmentMapper = MocksCreator.mockFragmentMapper("mock_name");

        Mockito.doReturn(mockFragmentMapper).when(fragmentMappersSpy).get(any());

        when(mockFragmentMapper.createField(anyString())).thenCallRealMethod();

        mapper.generateFragmentFields(fields,"aaaaaabbbbbbccccccddddddeeeeee","id");

        assertEquals(fields.size(), 5);
        assertEquals(fields.get(0).stringValue(), "aaaaaabbbbbbcccccc");
        assertEquals(fields.get(1).stringValue(), "bbbbbbccccccdddddd");
        assertEquals(fields.get(2).stringValue(), "ccccccddddddeeeeee");
        assertEquals(fields.get(3).stringValue(), "ddddddeeeeee");
        assertEquals(fields.get(4).stringValue(), "eeeeee");
    }

    @Test
    public void testGenerateFragmentFieldsNegative() {

        List<IndexableField> fields = new ArrayList<>();

        mapper.generateFragmentFields(fields,"","id");
        mapper.generateFragmentFields(null,"","id");
        mapper.generateFragmentFields(fields,null,"id");
        mapper.generateFragmentFields(fields,"spider",null);
        mapper.generateFragmentFields(fields,"spider","");
    }

    @Test
    public void testParseCreateField() throws Exception {

        List<IndexableField> fields = new ArrayList<>();

        // setup real fragment mappers, so that the final result will resemble close to what
        // we will see in real environment.
        //Map<String, IndexFragmentFieldMapper> realFragmentMappers = new HashMap<>();

        ParseContext mockParseContext = mock(ParseContext.class);

        // mappers setup
        for(int i=0;i<32;i++) {

            String fragmentName = MapperUtils.generateIndexFragmentName("id", String.valueOf(i + 1));

            IndexFragmentFieldMapper fragmentFieldMapper = createTestMapper(fragmentName);

            fragmentMappersSpy.put(fragmentName, fragmentFieldMapper);
        }

        IndexFragmentFieldMapper forwardFragmentFieldMapper = createTestMapper(MapperUtils.generateForwardFieldName("id"));
        IndexFragmentFieldMapper reverseFragmentFieldMapper = createTestMapper(MapperUtils.generateReverseFieldName("id"));
        fragmentMappersSpy.put(MapperUtils.generateForwardFieldName("id"), forwardFragmentFieldMapper);
        fragmentMappersSpy.put(MapperUtils.generateReverseFieldName("id"), reverseFragmentFieldMapper);

        // mapper setup - done

        when(mockParseContext.externalValueSet()).thenReturn(true);
        when(mockParseContext.externalValue()).thenReturn("bbbbbbccccccddddddeeeeeeffffff");

        mapper.parseCreateField(mockParseContext, fields);

        //assert for the result of parsing
        assertEquals(fields.size(), 7);

        assertEquals(fields.get(0).name(), "id_0");
        assertEquals(fields.get(0).stringValue(), "aaaaaabbbbbbccccccddddddeeeeee");

        assertEquals(fields.get(1).name(), "id_rev");
        assertEquals(fields.get(1).stringValue(), "eeeeeeddddddccccccbbbbbbaaaaaa");

        assertEquals(fields.get(2).name(), "id_1");
        assertEquals(fields.get(2).stringValue(), "aaaaaabbbbbbcccccc");

        assertEquals(fields.get(3).name(), "id_2");
        assertEquals(fields.get(3).stringValue(), "bbbbbbccccccdddddd");

        assertEquals(fields.get(4).name(), "id_3");
        assertEquals(fields.get(4).stringValue(), "ccccccddddddeeeeee");

        assertEquals(fields.get(5).name(), "id_4");
        assertEquals(fields.get(5).stringValue(), "ddddddeeeeee");

        assertEquals(fields.get(6).name(), "id_5");
        assertEquals(fields.get(6).stringValue(), "eeeeee");
    }

    private IndexFragmentFieldMapper createTestMapper(String fragmentName) {
        IndexFragmentFieldType fragmentFieldType = new IndexFragmentFieldType("id",
                fragmentName,
                6,
                18);

        return new IndexFragmentFieldMapper(fragmentFieldType, mockSettings);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testInvalidIndexOptions() {

        when(mockFieldType.indexOptions()).thenReturn(IndexOptions.NONE);

        new CryptoFieldMapper("test",
                mockFieldType,
                mockFieldType,
                -1,
                6,
                18,
                fragmentMappersSpy,
                mockSettings,
                mockMultiFields,
                32,
                mockCopyTo);
    }

    @Test
    public void testFieldAsserts() {

        IndexFragmentFieldMapper forwardFragmentFieldMapper = createTestMapper(MapperUtils.generateForwardFieldName("id"));
        fragmentMappersSpy.put(MapperUtils.generateForwardFieldName("id"), forwardFragmentFieldMapper);

        CryptoFieldMapper spy = spy(mapper);

        doReturn(mock(Iterator.class)).when(((FieldMapper)spy)).iterator();

        assertFalse(spy.isFragment());
        assertNotNull(spy.iterator());

        mapper.doMerge(mapper,false);
    }


}
