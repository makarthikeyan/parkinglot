package com.titaniamlabs.es.mapper;

import com.titaniamlabs.util.MapperUtils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static com.titaniamlabs.util.MapperUtils.*;

public class MapperUtilsTest {

    @Test
    public void testSplitToNCharSimple() {

        String orignal = "aaaabbbbccccddddee";

        String[] result = MapperUtils.splitToNChar(orignal,4,4);

        assertEquals(result.length, 5);
        assertEquals(result[0], "aaaa");
        assertEquals(result[1], "bbbb");
        assertEquals(result[2], "cccc");
        assertEquals(result[3], "dddd");
        assertEquals(result[4], "ee");
    }

    @Test
    public void testSplitToNCharSlidingWindow() {

        String orignal = "aaaabbbbccccddddee";

        String[] result = splitToNChar(orignal,2,4);

        assertEquals(result.length, 9);
        assertEquals(result[0], "aaaa");
        assertEquals(result[1], "aabb");
        assertEquals(result[2], "bbbb");
        assertEquals(result[3], "bbcc");
        assertEquals(result[4], "cccc");
        assertEquals(result[5], "ccdd");
        assertEquals(result[6], "dddd");
        assertEquals(result[7], "ddee");
        assertEquals(result[8], "ee");
    }

    @Test
    public void testSplitToNCharFailures() {

        String[] result = MapperUtils.splitToNChar(null,4,4);
        assertNull(result);

        result = MapperUtils.splitToNChar("",100,100);
        assertNull(result);

        result = MapperUtils.splitToNChar("a",100,100);
        assertNotNull(result);
        assertEquals(result.length, 1);
        assertEquals(result[0], "a");
    }

    @Test
    public void testNameGeneration() {

        assertEquals(MapperUtils.generateForwardFieldName("bee"), "bee_0");
        assertEquals(MapperUtils.generateReverseFieldName("bee"), "bee_rev");
        assertEquals(MapperUtils.generateIndexFragmentName("bee","101"), "bee_101");
    }

    @Test
    public void testSplitToNCharsInvalidRanges() {
        MapperUtils.splitToNChar("monkey",0,0);
        MapperUtils.splitToNChar("monkey",1,0);
        MapperUtils.splitToNChar("monkey",-1,-1);
        MapperUtils.splitToNChar("monkey",0,1);
    }

    @Test
    public void testForwardOrReverseName() {

       assertTrue(isForwardOrReverseName("abc_0"));
       assertTrue(isForwardOrReverseName("adfasdf_rev"));
       assertTrue(isForwardOrReverseName("a_0"));
       assertTrue(isForwardOrReverseName("b_rev"));

        assertFalse(isForwardOrReverseName("_rev"));
        assertFalse(isForwardOrReverseName("_0"));
       assertFalse(isForwardOrReverseName("_"));
       assertFalse(isForwardOrReverseName("0"));
       assertFalse(isForwardOrReverseName("rev"));
       assertFalse(isForwardOrReverseName(null));
       assertFalse(isForwardOrReverseName(""));
       assertFalse(isForwardOrReverseName("abc_7"));
    }
}
