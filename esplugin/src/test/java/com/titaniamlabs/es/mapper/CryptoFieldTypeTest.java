package com.titaniamlabs.es.mapper;

import org.apache.lucene.search.MultiTermQuery;
import org.elasticsearch.index.query.QueryShardContext;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class CryptoFieldTypeTest {

    @Test
    public void testSimple() {
        CryptoFieldType type = new CryptoFieldType();
        assertEquals(type.typeName(), CryptoFieldType.TYPE_NAME);

        type.setFielddata(true);
        type.setFielddataMinFrequency(100);
        type.setFielddataMaxFrequency(200);
        type.setFielddataMinSegmentSize(300);
        type.setPrefixFieldType(mock(PrefixFieldType.class));
        type.setIndexPhrases(true);

        assertTrue(type.fielddata());
        assertEquals(type.fielddataMinFrequency(), 100);
        assertEquals(type.fielddataMaxFrequency(), 200);
        assertEquals(type.fielddataMinSegmentSize(), 300);
        assertNotNull(type.getPrefixFieldType());
        assertTrue(type.isIndexPhrases());
        assertNotNull(type.fielddataBuilder("index001"));
        assertNotNull(type.toString());
    }

    @Test
    public void testQueryRewrites() {

        TitaniamQueryBuilder mockBuilder = mock(TitaniamQueryBuilder.class);

        CryptoFieldType type = new CryptoFieldType();
        type.setQueryBuilder(mockBuilder);
        type.setName("ssn");
        type.setFragmentedFieldMapperMap(new HashMap<>());


        MultiTermQuery.RewriteMethod mockRewriteMethod = mock(MultiTermQuery.RewriteMethod.class);
        QueryShardContext mockContext = mock(QueryShardContext.class);

        //prefix
        type.prefixQuery("mone", mockRewriteMethod, mockContext);
        verify(mockBuilder, times(1)).prefixQuery("ssn_0","mone",mockRewriteMethod, mockContext);

        //exists
        assertNotNull(type.existsQuery(mockContext));

        //term
        type.termQuery("408",mockContext);
        verify(mockBuilder, times(1)).termQuery("ssn","408", mockContext);

        type.wildCardQuery("242", mockRewriteMethod, mockContext);
        verify(mockBuilder, times(1)).wildCardQuery("ssn","242", mockRewriteMethod, mockContext);
    }
}
