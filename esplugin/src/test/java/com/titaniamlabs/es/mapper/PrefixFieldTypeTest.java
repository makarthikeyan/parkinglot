package com.titaniamlabs.es.mapper;

import org.apache.lucene.analysis.Analyzer;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.analysis.NamedAnalyzer;
import org.elasticsearch.index.query.QueryShardContext;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

public class PrefixFieldTypeTest {

    @Test
    public void testSimple() throws Exception {

        PrefixFieldType type = new PrefixFieldType("ssn", 6, 18);
        //length
        assertTrue(type.accept(12));
        assertFalse(type.accept(2));

        //type
        NamedAnalyzer mockAnalyzer = mock(NamedAnalyzer.class);
        when(mockAnalyzer.name()).thenReturn("mock-analyzer");
        when(mockAnalyzer.analyzer()).thenReturn(mock(Analyzer.class));
        type.setAnalyzer(mockAnalyzer);

        //xcontent
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        type.doXContent(builder);
        builder.endObject();
        assertEquals(builder.string(), "{\"index_prefixes\":{\"min_chars\":6,\"max_chars\":18}}");

        //clone
        PrefixFieldType clone = type.clone();
        assertNotNull(clone);
        assertEquals(clone, type);
        //explicit equals
        assertTrue(type.equals(clone));
        assertEquals(type.hashCode(), clone.hashCode());

        //typename
        assertEquals(type.typeName(), "prefix");

        //tostring
        assertNotNull(type.toString());
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testUnsupportedMethods() {
        new PrefixFieldType("ssn", 6, 18).existsQuery(mock(QueryShardContext.class));
    }
}
