package com.titaniamlabs.es.mapper;

import org.elasticsearch.Version;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.mapper.FieldMapper;
import org.elasticsearch.index.mapper.ParseContext;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class IndexFragmentFieldMapperTest {

    @Test
    public void testSimple() {

        IndexFragmentFieldType mockType = mock(IndexFragmentFieldType.class);
        when(mockType.name()).thenReturn("social");

        Settings mockSettings = mock(Settings.class);
        when(mockSettings.getAsVersion(any(), any())).thenReturn(Version.CURRENT);

        IndexFragmentFieldMapper mapper = new IndexFragmentFieldMapper(mockType, mockSettings);
        IndexFragmentFieldMapper spy = spy(mapper);

        doReturn(mockType).when((FieldMapper)spy).fieldType();

        assertEquals(spy.contentType(), IndexFragmentFieldType.TYPE_NAME);
        assertEquals(spy.fieldType(), mockType);
        assertNull(spy.spliterator());

        assertNotNull(spy.createField("408"));
        assertNotNull(spy.toString());
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testUnsupported() throws IOException {

        IndexFragmentFieldType mockType = mock(IndexFragmentFieldType.class);
        when(mockType.name()).thenReturn("social");

        Settings mockSettings = mock(Settings.class);
        when(mockSettings.getAsVersion(any(), any())).thenReturn(Version.CURRENT);

        IndexFragmentFieldMapper mapper = new IndexFragmentFieldMapper(mockType, mockSettings);
        mapper.parseCreateField(mock(ParseContext.class), new ArrayList<>());
    }
}
