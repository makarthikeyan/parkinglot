package com.titaniamlabs.es.mapper;

import org.elasticsearch.Version;
import org.elasticsearch.index.mapper.Mapper;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

public class TypeConfigurationParserTest {

    private TypeConfigurationParser configParser;
    private Mapper.TypeParser.ParserContext mockParserContext;
    private Map<String, Object> mockNode;

    @BeforeClass
    public void setup() {
        configParser = new TypeConfigurationParser();
        mockParserContext = mock(Mapper.TypeParser.ParserContext.class);
        mockNode = mock(Map.class);
    }

    @Test
    public void testMinMaxPrefixes() {

        Set mockSet = mock(Set.class);
        Iterator<Map.Entry<String, Object>> mockIterator = mock(Iterator.class);
        Map.Entry<String, Object> mockEntry = mock(Map.Entry.class);
        Version mockVersion = mock(Version.class);

        when(mockNode.entrySet()).thenReturn(mockSet);
        when(mockSet.iterator()).thenReturn(mockIterator);

        when(mockIterator.hasNext()).thenReturn(
                Boolean.TRUE, Boolean.FALSE,
                Boolean.TRUE, Boolean.FALSE,
                Boolean.TRUE, Boolean.FALSE,
                Boolean.TRUE, Boolean.FALSE,
                Boolean.TRUE, Boolean.FALSE
        );
        when(mockIterator.next()).thenReturn(mockEntry);

        when(mockEntry.getKey()).thenReturn("index_prefixes");

        Map<String,Object> indexPrefix = new HashMap<>();
        indexPrefix.put("min_chars","6");
        indexPrefix.put("max_chars","18");
        indexPrefix.put("max_fragments","32");

        when(mockEntry.getValue()).thenReturn(indexPrefix);

        when(mockParserContext.indexVersionCreated()).thenReturn(mockVersion);

        CryptoFieldMapperBuilder builder = configParser.parse("", mockNode, mockParserContext);

        assertNotNull(builder);
        assertEquals(builder.getMinPrefixChars(),6);
        assertEquals(builder.getMaxPrefixChars(),18);
        assertEquals(builder.getMaxIndexFragments(),32);
    }

    @Test
    public void testIndexPrefix() {

        Map<String, Object> nodeIP = new HashMap<>();
        nodeIP.put("min_chars", 5);
        nodeIP.put("max_chars", 6);

        Map<String, Object> node = new HashMap<>();
        node.put("index_prefixes", nodeIP);

        Version mockVersion = mock(Version.class);
        when(mockParserContext.indexVersionCreated()).thenReturn(mockVersion);

        CryptoFieldMapperBuilder builder = configParser.parse("ssn", node, mockParserContext);

        assertEquals(5, builder.getMinPrefixChars());
        assertEquals(6, builder.getMaxPrefixChars());
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testIndexPrefixNegative() {

        Map<String, Object> nodeIP = new HashMap<>();
        nodeIP.put("min_chars", -1);
        nodeIP.put("max_chars", -1);

        Map<String, Object> node = new HashMap<>();
        node.put("index_prefixes", nodeIP);

        Version mockVersion = mock(Version.class);
        when(mockParserContext.indexVersionCreated()).thenReturn(mockVersion);

        configParser.parse("ssn", node, mockParserContext);
    }

    @Test
    public void testIndexPrefixDefaults() {

        Map<String, Object> node = new HashMap<>();

        Version mockVersion = mock(Version.class);
        when(mockParserContext.indexVersionCreated()).thenReturn(mockVersion);

        CryptoFieldMapperBuilder builder = configParser.parse("ssn", node, mockParserContext);
        assertEquals(Defaults.INDEX_PREFIX_MIN_CHARS, builder.getMinPrefixChars());
        assertEquals(Defaults.INDEX_PREFIX_MAX_CHARS, builder.getMaxPrefixChars());
        assertEquals(Defaults.INDEX_PREFIX_MAX_FRAGMENTS, builder.getMaxIndexFragments());
    }
}
