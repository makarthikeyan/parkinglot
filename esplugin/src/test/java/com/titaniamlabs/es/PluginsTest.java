package com.titaniamlabs.es;

import org.elasticsearch.common.io.stream.NamedWriteableRegistry;
import org.elasticsearch.common.util.concurrent.ThreadContext;
import org.elasticsearch.ingest.Processor;
import org.elasticsearch.plugins.SearchPlugin;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertNotNull;

public class PluginsTest {

    @Test
    public void testSimple() {
        Plugins plugins = new Plugins();

        assertNotNull(plugins.getTransportInterceptors(mock(NamedWriteableRegistry.class),
                mock(ThreadContext.class)));

        assertNotNull(plugins.getQueries());

        assertNotNull(plugins.getMappers());

        assertNotNull(plugins.getProcessors(mock(Processor.Parameters.class)));

        assertNotNull(plugins.getFetchSubPhases(mock(SearchPlugin.FetchPhaseConstructionContext.class)));
    }
}
