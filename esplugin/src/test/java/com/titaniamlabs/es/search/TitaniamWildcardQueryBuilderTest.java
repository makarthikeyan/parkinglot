package com.titaniamlabs.es.search;

import com.fasterxml.jackson.core.JsonFactory;
import com.titaniamlabs.es.mapper.CryptoFieldType;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.*;
import org.elasticsearch.common.xcontent.json.JsonXContentParser;
import org.elasticsearch.index.mapper.TextFieldMapper;
import org.elasticsearch.index.query.QueryShardContext;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class TitaniamWildcardQueryBuilderTest {

    private TitaniamWildcardQueryBuilder builder;
    private QueryShardContext mockContext;

    @BeforeMethod
    public void setUp() {
        this.builder = new TitaniamWildcardQueryBuilder("id","*name*");
        this.mockContext = mock(QueryShardContext.class);
    }

    @Test
    public void testDoToQueryDelegatesToCrypto() throws IOException  {

        CryptoFieldType mockType = mock(CryptoFieldType.class);

        when(this.mockContext.fieldMapper("id")).thenReturn(mockType);

        this.builder.doToQuery(mockContext);

        verify(mockType, times(1)).wildCardQuery(any(), any(), any());
    }

    @Test
    public void testDoToQueryDoesNOTDelegatesToCrypto() throws IOException  {

        WildcardWrapper wrapper = mock(WildcardWrapper.class);
        when(wrapper.fieldName()).thenReturn("id");

        when(this.mockContext.fieldMapper(any())).thenReturn(new TextFieldMapper.TextFieldType());

        // verify a non-crypto field is not calling wildCardQuery()
        this.builder = new TitaniamWildcardQueryBuilder(wrapper);

        this.builder.doToQuery(mockContext);

        verify(wrapper, times(1)).doToQuery(any());
    }

    @Test
    public void testConstructors() throws Exception {
        WildcardQueryBuilder queryBuilder = mock(WildcardQueryBuilder.class);
        when(queryBuilder.fieldName()).thenReturn("social");
        when(queryBuilder.value()).thenReturn("*408*");

        this.builder = new TitaniamWildcardQueryBuilder(queryBuilder);
        this.builder = new TitaniamWildcardQueryBuilder(mock(StreamInput.class));

        //rewrite
        this.builder.rewrite();
        this.builder.rewrite("abc");
        this.builder.doXContent(mock(XContentBuilder.class), mock(ToXContent.Params.class));
    }

    @Test
    public void testDoWriteTo() throws Exception{
        WildcardQueryBuilder queryBuilder = mock(WildcardQueryBuilder.class);
        when(queryBuilder.fieldName()).thenReturn("social");
        when(queryBuilder.value()).thenReturn("*408*");
        when(queryBuilder.rewrite()).thenReturn("408");

        this.builder = new TitaniamWildcardQueryBuilder(queryBuilder);

        StreamOutput output = mock(StreamOutput.class);

        this.builder.doWriteTo(output);
        verify(output, times(1)).writeString("social");
        verify(output, times(1)).writeString("*408*");
        verify(output, times(1)).writeOptionalString("408");

        //validate accessors
        assertEquals(this.builder.fieldName(), "social");
        assertEquals(this.builder.value(), "*408*");
    }

    @Test
    public void testFromXContent() throws Exception {

        //TODO: Find hte right json that will make this work..
//        String json = "{ \"wildcard\" : \"ki*y\", \"boost\" : 2.0 }";
//
//        JsonFactory factory = new JsonFactory();
//        XContentParser parser = new JsonXContentParser(NamedXContentRegistry.EMPTY,
//                factory.createParser(json));
//
//        TitaniamWildcardQueryBuilder builder = TitaniamWildcardQueryBuilder.fromXContent(parser);
//        assertNotNull(builder);
    }

    @Test
    public void testEqualsHashCode() {
        WildcardQueryBuilder queryBuilder = mock(WildcardQueryBuilder.class);
        when(queryBuilder.fieldName()).thenReturn("social");
        when(queryBuilder.value()).thenReturn("*408*");
        when(queryBuilder.rewrite()).thenReturn("408");
        this.builder = new TitaniamWildcardQueryBuilder(queryBuilder);

        assertTrue(this.builder.doEquals(this.builder));
        // hascode is -ve????
        assertTrue(this.builder.doHashCode()!=0,"Hashcode: "+this.builder.doHashCode());
    }
}
