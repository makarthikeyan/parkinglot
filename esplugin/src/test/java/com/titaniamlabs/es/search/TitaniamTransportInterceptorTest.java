package com.titaniamlabs.es.search;

import org.elasticsearch.transport.TransportInterceptor;
import org.elasticsearch.transport.TransportRequestHandler;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertNotNull;

public class TitaniamTransportInterceptorTest {

    @Test
    public void testSimple() {

        TitaniamTransportInterceptor interceptor = new TitaniamTransportInterceptor(null,null);
        interceptor.interceptHandler("GET","null",false,mock(TransportRequestHandler.class));

        TransportInterceptor.AsyncSender mockSender = mock(TransportInterceptor.AsyncSender.class);
        TransportInterceptor.AsyncSender asyncSender = interceptor.interceptSender(mockSender);
        assertNotNull(asyncSender);

        TransportInterceptor.AsyncSender spy = spy(asyncSender);
        spy.sendRequest(null,null,null,null,null);
        verify(mockSender, times(1)).sendRequest(any(), any(), any(), any(), any());

        //validate all methods in inner sendRequest

    }
}
