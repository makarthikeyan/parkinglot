package com.titaniamlabs.es.search;

import com.titaniamlabs.crypto.MockCryptoEngine;
import com.titaniamlabs.es.mapper.CryptoFieldType;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.mapper.MapperService;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.FetchSubPhase;
import org.elasticsearch.search.internal.SearchContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class TitaniamFetchSubPhaseTest {

    private TitaniamFetchSubPhase subPhase;
    private SearchContext mockContext;
    private FetchSubPhase.HitContext mockHitContext;
    private MapperService mockMapperService;

    @BeforeMethod
    public void setup() {

        this.subPhase = new TitaniamFetchSubPhase();
        this.subPhase.setCryptoEngine(new MockCryptoEngine());

        this.mockContext = mock(SearchContext.class);
        this.mockHitContext = mock(FetchSubPhase.HitContext.class);
        this.mockMapperService = mock(MapperService.class);

        when(mockContext.mapperService()).thenReturn(mockMapperService);
        when(mockMapperService.fullName("social")).thenReturn(new CryptoFieldType());
    }

    @Test
    public void testSimple() throws Exception {

        Map<String, Object> documentSource = new HashMap<>();
        documentSource.put("first_name", "joe");
        documentSource.put("last_name", "joe");
        documentSource.put("social", "444444......555555......666666666666");

        XContentBuilder builder = this.subPhase.untangleDocumentSource(documentSource, mockContext);
        assertNotNull(builder);

        assertEquals(builder.string(), "{\"social\":\"3-4-55\",\"last_name\":\"joe\",\"first_name\":\"joe\"}");
    }

    @Test
    public void testNested() throws Exception {

        Map<String, Object> nested = new HashMap<>();
        nested.put("social", "333333------444444------555555555555");

        Map<String, Object> documentSource = new HashMap<>();
        documentSource.put("first_name", "joe");
        documentSource.put("last_name", "joe");
        documentSource.put("sensitive", nested);

        XContentBuilder builder = this.subPhase.untangleDocumentSource(documentSource, mockContext);
        assertNotNull(builder);

        //TODO: clarify the expected functionality for nested fields.
        // assertEquals(builder.string(), "{\"last_name\":\"joe\",\"sensitive\":{\"social\":\"3-4-55\"},\"first_name\":\"joe\"}");
    }

    @Test
    public void testHitExecute() throws Exception {

        Map<String, Object> documentSource = new HashMap<>();
        documentSource.put("first_name", "joe");
        documentSource.put("last_name", "joe");
        documentSource.put("social", "333333------444444------555555555555");

        SearchHit mockSearchHit = mock(SearchHit.class);
        when(mockSearchHit.getSourceAsMap()).thenReturn(documentSource);

        when(mockHitContext.hit()).thenReturn(mockSearchHit);

        this.subPhase.hitExecute(mockContext, mockHitContext);

        verify(mockSearchHit, times(1)).sourceRef(any());
    }
    
    @Test
    public void testNestedDocument() throws IOException {

        Map<String, Object> grandParent = new HashMap<>();
        Map<String, Object> parent = new HashMap<>();
        Map<String, Object> child = new HashMap<>();
        child.put("social", "444444......555555");
        
        parent.put("parent", child);
        grandParent.put("grandparent", parent);
        
        Map<String, Object> documentSource = new HashMap<>();
        documentSource.put("greatgrandparent", grandParent);

        XContentBuilder builder = this.subPhase.untangleDocumentSource(documentSource, mockContext);
        assertNotNull(builder);

        assertEquals(builder.string(), "{\"greatgrandparent\":{\"grandparent\":{\"parent\":{\"social\":\"3-4\"}}}}");
    }

    @Test
    public void testMapHandlerMapChildren() throws IOException {

        Map<String, Object> grandParent = new HashMap<>();
        Map<String, Object> parent = new HashMap<>();
        Map<String, Object> child = new HashMap<>();
        child.put("social", "444444......555555");

        parent.put("parent", child);
        grandParent.put("grandparent", parent);

        Map<String, Object> documentSource = new HashMap<>();
        documentSource.put("greatgrandparent", grandParent);

        TitaniamFetchSubPhase spy = spy(this.subPhase);

        spy.handleMap(documentSource, mockContext);
        verify(spy).handleMap(grandParent, mockContext);
        verify(spy).handleMap(parent, mockContext);
        verify(spy).handleMap(child, mockContext);
    }

    @Test
    public void testMapHandlerListChildren() throws IOException {

        Map<String, Object> grandParent = new HashMap<>();
        Map<String, Object> parent = new HashMap<>();
        List<Object> children = Arrays.asList("a","b");

        parent.put("parent", children);
        grandParent.put("grandparent", parent);

        Map<String, Object> documentSource = new HashMap<>();
        documentSource.put("greatgrandparent", grandParent);

        TitaniamFetchSubPhase spy = spy(this.subPhase);

        spy.handleMap(documentSource, mockContext);
        verify(spy).handleMap(grandParent, mockContext);
        verify(spy).handleMap(parent, mockContext);
        verify(spy).handleList("parent",children, mockContext);
    }

    @Test
    public void testListHandlerMapChildren() throws IOException {

        Map<String, Object> grandParent = new HashMap<>();
        Map<String, Object> parent = new HashMap<>();

        Map<String, Object> child1 = new HashMap<>();
        child1.put("social", "444444......555555");

        Map<String, Object> child2 = new HashMap<>();
        child2.put("social", "666666......777777");

        List<Object> children = Arrays.asList(child1, child2);

        parent.put("parent", children);
        grandParent.put("grandparent", parent);

        Map<String, Object> documentSource = new HashMap<>();
        documentSource.put("greatgrandparent", grandParent);

        TitaniamFetchSubPhase spy = spy(this.subPhase);

        Map<String, Object> result = spy.handleMap(documentSource, mockContext);
        verify(spy).handleMap(grandParent, mockContext);
        verify(spy).handleMap(parent, mockContext);
        verify(spy).handleList("parent",children, mockContext);
        verify(spy).handleMap(child1, mockContext);
        verify(spy).handleMap(child2, mockContext);

        assertTrue(result.containsKey("greatgrandparent"));
        Map<String, Object> grandParentInResult = (Map<String, Object>)result.get("greatgrandparent");
        assertTrue(grandParentInResult.containsKey("grandparent"));
        Map<String, Object> parentInResult = (Map<String, Object>)grandParentInResult.get("grandparent");
        assertTrue(parentInResult.containsKey("parent"));
        List<Object> childrenInResult = (List<Object>) parentInResult.get("parent");
        assertEquals(childrenInResult.size(), 2);
    }

    @Test
    public void testEmptyCollections() {

        Map<String, Object> grandParent = new HashMap<>();

        Map<String, Object> documentSource = new HashMap<>();
        documentSource.put("greatgrandparent", grandParent);

        TitaniamFetchSubPhase spy = spy(this.subPhase);
        spy.handleMap(documentSource, mockContext);

        grandParent.put("parent", new ArrayList<>());
        spy.handleMap(documentSource, mockContext);
    }
}
