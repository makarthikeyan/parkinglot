package com.titaniamlabs.es.search;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.internal.ShardSearchTransportRequest;
import org.elasticsearch.tasks.Task;
import org.elasticsearch.tasks.TaskId;
import org.elasticsearch.transport.TransportChannel;
import org.elasticsearch.transport.TransportRequestHandler;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

public class TitaniamTransportRequestHandlerTest {

    @Test
    public void testSimple() throws Exception {

        TransportRequestHandler actualHandler = mock(TransportRequestHandler.class);
        TitaniamTransportRequestHandler handler = new TitaniamTransportRequestHandler(actualHandler);
        TitaniamTransportRequestHandler spy = spy(handler);

        SearchSourceBuilder mockSource = mock(SearchSourceBuilder.class);

        ShardSearchTransportRequest mockRequest = mock(ShardSearchTransportRequest.class);
        when(mockRequest.source()).thenReturn(mockSource);

        Task mockTask = mock(Task.class);
        TaskId mockTaskId = mock(TaskId.class);
        when(mockTaskId.getId()).thenReturn(101L);

        when(mockTask.getParentTaskId()).thenReturn(mockTaskId);

        handler.messageReceived(mockRequest, mock(TransportChannel.class), mockTask);
        verify(actualHandler, timeout(1)).messageReceived(any(), any(), any());

        //BoolQuery
        when(mockTaskId.getId()).thenReturn(202L);

        BoolQueryBuilder mockBoolQuery = mock(BoolQueryBuilder.class);
        when(mockSource.query()).thenReturn(mockBoolQuery);
        spy.messageReceived(mockRequest, mock(TransportChannel.class), mockTask);
        verify(spy,times(1)).convertBoolQueryBuilder(any());

        //2 arg messageReceived
        spy.messageReceived(mockRequest, null);
    }
}
