package com.titaniamlabs.es.ingest;

import com.titaniamlabs.api.ISearchApi;
import com.titaniamlabs.crypto.*;
import org.elasticsearch.common.inject.AbstractModule;
import org.elasticsearch.common.inject.Guice;
import org.elasticsearch.common.inject.Injector;
import org.elasticsearch.ingest.IngestDocument;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class TitaniamNativeProcessorTest {

    private TitaniamProcessor processor;
    private ISearchApi mockSearchApi;

    @BeforeSuite
    public void setup() {

        mockSearchApi = mock(ISearchApi.class);

        AbstractModule testModule = new AbstractModule() {

            @Override
            protected void configure() {
                bind(CryptoEngine.class).to(NativeCryptoEngine.class);
                bind(ISearchApi.class).toInstance(mockSearchApi);
            }
        };

        Injector injector = Guice.createInjector(testModule);
        processor = new TitaniamProcessor("-", Arrays.asList("a","b"));
        processor.setCryptoEngine(injector.getInstance(CryptoEngine.class));
    }

    @Test
    public void testCryptoEngineWithMockSearch() throws Exception {

        when(mockSearchApi.entangle(anyString(),anyBoolean(),anyBoolean())).thenReturn(new String[]{"","shuffled:SSN",""});

        IngestDocument mockIngestDoc = mock(IngestDocument.class);

        when(mockIngestDoc.getFieldValue(any(String.class),any(Class.class), anyBoolean())).thenReturn("SSN");
        processor.execute(mockIngestDoc);

        verify(mockIngestDoc).setFieldValue("a", "shuffled:SSN");
        verify(mockIngestDoc).setFieldValue("b", "shuffled:SSN");
    }

    public void testCryptoEnginewithJNIMock() {

    }
}
