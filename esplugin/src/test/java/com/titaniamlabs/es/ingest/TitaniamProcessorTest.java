package com.titaniamlabs.es.ingest;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.crypto.MockCryptoEngine;
import org.elasticsearch.index.VersionType;
import org.elasticsearch.ingest.IngestDocument;
import org.elasticsearch.ingest.Processor;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

public class TitaniamProcessorTest {

    private TitaniamProcessor processor;

    @BeforeSuite
    public void setup() {

        AbstractModule testModule = new AbstractModule() {

            @Override
            protected void configure() {
                bind(CryptoEngine.class).to(MockCryptoEngine.class);
            }
        };

        Injector injector = Guice.createInjector(testModule);
        processor = new TitaniamProcessor("-", Arrays.asList("a","b"));
        processor.setCryptoEngine(injector.getInstance(CryptoEngine.class));
    }

    @Test
    public void testInitialization() {
        assertNotNull(processor);
    }

    @Test
    public void testProcessor() throws Exception {

        IngestDocument mockIngestDoc = mock(IngestDocument.class);

        when(mockIngestDoc.getFieldValue(any(String.class),any(Class.class), anyBoolean())).thenReturn("SSN");
        processor.execute(mockIngestDoc);

        verify(mockIngestDoc).setFieldValue("a", "TTTTTTTTTTTTOOOOOO");
        verify(mockIngestDoc).setFieldValue("b", "TTTTTTTTTTTTOOOOOO");

        //assert type
        assertEquals(processor.getType(), TitaniamProcessor.TYPE);
    }

    public static IngestDocument mockIngestDocument() {
        String index = "index:1";
        String id = "ID:1";
        String routing = null;
        Long version = 10L;
        VersionType versionType = VersionType.INTERNAL;

        return new IngestDocument(index, "", id, routing, "parent", new HashMap());
    }

    @Test
    public void testProcessorFactory() throws Exception {

        ProcessorFactory factory = new ProcessorFactory();
        factory.setCryptoEngine(mock(CryptoEngine.class));

        Map<String, Object> config = new HashMap<>();
        config.put(TitaniamProcessor.FIELD, new ArrayList<>());

        Processor processor = factory.create(new HashMap<>(), "ssn", config);
        assertNotNull(processor);
    }

}
