package com.titaniamlabs.es.basic;

import com.titaniamlabs.log.Log;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.document.DocumentField;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.testng.annotations.*;

import java.util.Map;

import static com.titaniamlabs.es.basic.IndexManager.*;
import static org.testng.Assert.*;

public class BasicCrudFuncTest {

    private Embedded embedded;
    private RestHighLevelClient client;
    private IndexManager index;

    @BeforeSuite
    public void oneTimeSetup() throws Exception {
        embedded = new Embedded();
        embedded.start();

        client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")
                )
        );

        index = new IndexManager(client);
        assertTrue(index.initializePipeline());
        assertTrue(index.initializeIndex());
    }

    @AfterSuite
    public void oneTimeTearDown() throws Exception {

        if(client!=null) {
            client.close();
        }

        if(embedded!=null) {
            embedded.stop();
        }

    }

    @Test
    public void testSimple() throws Exception {

        String id = index.putUser("Joe", "Bloe","male","666-999-6666");
        Map<String, Object> user = index.getUserById(id);

        assertNotNull(user);
        assertEquals(user.get("first_name"), "Joe");
        assertEquals(user.get("last_name"), "Bloe");
        assertEquals(user.get("gender"), "male");
        assertEquals(user.get("social"), "777777777777777777......::::::::::::::::::......777777777777777777777777");

        index.deleteRecords(INDEX_NAME_USERS, new String[]{id});
    }

    @Test
    public void testSearchAllUsers() throws Exception {

        String id1 = index.putUser("Joe", "Bloe","male","666-999-6666");
        String id2 = index.putUser("Jane", "Doe","female","666-777-8888");
        String id3 = index.putUser("Joe", "Shmoe","male","222-777-6666");

        // map is used for later asserts
        Map<String, String> idToSocial = Map.of(id1, "666-999-6666",
                id2, "666-777-8888",
                id3, "222-777-6666");

        // For some reason, searching right after indexing seems to fail..
        // Thread.sleep(5000);
        // This is required, else, the next query will not return any result.
        index.refreshIndex();

        SearchHits searchHits = index.findAllUsers();
        assertNotNull(searchHits);

        assertEquals(searchHits.getTotalHits(), 3);

        // validate the tangled field is clear in the result, as this is a search case.
        SearchHit searchHit = searchHits.getAt(0);

        Map<String, Object> source = searchHit.getSourceAsMap();
        assertEquals(source.get(FIELD_NAME_SSN), idToSocial.get(searchHit.getId()));

        index.deleteRecords(INDEX_NAME_USERS, new String[]{id1, id2, id3});
    }

    @Test
    public void testPrefixSearch() throws Exception {

        String id1 = index.putUser("Adam", "Bloe","male","666-999-6666");
        String id2 = index.putUser("Ashly", "Doe","female","666-777-8888");
        String id3 = index.putUser("Andy", "Shmoe","male","333-777-6666");

        // For some reason, searching right after indexing seems to fail..
        // Thread.sleep(5000);
        // This is required, else, the next query will not return any result.
        index.refreshIndex();

        // 3 char prefix.
        SearchHits searchHits = index.findAllUserBySocialPrefix("333");
        assertNotNull(searchHits);

        assertEquals(searchHits.getTotalHits(), 1);

        SearchHit searchHit = searchHits.getAt(0);
        Map<String, Object> source = searchHit.getSourceAsMap();
        assertEquals(source.get(FIELD_NAME_SSN), "333-777-6666");
        assertEquals(source.get(IndexManager.FIELD_NAME_FIRST_NAME), "Andy");
        assertEquals(source.get(IndexManager.FIELD_NAME_LAST_NAME), "Shmoe");

        // 5 char prefix
        searchHits = index.findAllUserBySocialPrefix("666-7");
        assertNotNull(searchHits);

        assertEquals(searchHits.getTotalHits(), 1);

        searchHit = searchHits.getAt(0);
        source = searchHit.getSourceAsMap();
        assertEquals(source.get(FIELD_NAME_SSN), "666-777-8888");

        index.deleteRecords(INDEX_NAME_USERS, new String[]{id1, id2, id3});
    }

    @Test
    public void testPrefixSearchNestedObject() throws Exception {

        assertTrue(index.initializeIndexNested());

        String id = index.putUserNested("Abhi", "Johnson", "male", "444-555-7777");

        index.refreshIndex(INDEX_NAME_USERS_NESTED);

        // validate the nested structure by fetching it by ID
        Map<String, Object> user = index.getUserById(id, INDEX_NAME_USERS_NESTED);
        assertNotNull(user);
        assertEquals(user.get("first_name"), "Abhi");
        assertTrue(user.get("sensitive") instanceof  Map);
        assertEquals(((Map<String,String>)user.get("sensitive")).get(FIELD_NAME_SSN), "444-555-7777");
        // The above assert should ideally be like below:
        // assertEquals(((Map<String,String>)user.get("sensitive")).get(FIELD_NAME_SSN), "444444444444444444------555555555555555555------777777777777777777777777");

        // Basic search on normal field.
        SearchHits searchHits = index.findAllUserByPrefix(FIELD_NAME_FIRST_NAME,"Abhi", INDEX_NAME_USERS_NESTED);
        assertNotNull(searchHits);
        assertEquals(searchHits.getTotalHits(), 1);

        // Because of the above issue, below search query would not work.
        //Search for the same user by prefix.
        //this should return the same document with fields untangled.
        /*
        searchHits = index.findAllUserBySocialPrefix("444", INDEX_NAME_USERS_NESTED);
        assertNotNull(searchHits);
        assertEquals(searchHits.getTotalHits(), 1);

        SearchHit searchHit = searchHits.getAt(0);
        user = searchHit.getSourceAsMap();
        assertEquals(user.get("first_name"), "Abhi");
        assertEquals(((Map<String,String>)user.get("sensitive")).get(FIELD_NAME_SSN), "444-555-7777");
         */
    }

    @Test
    public void testTermQuery() throws  Exception {

        String id1 = index.putUser("Sam", "Samuels", "male", "555-666-8888");
        String id2 = index.putUser("Jay", "Jacobs", "female", "666-666-8888");
        String id3 = index.putUser("Dan", "Dickinson", "male", "888-666-8888");

        index.refreshIndex(INDEX_NAME_USERS);

//        Thread.sleep(180000);

        SearchHits hits = index.findAllUsersByTerm(INDEX_NAME_USERS, "social", "555-666-8888");
//        SearchHits hits = index.findAllUsersByTerm(INDEX_NAME_USERS, "social", "555");
        assertNotNull(hits);
        assertEquals(hits.getTotalHits(), 1);

        SearchHit searchHit = hits.getAt(0);
        Map<String, Object> user  = searchHit.getSourceAsMap();
        assertEquals(user.get(FIELD_NAME_SSN), "555-666-8888");
        assertEquals(user.get(FIELD_NAME_FIRST_NAME), "Sam");
        assertEquals(user.get(FIELD_NAME_LAST_NAME), "Samuels");
        assertEquals(user.get(FIELD_NAME_GENDER), "male");

        index.deleteRecords(INDEX_NAME_USERS, new String[]{id1, id2, id3});
    }

    @Test
    public void testTermQueryLetters() throws  Exception {

        String id1 = index.putUser("Sam", "Samuels", "male", "KKK-MMM-YYYY");

        index.refreshIndex(INDEX_NAME_USERS);

        SearchHits hits = index.findAllUsersByTerm(INDEX_NAME_USERS, "social", "KKK-MMM-YYYY");
        assertNotNull(hits);
        assertEquals(hits.getTotalHits(), 1);

        SearchHit searchHit = hits.getAt(0);
        Map<String, Object> user  = searchHit.getSourceAsMap();
        assertEquals(user.get(FIELD_NAME_SSN), "KKK-MMM-YYYY");
        assertEquals(user.get(FIELD_NAME_FIRST_NAME), "Sam");
        assertEquals(user.get(FIELD_NAME_LAST_NAME), "Samuels");
        assertEquals(user.get(FIELD_NAME_GENDER), "male");

        index.deleteRecords(INDEX_NAME_USERS, new String[]{id1});
    }

    @Test
    public void testWildcardQuery() throws Exception {

        String id1 = index.putUser("Sam", "Samuels", "male", "555-666-8888");
        String id2 = index.putUser("Jay", "Jacobs", "female", "666-666-8888");
        String id3 = index.putUser("Dan", "Dickinson", "male", "888-666-8888");

        index.refreshIndex(INDEX_NAME_USERS);

        SearchHits hits = index.findAllUsersByWildcard(INDEX_NAME_USERS, "social", "*666*");
        assertNotNull(hits);
        assertEquals(hits.getTotalHits(), 3);

        //assert hits.
        Map<String, String> expectedIDToSocial = Map.of(id1, "555-666-8888",
                id2, "666-666-8888",
                id3, "888-666-8888");

        for(SearchHit searchHit : hits) {
            Map<String, Object> user  = searchHit.getSourceAsMap();
            assertEquals(user.get(FIELD_NAME_SSN), expectedIDToSocial.get(searchHit.getId()));
        }

        index.deleteRecords(INDEX_NAME_USERS, new String[]{id1, id2, id3});
    }

    @Test
    public void testWildcardQueryMoreThan3() throws Exception {

        String id1 = index.putUser("Sam", "Samuels", "male", "555-6666-888");
        String id2 = index.putUser("Jay", "Jacobs", "female", "666-777-8888");
        String id3 = index.putUser("Dan", "Dickinson", "male", "888-6666-8888");

        index.refreshIndex(INDEX_NAME_USERS);

        SearchHits hits = index.findAllUsersByWildcard(INDEX_NAME_USERS, "social", "*6666*");
        assertNotNull(hits);
        assertEquals(hits.getTotalHits(), 2);

        //assert hits.
        Map<String, String> expectedIDToSocial = Map.of(id1, "555-6666-888",
                id3, "888-6666-8888");

        for(SearchHit searchHit : hits) {
            Map<String, Object> user  = searchHit.getSourceAsMap();
            assertEquals(user.get(FIELD_NAME_SSN), expectedIDToSocial.get(searchHit.getId()));
        }

        index.deleteRecords(INDEX_NAME_USERS, new String[]{id1, id2, id3});
    }

    //Boolean with non-tangled field with a tangled field.

}
