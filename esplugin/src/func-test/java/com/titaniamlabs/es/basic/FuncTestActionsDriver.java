package com.titaniamlabs.es.basic;

import com.titaniamlabs.log.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.util.Strings;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

public class FuncTestActionsDriver {

    private static final Log log = Log.getLogger(FuncTestActionsDriver.class);

    private Embedded embedded;

    @BeforeSuite
    public void oneTimeSetup() throws Exception {
        embedded = new Embedded();
        embedded.start();
    }

    @Test
    public void executeTest() throws IOException, InterruptedException {
        // Step 1: Scan for all tests in path
        // test names should start with titaniam-spec-
        List<File> files = findAllActionFiles();
        assertTrue(files.size()>0);

        // Step 2: for each spec, load datasource into it's own index, if specified
        // then, executes the test.
        int totalActions = 0;

        for (File aFile : files) {

            List<RestAction> restActions = parseTestActions(aFile);
            performActions(restActions);

            totalActions += restActions.size();
        }

        log.warn("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        log.warn("^^^^^^ Successfully run {} actions across {} file(s) ^^^^^^^", totalActions, files.size());
        log.warn("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    }

    /**
     * High level method that executes the actions.
     *
     * @param restActions
     * @throws IOException
     * @throws InterruptedException
     */
    public void performActions(List<RestAction> restActions) throws IOException, InterruptedException {

        HttpClient httpClient = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(60))
                .build();

        for(RestAction restAction : restActions) {

            log.debug("-------------------------> Starting request: {}",restAction.shortString());

            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(makeElasticURI(restAction))
                    .header("Content-Type","application/json")
                    .header("Accept-Encoding","")
                    .method(restAction.method, HttpRequest.BodyPublishers.ofString(restAction.bodyJson.toString()))
                    .build();

            log.debug("Executing: {}", restAction);

            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

            if(response.statusCode() >= 400) {
                log.error("Status Not 200: {}",response.statusCode());
                log.error("Failed: {}", restAction);
                log.error(response.headers());
                fail(response.body());
            } else {
                log.debug("Success calling: {}, http status: {}", restAction.shortString(), response.statusCode());
            }

            assertResponse(restAction, response.body());
        }

    }

    /**
     * Ensures the actual json response has at least the keys specified
     * in the actions file.
     *
     * @param restAction
     * @param responseAsJson
     */
    public void assertResponse(RestAction restAction, String responseAsJson) {

        log.debug("-->Full response: {}", responseAsJson);

        JSONObject actual = new JSONObject(responseAsJson);

        assertJsonEquals(actual, restAction.expectedJson, restAction);
    }

    public void assertJsonEquals(JSONObject actual, JSONObject expected, RestAction restAction) {

        for(String key: expected.keySet()) {

            log.debug("Validating field: {}", key);
            assertTrue(actual.has(key), key+" field NOT found in the actual response");

            Object actualValue = actual.get(key);
            Object expectedValue = expected.get(key);

            assertEquals(actualValue.getClass(), expectedValue.getClass(), "Class mismatch for field: "+key);

            if(actualValue instanceof  JSONObject) {

                // recursion
                assertJsonEquals((JSONObject) actualValue, (JSONObject) expectedValue, restAction);

            } else if(actualValue instanceof JSONArray){

                JSONArray actualArray = (JSONArray) actualValue;
                JSONArray expectedArray = (JSONArray) expectedValue;

                assertEquals(actualArray.length(), expectedArray.length(), "JSON Array's not matching for field: "+key);

                for(int i=0;i<actualArray.length();i++) {

                    // recursion
                    assertJsonEquals(actualArray.getJSONObject(i),
                            expectedArray.getJSONObject(i),
                            restAction);
                }

            } else {

                // recursion break
                assertEquals(actualValue, expectedValue, restAction.shortString());
            }
        }

    }

    /**
     * Forms the URL for the call. The actions file can specify an absolute url..
     *
     * @param restAction
     * @return
     */
    public URI makeElasticURI(RestAction restAction) {

        if(restAction.location.startsWith("http:") || restAction.location.startsWith("https:")) {

            return URI.create(restAction.location);

        } else {

            String path = restAction.location.charAt(0) == '/' ? restAction.location : "/"+restAction.location;

            return URI.create("http://localhost:9200"+path);
        }
    }

    /**
     * Searches the resources dir for all files with prefix "titaniam-spec"
     *
     * @return
     */
    public List<File> findAllActionFiles() {

        List<File> testSpecs = new ArrayList<>();

        File resourcesDirectory = new File("src/func-test/resources");

        File[] listOfFiles = resourcesDirectory.listFiles();

        for (File aFile: listOfFiles) {

            if (aFile.isFile() && aFile.getName().startsWith("titaniam-spec")) {
                testSpecs.add(aFile);
                log.debug("Adding: {}",aFile.getAbsolutePath());
            }
        }

        return testSpecs;
    }

    /**
     * Converts the values in file to objects for further processing.
     *
     * @param actionsFile
     * @return
     * @throws IOException
     */
    public List<RestAction> parseTestActions(File actionsFile) throws IOException {

        List<RestAction> restActionList = new ArrayList<>();

        String jsonAsString = Files.readString(Paths.get(actionsFile.toURI()));

        JSONObject jsonObject = new JSONObject(jsonAsString);
        JSONArray actions = jsonObject.getJSONArray("actions");

        for(Object o : actions) {

            JSONObject actionJson = (JSONObject) o;

            String actionMethod = actionJson.getString("#method");
            String actionUrl = actionJson.getString("#url");
            JSONObject actionBody = actionJson.getJSONObject("#body");
            JSONObject actionExpected = actionJson.getJSONObject("#expected");

            RestAction restAction = new RestAction(actionMethod,
                    actionUrl,
                    actionBody,
                    actionExpected,
                    actionsFile);

            restAction.doValidate();
            restActionList.add(restAction);
        }

        log.debug("Total actions to execute: {}", restActionList.size());

        return restActionList;
    }

    /**
     * Holder object for the contents of each action.
     */
    public static class RestAction {
        private String method;
        private String location;
        private JSONObject bodyJson;
        private JSONObject expectedJson;
        private File actionsFile;

        public RestAction(String method,
                          String location,
                          JSONObject bodyJson,
                          JSONObject expectedJson,
                          File actionsFile) {
            this.method = method;
            this.location = location;
            this.bodyJson = bodyJson;
            this.expectedJson = expectedJson;
            this.actionsFile = actionsFile;
        }

        public void doValidate() {
            //validate and throw exceptions

            if(Strings.isNullOrEmpty(this.method)) {
                throw new IllegalArgumentException("'#method' can not be empty");
            }

            if(Strings.isNullOrEmpty(this.location)) {
                throw new IllegalArgumentException("'#location' can not be empty");
            }

            if(this.expectedJson==null) {
                throw new IllegalArgumentException("'#expected' can not be empty");
            }
        }

        public String shortString() {
            return method+" "+location;
        }

        @Override
        public String toString() {
            return "RestAction{" +
                    "method='" + method + '\'' +
                    ", location='" + location + '\'' +
                    ", bodyJsonAsString='" + bodyJson + '\'' +
                    ", expectedJsonAsString='" + expectedJson + '\'' +
                    ", actionsFile=" + actionsFile +
                    '}';
        }
    }
}
