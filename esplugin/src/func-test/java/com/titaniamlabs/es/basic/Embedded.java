package com.titaniamlabs.es.basic;

import com.titaniamlabs.crypto.CryptoModule;
import com.titaniamlabs.log.Log;
import org.testng.util.Strings;
import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;
import pl.allegro.tech.embeddedelasticsearch.JavaHomeOption;
import pl.allegro.tech.embeddedelasticsearch.PopularProperties;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Embedded {

    /*
     * These properties are defined in build.gradle. Please refer to build.gradle
     * to adjust/tweak the values.
     *
     * java_home is a system dependent. So, every developer has to manually specify the
     * right directory. TODO: Make this system independent
     */
    public static final String PROPERTY_JAVA_HOME = "titaniam.embedded-elastic.java_home";
    public static final String PROPERTY_PLUGIN_LOCATION = "titaniam.embedded-elastic.plugin-location";
    public static final String PROPERTY_PATH_LOGS = "titaniam.embedded-elastic.path-logs";

    private static Log log = Log.getLogger(Embedded.class);

    private static EmbeddedElastic embeddedElastic;

    public void start() throws IOException, InterruptedException  {

        if(embeddedElastic!=null) {
            return;
        }

        log.debug("Starting EmbeddedElastic. Java Version: {}",System.getProperty("java.version"));

        embeddedElastic = EmbeddedElastic.builder()
                .withElasticVersion("6.2.2")
                .withSetting(PopularProperties.TRANSPORT_TCP_PORT, 9350)
                .withSetting(PopularProperties.CLUSTER_NAME, "titaniam")
                .withSetting("logger.org.elasticsearch", "info")
                .withSetting("logger.com.titaniamlabs", "trace")
                .withSetting("path.logs", locateFileAtSystemProperty(PROPERTY_PATH_LOGS))
                .withStartTimeout(60, TimeUnit.SECONDS)
                .withJavaHome(findJavaHome())
                .withPlugin("file://"+titaniamPluginLocation())
                .withCleanInstallationDirectoryOnStop(false)
                .withEsJavaOpts(esJavaOpts())
//                .withEsJavaOpts("-Xms256m -Xmx256m")
//                .withEsJavaOpts("-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005")
                .build()
                .start();

                log.debug("Successfully started embedded elastic");
    }

    public String elasticJavaPath() {
        return locateFileAtSystemProperty(PROPERTY_JAVA_HOME);
    }

    public String locateFileAtSystemProperty(String propertyName) {

        String value = System.getProperty(propertyName);

        if(Strings.isNullOrEmpty(value)) {
            throw new IllegalArgumentException("Missing \""+propertyName+"\" in build.gradle");
        }

        if(!(new File(value).exists())) {
            throw new IllegalArgumentException("File value for \""+propertyName+"\" in build.gradle is invalid. No such file or directory: "+value);
        }

        return value;
    }

    public String esJavaOpts() {

        String mockSetting = System.getProperty(CryptoModule.TITANIAM_CRYPTOENGINE);

        if(Strings.isNotNullAndNotEmpty(mockSetting)) {
            return "-D"+CryptoModule.TITANIAM_CRYPTOENGINE+"="+mockSetting;
        } else {
            return "";
        }

    }

    /**
     * This will find the override, if present use the override. Else, will use
     * system java path.
     *
     * @return
     */
    public JavaHomeOption findJavaHome() {

        if(Strings.isNotNullAndNotEmpty(System.getProperty(PROPERTY_JAVA_HOME))) {
            return JavaHomeOption.path(elasticJavaPath());
        } else {
            // we could use test case java home as well. not sure which one
            // would be a default for most developer envs..
            return JavaHomeOption.inheritTestSuite();
        }
    }

    public String titaniamPluginLocation() {
        return locateFileAtSystemProperty(PROPERTY_PLUGIN_LOCATION);
    }

    public void stop() {

        if(embeddedElastic!=null) {
            embeddedElastic.stop();
            log.debug("Embedded elastic shutdown.");
            embeddedElastic = null;
        }
    }

}
