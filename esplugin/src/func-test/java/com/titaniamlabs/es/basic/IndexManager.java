package com.titaniamlabs.es.basic;

import com.titaniamlabs.log.Log;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.admin.indices.refresh.RefreshResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.ingest.PutPipelineRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.bytes.BytesArray;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

public class IndexManager {

    private static final Log log = Log.getLogger(IndexManager.class);

    public static final String INDEX_NAME_USERS = "users";
    public static final String INDEX_NAME_USERS_NESTED = "users_nested";
    public static final String PIPELINE = "tangle-pipeline";

    public static final String TITANIAM_INGEST_PROCESSOR = "titaniam_ingest_processor";

    public static final String FIELD_NAME_SSN = "social";
    public static final String FIELD_NAME_FIRST_NAME = "first_name";
    public static final String FIELD_NAME_LAST_NAME = "last_name";
    public static final String FIELD_NAME_GENDER = "gender";
    public static final String FIELD_NAME_SENSITIVE = "sensitive";

    private RestHighLevelClient client;

    public IndexManager(RestHighLevelClient client) {
        this.client = client;
    }

    /**
     * curl -X PUT "localhost:9200/_ingest/pipeline/tangle-pipeline" -H 'Content-Type: application/json' -d'
     * {
     *   "description": "A pipeline to do whatever",
     *   "processors": [
     *     {
     *       "titaniam_ingest_processor" : {
     *         "field" : ["ssn"]
     *       }
     *     }
     *   ]
     * }'
     * @return - true if the request was acked by elastic.
     * @throws Exception
     */
    public boolean initializePipeline() throws Exception {

        assert client!=null : "Client not set";

        XContentBuilder builder = XContentFactory.jsonBuilder();

        builder.startObject();
        {
            builder.field("description", "A pipeline to do whatever");
            builder.startArray("processors")
                    .startObject()
                    .startObject(TITANIAM_INGEST_PROCESSOR)
                    .startArray("field")
                    .value(FIELD_NAME_SSN)
                    .endArray()
                    .endObject()
                    .endObject()
                    .endArray();
        }
        builder.endObject();
        builder.close();

        String json = Strings.toString(builder);
        log.debug(json);

        PutPipelineRequest request = new PutPipelineRequest(
                PIPELINE,
                new BytesArray(json.getBytes(StandardCharsets.UTF_8)),
                XContentType.JSON
        );

        log.debug("Starting PutPipelineRequest");

        AcknowledgedResponse response = client.ingest().putPipeline(request, RequestOptions.DEFAULT);

        log.debug("Done PutPipelineRequest. isAcknowledged: {}", response.isAcknowledged());

        return response.isAcknowledged();
    }

    /**
     * curl -X PUT "http://localhost:9200/users" -H 'Content-Type: application/json' -d'
     * {
     *     "mappings": {
     *       "_doc": {
     *         "properties": {
     *             "first_name": {
     *                 "type": "keyword"
     *             },
     *             "last_name": {
     *                 "type": "keyword"
     *             },
     *             "gender": {
     *                 "type": "keyword"
     *             },
     *             "ssn": {
     *                 "type": "tangled_text",
     *                 "index_prefixes": {
     *                     "min_chars": 6,
     *                     "max_chars": 18
     *                 }
     *             }
     *         }
     *       }
     *     }
     * }
     *
     * @return - true if the request was acked by elastic.
     * @throws Exception
     */
    public boolean initializeIndex() throws IOException {

        log.debug("initializeIndex");

        XContentBuilder builder = XContentFactory.jsonBuilder();

        builder.startObject()
                .startObject("properties")

                .startObject(FIELD_NAME_FIRST_NAME)
                .field("type","keyword")
                .endObject()

                .startObject(FIELD_NAME_LAST_NAME)
                .field("type","keyword")
                .endObject()

                .startObject(FIELD_NAME_GENDER)
                .field("type","keyword")
                .endObject()

                .startObject(FIELD_NAME_SSN)
                .field("type","tangled_text")
                .startObject("index_prefixes")
                .field("min_chars",6)
                .field("max_chars", 18)
                .endObject()
                .endObject()

                .endObject()
                .endObject();

        builder.close();

        CreateIndexRequest request = new CreateIndexRequest(INDEX_NAME_USERS);
        request.mapping("_doc", builder);
        request.timeout(TimeValue.timeValueMinutes(1));

        log.debug("CreateIndexRequest json: {} ",Strings.toString(builder));

        CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);

        log.debug("Done CreateIndexResponse. createIndexResponse.isAcknowledged() - {}", createIndexResponse.isAcknowledged());

        return createIndexResponse.isAcknowledged();
    }

    /**
     * Creates one additional level of nesting for testing nesting use cases.
     * index name created is in <tt>INDEX_NAME_USERS_NESTED</tt>
     * @return
     * @throws IOException
     */
    public boolean initializeIndexNested() throws IOException {

        log.debug("initializeIndex");

        XContentBuilder builder = XContentFactory.jsonBuilder();

        builder.startObject()
                .startObject("properties")

                .startObject(FIELD_NAME_FIRST_NAME)
                .field("type","keyword")
                .endObject()

                .startObject(FIELD_NAME_LAST_NAME)
                .field("type","keyword")
                .endObject()

                .startObject(FIELD_NAME_GENDER)
                .field("type","keyword")
                .endObject()

                .startObject(FIELD_NAME_SENSITIVE)
                .startObject("properties")

                .startObject(FIELD_NAME_SSN)
                .field("type","tangled_text")
                .startObject("index_prefixes")
                .field("min_chars",6)
                .field("max_chars", 18)
                .endObject()
                .endObject()

                .endObject()
                .endObject()

                .endObject()
                .endObject();

        builder.close();

        CreateIndexRequest request = new CreateIndexRequest(INDEX_NAME_USERS_NESTED);
        request.mapping("_doc", builder);
        request.timeout(TimeValue.timeValueMinutes(1));

        log.debug("CreateIndexRequest json: {} ",Strings.toString(builder));

        CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);

        log.debug("Done CreateIndexResponse. createIndexResponse.isAcknowledged() - {}", createIndexResponse.isAcknowledged());

        return createIndexResponse.isAcknowledged();
    }
    /**
     *
     * @param firstName
     * @param lastName
     * @param gender
     * @param social
     * @return id - of the new document created
     * @throws IOException
     */
    public String putUser(String firstName, String lastName, String gender, String social) throws IOException {

        log.debug("putUser: {}, {}, {}, {}", firstName, lastName, gender, social);

        IndexRequest indexRequest = new IndexRequest(INDEX_NAME_USERS);
        indexRequest.setPipeline(PIPELINE);
        indexRequest.type("_doc");

        indexRequest.source(
                FIELD_NAME_FIRST_NAME, firstName,
                FIELD_NAME_LAST_NAME, lastName,
                FIELD_NAME_GENDER, gender,
                FIELD_NAME_SSN, social
        );

        IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);

        assertNotNull(indexResponse);
        assertEquals(indexResponse.status(), RestStatus.CREATED);

        return indexResponse.getId();
    }

    /**
     *
     * @param firstName
     * @param lastName
     * @param gender
     * @param social
     * @return
     * @throws IOException
     */
    public String putUserNested(String firstName, String lastName, String gender, String social) throws IOException {

        log.debug("putUser: {}, {}, {}, {}", firstName, lastName, gender, social);

        IndexRequest indexRequest = new IndexRequest(INDEX_NAME_USERS_NESTED);
        indexRequest.setPipeline(PIPELINE);
        indexRequest.type("_doc");

        Map<String, Object> nested = new HashMap();
        nested.put(FIELD_NAME_SSN, social);

        indexRequest.source(
                FIELD_NAME_FIRST_NAME, firstName,
                FIELD_NAME_LAST_NAME, lastName,
                FIELD_NAME_GENDER, gender,
                FIELD_NAME_SENSITIVE, nested
        );

        IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);

        assertNotNull(indexResponse);
        assertEquals(indexResponse.status(), RestStatus.CREATED);

        return indexResponse.getId();
    }

    /**
     *
     * @return
     */
    public Map<String,Object> getUserById(String id) throws IOException {
        return getUserById(id, INDEX_NAME_USERS);
    }

    public Map<String,Object> getUserById(String id, String indexName) throws IOException {

        log.debug("getUserById: {}", id);

        GetRequest getRequest = new GetRequest(
                indexName,
                "_all",
                id
        );

        GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
        assertNotNull(getResponse);
        assertNotNull(getResponse.getSourceAsMap());

        return getResponse.getSourceAsMap();
    }

    /**
     *
     * @return
     * @throws IOException
     */
    public boolean deleteIndex() throws IOException {

        DeleteIndexRequest request = new DeleteIndexRequest(INDEX_NAME_USERS);

        try {
            DeleteIndexResponse deleteIndexResponse = client.indices().delete(request, RequestOptions.DEFAULT);

            log.debug("Deleted index: users - {}", deleteIndexResponse.isAcknowledged());

            return deleteIndexResponse.isAcknowledged();

        } catch (ElasticsearchStatusException e) {

            log.debug("Nothing to delete!? - {}", e.getMessage());

            return false;
        }
    }

    public SearchHits findAllUsers() throws IOException {

        log.debug("findAllUsers");

        SearchRequest searchRequest = new SearchRequest(INDEX_NAME_USERS);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());

        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        assertNotNull(searchResponse);
        return searchResponse.getHits();
    }

    public void refreshIndex() throws IOException {
        refreshIndex(INDEX_NAME_USERS);
    }

    public void refreshIndex(String indexName) throws IOException {

        log.debug("refreshIndex");

        RefreshRequest request = new RefreshRequest(indexName);
        RefreshResponse refreshResponse = client.indices().refresh(request, RequestOptions.DEFAULT);
        assertNotNull(refreshResponse);
        assertEquals(refreshResponse.getStatus(), RestStatus.OK);
    }

    public SearchHits findAllUserBySocialPrefix(String prefix) throws IOException {
        return findAllUserBySocialPrefix(prefix, INDEX_NAME_USERS);
    }

    public SearchHits findAllUserBySocialPrefix(String prefix, String indexName) throws IOException {
        return findAllUserByPrefix(FIELD_NAME_SSN, prefix, indexName);
    }

    public SearchHits findAllUserByPrefix(String fieldName, String prefix, String indexName) throws IOException {

        log.debug("findAllUserBySocialPrefix - social-prefix:{}", prefix);

        PrefixQueryBuilder prefixQueryBuilder = QueryBuilders.prefixQuery(fieldName, prefix);
        return executeSearch(indexName, prefixQueryBuilder);
    }

    public SearchHits executeSearch(String indexName, QueryBuilder queryBuilder) throws IOException {

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(queryBuilder);

        SearchRequest searchRequest = new SearchRequest(indexName);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        assertNotNull(searchResponse);
        return searchResponse.getHits();
    }

    public SearchHits findAllUsersByTerm(String indexName, String fieldName, String termValue) throws IOException {

        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery(fieldName, termValue);
        return executeSearch(indexName, termQueryBuilder);
    }

    public SearchHits findAllUsersByWildcard(String indexName, String fieldName, String termValue) throws IOException {

        WildcardQueryBuilder wildcardQueryBuilder = QueryBuilders.wildcardQuery(fieldName, termValue);
        return executeSearch(indexName, wildcardQueryBuilder);
    }

    /**
     * Deletes the list of documents by IDs
     * @param indexName
     * @param ids
     * @throws Exception
     */
    public void deleteRecords(String indexName, String[] ids) throws Exception {

        for(String id: ids) {
            DeleteRequest deleteRequest = new DeleteRequest(indexName,
                    "_doc",
                    id);
            deleteRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.NONE);

            DeleteResponse deleteResponse = client.delete(deleteRequest, RequestOptions.DEFAULT);
            assertEquals(deleteResponse.status(), RestStatus.OK);
        }

        refreshIndex(indexName);

    }


}
