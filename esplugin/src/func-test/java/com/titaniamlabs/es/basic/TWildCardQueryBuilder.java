package com.titaniamlabs.es.basic;

import com.titaniamlabs.log.Log;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.WildcardQueryBuilder;

import java.io.IOException;

public class TWildCardQueryBuilder extends WildcardQueryBuilder {

    private static final Log log = Log.getLogger(TWildCardQueryBuilder.class);

    public TWildCardQueryBuilder(String fieldName, String value) {
        super(fieldName, value);
    }

    @Override
    public boolean isFragment() {
        return false;
    }

    @Override
    protected void doXContent(XContentBuilder builder, Params params) throws IOException {

        log.debug("------------||||||------------");
        builder.startObject("wildcard");
        builder.startObject(fieldName());

        builder.field("value", value());

        builder.endObject();
        builder.endObject();
        //do nothing..
    }
}
