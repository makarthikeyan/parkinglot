## Functional Test Specs
* Any file in this directory starting with `titaniam-spec` is considered a functional test specification file.
* All of these files will be parsed and executed.
* Each test specification file is made up to a series of actions.
* Actions will be executed in the order in which it is specified in the file.

## Where are actions run?
* Embedded elasticsearch will be started. All urls will be run against the embedded server.
* If you specify absolute url in actions, it will skip the embedded server. Actions will be run against the server at the endpoint. 

## How to run?
* Checkout code
* Goto top level `esplugin` directory
* `./gradlew dumbledore` 
* Supported Java version(s): 9,10,11,12 (??)
* TL;DR
   * Embedded elasticsearch logs in top level `esplugin/logs` directory

## Checking-in your changes
TODO


## Action Definition
* Each action is nothing but a curl call specified in a format that can executed and response asserted. 
* Each action has meta fields at the top level. This include

| Name   | Description      |
|----------|-------------|
| `#comment` | User readable string, for describing the action itself. |
| `#method` | http method for this call   |
| `#url` | url to call. Can be absolute or just path. If it is just path, then `http://localhost:9200` will be prepended. |
| `#body` | JSON body to be sent with the http request. Can be empty `{}`. |
| `#expected` | Will be used to assert against the response from server. Can be subset of actual fields. Can be empty - `{}`. |

* Note that all fields are required. Some fields can be empty (or `{}`).

* Example:

```
        {
          "actions": [
            {
              "#comment": "-------------create a pipeline------------",
              "#method": "PUT",
              "#url": "/_ingest/pipeline/k3ip-cve-pipeline",
              "#body": {
                "description": "For IP CVE index",
                "processors": [
                  {
                    "titaniam_ingest_processor" : {
                      "field" : ["cve","ip"]
                    }
                  }
                ]
              },
              "#expected": {
                "acknowledged": true
              }
            }
          ]
        }
```

## Debugging failures
* Look at all the cryptic log messages as each action is executed.
* They will provide a trace of all actions and additional details from the elasticserver response.  

## Dos/Don'ts
* It is usual to create dependency across actions within same file.
* It is UNusual to create dependency across files.
* Keep each logical test suite in it's own separate file
* Add `#comment` field to describe the intent of the actions. This will translate your Greek to English for others to read.
* Refresh the index before querying, when you do create/update/delete operations. Ex:
```
        {
          "#comment": "-------------Refresh, else, queries will fail------------",
          "#method": "GET",
          "#url": "/k3ipcve/_refresh",
          "#body": {},
          "#expected": {}
        }
```
* Have fun!