package com.titaniamlabs.es;

import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.crypto.CryptoModule;
import com.titaniamlabs.es.ingest.ProcessorFactory;
import com.titaniamlabs.es.ingest.TitaniamProcessor;
import com.titaniamlabs.es.mapper.CryptoFieldType;
import com.titaniamlabs.es.mapper.TypeConfigurationParser;
import com.titaniamlabs.es.search.TitaniamFetchSubPhase;
import com.titaniamlabs.es.search.TitaniamTransportInterceptor;
import com.titaniamlabs.es.search.TitaniamWildcardQueryBuilder;
import com.titaniamlabs.log.Log;
import org.elasticsearch.common.collect.MapBuilder;
import org.elasticsearch.common.inject.Guice;
import org.elasticsearch.common.inject.Injector;
import org.elasticsearch.common.io.stream.NamedWriteableRegistry;
import org.elasticsearch.common.util.concurrent.ThreadContext;
import org.elasticsearch.index.mapper.Mapper;
import org.elasticsearch.ingest.Processor;
import org.elasticsearch.plugins.*;
import org.elasticsearch.search.fetch.FetchSubPhase;
import org.elasticsearch.transport.TransportInterceptor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;

/**
 * This is the hook into the elasticsearch world. The fully qualified name of
 * this class is included in <code>src/main/resources/plugin-descriptor.properties</code>.
 *
 * This class and all the classes instantiated from here are an artifact of how
 * elasticsearch works. There is very little control we have on the flow, although
 * we still control how the code is organized.
 *
 */
public class Plugins extends Plugin implements IngestPlugin, MapperPlugin, SearchPlugin, NetworkPlugin {

    private static final Log log = Log.getLogger(Plugins.class);

    // Impl Note:
    // IOC has been done without much-thinking on how to integrate with
    // ES. Need to find a better way to do this than direct creation of injectors all
    // over the place
    private Injector injector;

    public Plugins() {
        this.injector = Guice.createInjector(new CryptoModule());
    }

    /**
     * This processor handles the document ingestion. It tangles all fields
     * marked as input to this processor. Read more about how this works in
     * the processor doc.
     *
     * @param parameters
     * @return
     */
    @Override
    public Map<String, Processor.Factory> getProcessors(Processor.Parameters parameters) {

        log.debug("Plugins.getProcessors()");

        ProcessorFactory factory = new ProcessorFactory();
        factory.setCryptoEngine(this.injector.getInstance(CryptoEngine.class));

        return MapBuilder.<String, Processor.Factory>newMapBuilder()
                .put(TitaniamProcessor.TYPE, factory)
                .immutableMap();
    }

    /**
     * This is used to untangle the document on the way out of search queries. The fields
     * that are marked as tangled will be replaced with untangled fields when search is
     * completed and results are sent back.
     *
     * @param context
     * @return
     */
    @Override
    public List<FetchSubPhase> getFetchSubPhases(FetchPhaseConstructionContext context) {

        // To return clearText in the response use TitaniamFetchSubPhase instead of empty_list
        TitaniamFetchSubPhase titaniamFetchSubPhase = new TitaniamFetchSubPhase();
        titaniamFetchSubPhase.setCryptoEngine(injector.getInstance(CryptoEngine.class));

        return singletonList(titaniamFetchSubPhase);
        // Uncomment the below line to return the tangled text in the response of tangled field
    }

    /**
     * The mappers are responsible for creating the fragments from the original document
     * that was processed through <code>TitaniamProcessor</code>. Most of the search logic
     * flows through the mapper and their associated type class.
     *
     * @return
     */
    @Override
    public Map<String, Mapper.TypeParser> getMappers() {

        log.debug("Plugins.getMappers()");

        // return Collections.singletonMap(TitaniamFieldMapper.CONTENT_TYPE, new TitaniamFieldMapper.TypeParser());
        TypeConfigurationParser parser = new TypeConfigurationParser();
        parser.setCryptoEngine(injector.getInstance(CryptoEngine.class));

        return Collections.singletonMap(CryptoFieldType.TYPE_NAME, parser);
    }

    /**
     * Wildcard queries are special in elasticsearch (don't know why). This flow hooks the wilcard
     * query builders. Also, this alone is not enough for wildcard queries to be executed.
     *
     * Well, I am not even sure if this is needed..
     *
     * @return
     */
    @Override
    public List<QuerySpec<?>> getQueries() {

        log.debug("Plugins.getQueries()");

        return singletonList(new QuerySpec<>(TitaniamWildcardQueryBuilder.NAME, TitaniamWildcardQueryBuilder::new, TitaniamWildcardQueryBuilder::fromXContent));
    }

    /**
     * This is required to hook-up wildcard queries. Without this hook, wildcard queries will not
     * be executed by elastic when a search comes in.
     *
     * This is a bit around-about way of handling wild-card queries. I am surprised that this even works
     * as expected.
     *
     * @param namedWriteableRegistry
     * @param threadContext
     * @return
     */
    @Override
    public List<TransportInterceptor> getTransportInterceptors(NamedWriteableRegistry namedWriteableRegistry,
                                                               ThreadContext threadContext) {
        log.debug("Plugins.getTransportInterceptors()");

        return Collections.singletonList(new TitaniamTransportInterceptor(namedWriteableRegistry, threadContext));
    }


}