package com.titaniamlabs.es.mapper;

import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.log.Log;
import org.elasticsearch.common.xcontent.support.XContentMapValues;
import org.elasticsearch.index.mapper.DocumentMapperParser;
import org.elasticsearch.index.mapper.Mapper;
import org.elasticsearch.index.mapper.MapperParsingException;
import org.elasticsearch.index.mapper.TypeParsers;

import javax.inject.Inject;
import java.util.Map;
import java.util.Iterator;

/**
 * Factory for constructing the Builder for FieldMapper
 */
public class TypeConfigurationParser implements Mapper.TypeParser {

    private static final Log log = Log.getLogger(TypeConfigurationParser.class);

    private CryptoEngine cryptoEngine;

    public TypeConfigurationParser() {
        log.debug("new TypeConfigurationParser()");
    }

    @Inject
    public void setCryptoEngine(CryptoEngine cryptoEngine) {
        this.cryptoEngine = cryptoEngine;
    }

    @Override
    public CryptoFieldMapperBuilder parse(String fieldName, Map<String, Object> node, ParserContext parserContext) throws MapperParsingException {

        log.debug("Begin parse: {}", fieldName);

        CryptoFieldMapperBuilder builder = new CryptoFieldMapperBuilder(fieldName,
                Defaults.FIELD_TYPE,
                Defaults.FIELD_TYPE);

        builder.setCryptoEngine(this.cryptoEngine);

        /*
            Impl Note: Copy/pasted from TextFieldMapper.TypeParser
        */
        if(parserContext.getIndexAnalyzers()!=null) {
            builder.fieldType().setIndexAnalyzer(parserContext.getIndexAnalyzers().getDefaultIndexAnalyzer());
            builder.fieldType().setSearchAnalyzer(parserContext.getIndexAnalyzers().getDefaultSearchAnalyzer());
            builder.fieldType().setSearchQuoteAnalyzer(parserContext.getIndexAnalyzers().getDefaultSearchQuoteAnalyzer());
        }

        TypeParsers.parseTextField(builder, fieldName, node, parserContext);
        Iterator<Map.Entry<String, Object>> iterator = node.entrySet().iterator();

        while(iterator.hasNext()) {

            Map.Entry<String, Object> entry = iterator.next();
            String propName = entry.getKey();
            Object propNode = entry.getValue();

            if (propName.equals("position_increment_gap")) {

                int newPositionIncrementGap = XContentMapValues.nodeIntegerValue(propNode, -1);
                builder.positionIncrementGap(newPositionIncrementGap);
                iterator.remove();

            } else if (propName.equals("fielddata")) {

                builder.fielddata(XContentMapValues.nodeBooleanValue(propNode, "fielddata"));
                iterator.remove();

            } else if (propName.equals("eager_global_ordinals")) {

                builder.eagerGlobalOrdinals(XContentMapValues.nodeBooleanValue(propNode, "eager_global_ordinals"));
                iterator.remove();

            } else if (propName.equals("fielddata_frequency_filter")) {

                Map<?, ?> frequencyFilter = (Map<?, ?>) propNode;
                double minFrequency = XContentMapValues.nodeDoubleValue(frequencyFilter.remove("min"), 0);
                double maxFrequency = XContentMapValues.nodeDoubleValue(frequencyFilter.remove("max"), Integer.MAX_VALUE);
                int minSegmentSize = XContentMapValues.nodeIntegerValue(frequencyFilter.remove("min_segment_size"), 0);
                builder.fielddataFrequencyFilter(minFrequency, maxFrequency, minSegmentSize);
                DocumentMapperParser.checkNoRemainingFields(propName, frequencyFilter, parserContext.indexVersionCreated());
                iterator.remove();

            } else if (propName.equals("index_prefixes")) {

                log.debug("index_prefixes found");

                Map<?, ?> indexPrefix = (Map<?, ?>) propNode;

                int minChars = XContentMapValues.nodeIntegerValue(indexPrefix.remove("min_chars"),
                        Defaults.INDEX_PREFIX_MIN_CHARS);

                int maxChars = XContentMapValues.nodeIntegerValue(indexPrefix.remove("max_chars"),
                        Defaults.INDEX_PREFIX_MAX_CHARS);

                int maxFragments = XContentMapValues.nodeIntegerValue(indexPrefix.remove("max_fragments"),
                        Defaults.INDEX_PREFIX_MAX_FRAGMENTS);

                builder.indexPrefixes(minChars, maxChars);
                builder.setMaxIndexFragments(maxFragments);

                DocumentMapperParser.checkNoRemainingFields(propName, indexPrefix, parserContext.indexVersionCreated());
                iterator.remove();

            } else if (propName.equals("index_phrases")) {

                builder.indexPhrases(XContentMapValues.nodeBooleanValue(propNode, "index_phrases"));
                iterator.remove();

            }
        }

        setDefaults(builder);

        // If the user specified invalid values, then, this will throw validation
        // errors.
        builder.doValidate();

        return builder;
    }

    /**
     * This is to ensure that if the user has entirely skipped a
     * section, we will still honor sensible defaults.
     *
     * @param builder
     */
    private void setDefaults(CryptoFieldMapperBuilder builder) {

        //If the user did not specify any value, then, it will be defaulted.
        if(builder.getMinPrefixChars()==0) {
            builder.setMinPrefixChars(Defaults.INDEX_PREFIX_MIN_CHARS);
        }

        if(builder.getMaxPrefixChars()==0) {
            builder.setMaxPrefixChars(Defaults.INDEX_PREFIX_MAX_CHARS);
        }

        if(builder.getMaxIndexFragments()==0) {
            builder.setMaxIndexFragments(Defaults.INDEX_PREFIX_MAX_FRAGMENTS);
        }
    }

}
