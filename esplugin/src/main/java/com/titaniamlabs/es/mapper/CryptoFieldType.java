package com.titaniamlabs.es.mapper;

import com.titaniamlabs.log.Log;
import com.titaniamlabs.util.MapperUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.elasticsearch.index.fielddata.IndexFieldData;
import org.elasticsearch.index.fielddata.plain.PagedBytesIndexFieldData;
import org.elasticsearch.index.mapper.FieldNamesFieldMapper;
import org.elasticsearch.index.mapper.StringFieldType;
import org.elasticsearch.index.query.QueryShardContext;

import javax.inject.Inject;
import java.util.Map;

public class CryptoFieldType extends StringFieldType {

    private static final Log log = Log.getLogger(CryptoFieldType.class);

    public static final String TYPE_NAME = "tangled_text";

    private boolean fielddata;
    private double fielddataMinFrequency;
    private double fielddataMaxFrequency;
    private int fielddataMinSegmentSize;
    private boolean indexPhrases = false;
    private PrefixFieldType prefixFieldType;

    private TitaniamQueryBuilder queryBuilder;

    // This is the unique property for this type. Everything else is a copy of TextFieldMapper
    private Map<String, IndexFragmentFieldMapper> fragmentedFieldMapperMap;

    public CryptoFieldType() {
        super();

        setTokenized(true);
        setOmitNorms(true);

        fielddata = false;
        fielddataMinFrequency = Defaults.FIELDDATA_MIN_FREQUENCY;
        fielddataMaxFrequency = Defaults.FIELDDATA_MAX_FREQUENCY;
        fielddataMinSegmentSize = Defaults.FIELDDATA_MIN_SEGMENT_SIZE;

    }

    @Inject
    public void setQueryBuilder(TitaniamQueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    public CryptoFieldType(CryptoFieldType reference) {
        super(reference);

        this.fielddata = reference.fielddata;
        this.fielddataMinFrequency = reference.fielddataMinFrequency;
        this.fielddataMaxFrequency = reference.fielddataMaxFrequency;
        this.fielddataMinSegmentSize = reference.fielddataMinSegmentSize;
        this.indexPhrases = reference.indexPhrases;

        if(reference.prefixFieldType!=null) {
            this.prefixFieldType = reference.prefixFieldType.clone();
        }
    }


    @Override
    public CryptoFieldType clone() {
        return new CryptoFieldType(this);
    }

    @Override
    /**
     * @returns - {TYPE_NAME}
     */
    public String typeName() {
        return TYPE_NAME;
    }

    public void setFielddata(boolean fielddata) {
        checkIfFrozen();
        this.fielddata = fielddata;
    }

    public boolean fielddata() {
        return fielddata;
    }

    public double fielddataMinFrequency() {
        return fielddataMinFrequency;
    }

    public void setFielddataMinFrequency(double fielddataMinFrequency) {
        checkIfFrozen();
        this.fielddataMinFrequency = fielddataMinFrequency;
    }

    public double fielddataMaxFrequency() {
        return fielddataMaxFrequency;
    }

    public void setFielddataMaxFrequency(double fielddataMaxFrequency) {
        checkIfFrozen();
        this.fielddataMaxFrequency = fielddataMaxFrequency;
    }

    public int fielddataMinSegmentSize() {
        return fielddataMinSegmentSize;
    }

    public void setFielddataMinSegmentSize(int fielddataMinSegmentSize) {
        checkIfFrozen();
        this.fielddataMinSegmentSize = fielddataMinSegmentSize;
    }

    void setPrefixFieldType(PrefixFieldType prefixFieldType) {
        checkIfFrozen();
        this.prefixFieldType = prefixFieldType;
    }

    void setIndexPhrases(boolean indexPhrases) {
        checkIfFrozen();
        this.indexPhrases = indexPhrases;
    }

    public PrefixFieldType getPrefixFieldType() {
        return this.prefixFieldType;
    }

    public void setFragmentedFieldMapperMap(Map<String, IndexFragmentFieldMapper> fragmentedFieldMapperMap) {
        this.fragmentedFieldMapperMap = fragmentedFieldMapperMap;
    }

    public boolean isIndexPhrases() {
        return indexPhrases;
    }

    //=======query rewrites=========//

    @Override
    public Query prefixQuery(String clearText, MultiTermQuery.RewriteMethod method, QueryShardContext context) {

        String prefixField = MapperUtils.generateForwardFieldName(name());

        return queryBuilder.prefixQuery(prefixField, clearText, method, context);
    }

    @Override
    public Query existsQuery(QueryShardContext context) {

        log.debug("existsQuery");

        if (omitNorms()) {
            return new TermQuery(new Term(FieldNamesFieldMapper.NAME, name()));
        } else {
            return new NormsFieldExistsQuery(name());
        }
    }


    @Override
    public Query termQuery(Object value, QueryShardContext context) {

        log.debug("termQuery");

        failIfNotIndexed();

        return queryBuilder.termQuery(name(), value, context);
    }

    public Query wildCardQuery(String value, MultiTermQuery.RewriteMethod method, QueryShardContext context) {

        log.debug("wildCardQuery");

        failIfNotIndexed();

        return queryBuilder.wildCardQuery(name(), value, method, context);
    }

    public IndexFieldData.Builder fielddataBuilder(String fullyQualifiedIndexName) {
        if (!fielddata) {
            throw new IllegalArgumentException("Fielddata is disabled on text fields by default. Set fielddata=true on [" + name()
                    + "] in order to load fielddata in memory by uninverting the inverted index. Note that this can however "
                    + "use significant memory. Alternatively use a keyword field instead.");
        }
        return new PagedBytesIndexFieldData.Builder(fielddataMinFrequency, fielddataMaxFrequency, fielddataMinSegmentSize);
    }

    @Override
    public String toString() {

        return "CryptoFieldType{" +
                "fielddata=" + fielddata +
                ", fielddataMinFrequency=" + fielddataMinFrequency +
                ", fielddataMaxFrequency=" + fielddataMaxFrequency +
                ", fielddataMinSegmentSize=" + fielddataMinSegmentSize +
                ", indexPhrases=" + indexPhrases +
                '}';
    }
}
