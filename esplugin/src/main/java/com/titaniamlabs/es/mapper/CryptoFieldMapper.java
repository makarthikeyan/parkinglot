package com.titaniamlabs.es.mapper;

import com.google.common.base.Strings;
import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.crypto.CryptoResult;
import com.titaniamlabs.log.Log;
import com.titaniamlabs.util.MapperUtils;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexableField;
import org.elasticsearch.common.collect.Iterators;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.mapper.FieldMapper;
import org.elasticsearch.index.mapper.MappedFieldType;
import org.elasticsearch.index.mapper.Mapper;
import org.elasticsearch.index.mapper.ParseContext;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

/**
 * The class responsible for creating data for indexing.
 * Created by <code>CryptoFieldMapperBuilder</code>. There is an instance
 * for each of the field that needs to be encrypted. Upon indexing of a
 * document, the <code>parseCreateField</code> is called.
 */
public class CryptoFieldMapper extends FieldMapper {

    private static final Log log = Log.getLogger(CryptoFieldMapper.class);

    private int positionIncrementGap;
    private int minPrefixChars;
    private int maxPrefixChars;
    private int maxIndexFragments;
    private Map<String, IndexFragmentFieldMapper> fragmentFields;

    private CryptoEngine cryptoEngine;

    public CryptoFieldMapper(String simpleName,
                             MappedFieldType fieldType,
                             MappedFieldType defaultFieldType,
                             int positionIncrementGap,
                             int minPrefixChars,
                             int maxPrefixChars,
                             Map<String, IndexFragmentFieldMapper> fragmentFields,
                             Settings indexSettings,
                             MultiFields multiFields,
                             int maxIndexFragments,
                             CopyTo copyTo) {

        super(simpleName, fieldType, defaultFieldType, indexSettings, multiFields, copyTo);

        this.minPrefixChars = minPrefixChars;
        this.maxPrefixChars = maxPrefixChars;
        this.positionIncrementGap = positionIncrementGap;
        this.fragmentFields = fragmentFields;
        this.maxIndexFragments = maxIndexFragments;

        log.debug("new CryptoFieldMapper()");

        if (fieldType().indexOptions() == IndexOptions.NONE) {
            throw new IllegalArgumentException("Cannot enable fielddata on a [text] field that is not indexed: [" + name() + "]");
        }

        log.debug("new CryptoFieldMapper() - done");
    }

    /**
     *
     * @param context
     * @param fields
     * @throws IOException
     */
    @Override
    protected void parseCreateField(ParseContext context, List<IndexableField> fields) throws IOException {

        assert cryptoEngine!=null : "cryptoEngine is null. initialization failed";

        final String value = getIndexString(context);

        if(!doValidateConfiguration(value)) {
            return;
        }

        log.debug("encrypted text: {}", value);

        //The value in the doc has been encrypted in the ingest plugin.
        //The first step is to decrypt it.
        String clearText = cryptoEngine.decrypt(value, true);

        log.debug("decrypted text: {}", clearText);

        CryptoResult cryptoResult = cryptoEngine.encrypt(clearText);

        log.debug("CryptoResult: {}", cryptoResult);

        // This uses fieldType().name() in the original code. How does that even work??
        String parentFieldName = simpleName();

        //TODO: Hack Alert! toLowercase her is added to counter
        // for not analyzing this field, that would make it
        // split into fragments. Ideally, there is should be a
        // language neutral way to do this..
        addField(MapperUtils.generateForwardFieldName(parentFieldName),
                MapperUtils.toLowercase(cryptoResult.getForward()),
                fields);

        addField(MapperUtils.generateReverseFieldName(parentFieldName),
                MapperUtils.toLowercase(cryptoResult.getReverse()),
                fields);

        if (fieldType().omitNorms()) {
            createFieldNamesField(context, fields);
        }

        generateFragmentFields(fields,
                cryptoResult.getForward(),
                parentFieldName);

        log.debug("fragment generation done for {} => {}", parentFieldName, clearText);
    }

    /**
     * Generates the fields for all the fragments that needs to be indexed.
     *
     * @param fields
     * @param value
     * @param parentFieldName
     */
    public void generateFragmentFields(List<IndexableField> fields, String value, String parentFieldName) {

        String[] valueFragments = MapperUtils.splitToNChar(value, minPrefixChars, maxPrefixChars);

        if((valueFragments == null) || Strings.isNullOrEmpty(parentFieldName)) {
            log.warn("Invalid input. value: {}, parentFieldName: {}",
                    value,
                    parentFieldName);
            return;
        }

        log.debug("Total fragments: {}", valueFragments.length);

        for (int i = 0; i < valueFragments.length; i++) {

            if (!isFragmentLimitReached(i)) {

                String aFragment = valueFragments[i];

                // fragmentedField creates from _1
                int fragmentedFieldSuffix = i + 1;

                addField(MapperUtils.generateIndexFragmentName(parentFieldName,
                        String.valueOf(fragmentedFieldSuffix)),
                        aFragment,
                        fields
                );

            } else {
                log.warn("Max fragment limit reached. Skipping fragment: {}", i + 1);
            }
        }

        log.debug("Completed generateFragmentFields");

    }

    public boolean isFragmentLimitReached(int currentFragmentIndex) {

        return currentFragmentIndex > maxIndexFragments;
    }

    public void addField(String fieldName, String fieldValue, List<IndexableField> fields) {

        IndexFragmentFieldMapper forwardFieldMapper = fragmentFields.get(fieldName);

        if(forwardFieldMapper==null) {
            throw new NullPointerException("Unexpected. fieldName: "+fieldName+" ["+fragmentFields.keySet()+"]");
        }

        log.debug("beg: addField - {} => {}", fieldName, forwardFieldMapper.fieldType().typeName());

        Field field = forwardFieldMapper.createField(fieldValue);
        fields.add(field);

        log.debug("end: addField - {}", field);
    }

    public String getIndexString(ParseContext context) throws IOException {

        final String value;
        if (context.externalValueSet()) {
            value = context.externalValue().toString();
        } else {
            value = context.parser().textOrNull();
        }
        return value;
    }

    /**
     * Used to recreate documents during indexing and merging.
     * @return
     */
    @Override
    protected String contentType() {
        return CryptoFieldType.TYPE_NAME;
    }

//    @Override
//    public void forEach(Consumer<? super Mapper> action) {
//        throw new UnsupportedOperationException("CryptoFieldMapper.forEach");
//    }
//
//    @Override
//    public Spliterator<Mapper> spliterator() {
//        throw new UnsupportedOperationException("CryptoFieldMapper.spliterator");
//    }

    @Override
    public boolean isFragment() {
        return false;
    }

    public boolean doValidateConfiguration(String value) throws IOException {

        if ((minPrefixChars % 6 != 0) && (maxPrefixChars % 6 != 0)) {
            throw new IOException("index prefixes minChars & maxChars value should be divided by 6");
        }

        if (value == null) {
            log.debug("Input value null");
            return false;
        }

        if (fieldType().indexOptions() == IndexOptions.NONE && !fieldType().stored()) {
            log.debug("Doing nothing. fieldType().indexOptions() == IndexOptions.NONE && !fieldType().stored()");
            return false;
        }

        return true;
    }

    @Override
    protected void doMerge(Mapper mergeWith, boolean updateAllTypes) {
        super.doMerge(mergeWith, updateAllTypes);
    }

    public int getPositionIncrementGap() {
        return positionIncrementGap;
    }

    public int getMinPrefixChars() {
        return minPrefixChars;
    }

    public int getMaxPrefixChars() {
        return maxPrefixChars;
    }

    public int getMaxIndexFragments() {
        return maxIndexFragments;
    }

    public Map<String, IndexFragmentFieldMapper> getFragmentFields() {
        return fragmentFields;
    }

    @Inject
    public void setCryptoEngine(CryptoEngine cryptoEngine) {
        this.cryptoEngine = cryptoEngine;
    }

    /**
     * This is the critical piece that ties the fragments to the main index.
     * Without it, there will be silent failures with message in the logs as below
     * <tt>[2020-04-30T13:38:37,534][WARN ][o.e.i.s.IndexShard       ] [mTRZJIj] [users][2] no index mapper found for field: [social_0] returning default postings format</tt>
     * @return
     */
    @Override
    public Iterator<Mapper> iterator() {

        List<Mapper> subIterators = new ArrayList<>(this.fragmentFields.values());

        if (subIterators.size() == 0) {
            return super.iterator();
        }

        return Iterators.concat(super.iterator(), subIterators.iterator());
    }


}
