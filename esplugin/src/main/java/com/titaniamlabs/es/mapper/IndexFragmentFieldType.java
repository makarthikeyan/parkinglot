package com.titaniamlabs.es.mapper;

import com.titaniamlabs.log.Log;
import com.titaniamlabs.util.MapperUtils;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.AutomatonQuery;
import org.apache.lucene.search.MultiTermQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.automaton.Automata;
import org.apache.lucene.util.automaton.Automaton;
import org.apache.lucene.util.automaton.Operations;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.analysis.AnalyzerScope;
import org.elasticsearch.index.analysis.NamedAnalyzer;
import org.elasticsearch.index.mapper.StringFieldType;
import org.elasticsearch.index.query.QueryShardContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class IndexFragmentFieldType extends StringFieldType {

    private static final Log log = Log.getLogger(IndexFragmentFieldType.class);

    private int minChars;
    private int maxChars;
    private String parentField;

    public static final String TYPE_NAME = "fragmented_tangle_text";

    public IndexFragmentFieldType(String parentField, String name, int minChars, int maxChars) {
        //TODO: cleanup. shouldn't call public super class methods in constructor.

        setOmitNorms(true);
        setIndexOptions(IndexOptions.DOCS);
        setName(name);
        this.minChars = minChars;
        this.maxChars = maxChars;
        this.parentField = parentField;
    }

    @Override
    public String typeName() {
        return TYPE_NAME;
    }

    @Override
    public Query existsQuery(QueryShardContext context) {
        throw new UnsupportedOperationException("not yet :(");
    }

    public IndexFragmentFieldType setAnalyzer(NamedAnalyzer delegate) {

        setIndexAnalyzer(new NamedAnalyzer(delegate.name(), AnalyzerScope.INDEX,
                new PrefixWrappedAnalyzer(delegate.analyzer(), minChars, maxChars)));

        return this;
    }

    @Override
    public Query termQuery(Object value, QueryShardContext context) {
        return super.termQuery(value, context);
    }

    void doXContent(XContentBuilder builder) throws IOException {
        builder.startObject("index_prefixes");
        builder.field("min_chars", minChars);
        builder.field("max_chars", maxChars);
        builder.endObject();
    }

    @Override
    public IndexFragmentFieldType clone() {

        IndexFragmentFieldType fragmentFieldType = new IndexFragmentFieldType(parentField, name(), minChars, maxChars);
        fragmentFieldType.setTokenized(this.tokenized());
        fragmentFieldType.setIndexAnalyzer(this.indexAnalyzer());

        return fragmentFieldType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        IndexFragmentFieldType that = (IndexFragmentFieldType) o;
        return minChars == that.minChars &&
                maxChars == that.maxChars &&
                Objects.equals(parentField, that.parentField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), minChars, maxChars, parentField);
    }

    //TODO: Clean this up and move it to <code>TitaniamQueryBuilder</code>
    public Query fragmentedPrefixQuery(String value, MultiTermQuery.RewriteMethod method, QueryShardContext context) {

        if (MapperUtils.isForwardOrReverseName(name())) {

            log.debug("fragmentedPrefixQuery - value: {}", value);

            return super.prefixQuery(value, method, context);
        }

        List<Automaton> automata = new ArrayList<>();
        automata.add(Automata.makeString(value));

        for (int i = value.length(); i < minChars; i++) {
            automata.add(Automata.makeAnyChar());
        }
        Automaton automaton = Operations.concatenate(automata);
        return new AutomatonQuery(new Term(name(), value + "*"), automaton);
    }


    @Override
    public String toString() {

        return "IndexFragmentFieldType{" +
                "type=" + typeName() +
                ", name=" + name() +
                ", parentField='" + parentField + '\'' +
                '}' + super.toString();
    }
}
