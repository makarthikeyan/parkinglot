package com.titaniamlabs.es.mapper;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.AnalyzerWrapper;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.ngram.EdgeNGramTokenFilter;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.search.Query;
import org.elasticsearch.index.analysis.AnalyzerScope;
import org.elasticsearch.index.analysis.NamedAnalyzer;
import org.elasticsearch.index.mapper.MappedFieldType;
import org.elasticsearch.index.mapper.StringFieldType;
import org.elasticsearch.index.mapper.TextFieldMapper;
import org.elasticsearch.index.query.QueryShardContext;

public class PrefixWrappedAnalyzer extends AnalyzerWrapper {

    private final int minChars;
    private final int maxChars;
    private final Analyzer delegate;

    PrefixWrappedAnalyzer(Analyzer delegate, int minChars, int maxChars) {
        super(delegate.getReuseStrategy());
        this.delegate = delegate;
        this.minChars = minChars;
        this.maxChars = maxChars;
    }

    @Override
    protected Analyzer getWrappedAnalyzer(String fieldName) {
        return delegate;
    }

    @Override
    protected TokenStreamComponents wrapComponents(String fieldName, TokenStreamComponents components) {

        //EdgeNGramTokenFilter constructor will always fail, since it accesses a method
        //of super class to initialize a private variable.
        // java.lang.NullPointerException
        //	at org.apache.lucene.util.AttributeSource.addAttribute(AttributeSource.java:205)
        //	at org.apache.lucene.analysis.ngram.EdgeNGramTokenFilter.<init>(EdgeNGramTokenFilter.java:47)
        TokenFilter filter = new EdgeNGramTokenFilter(components.getTokenStream(), minChars, maxChars);
        return new TokenStreamComponents(components.getTokenizer(), filter);
    }
}


