package com.titaniamlabs.es.mapper;

import com.google.common.base.CharMatcher;
import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.crypto.CryptoResult;
import com.titaniamlabs.crypto.WildCardSearchType;
import com.titaniamlabs.log.Log;
import com.titaniamlabs.util.MapperUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.util.BytesRef;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.lucene.BytesRefs;
import org.elasticsearch.index.query.QueryShardContext;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Map;

import static com.titaniamlabs.es.mapper.Defaults.CRYPTO_FACTOR;

/**
 * Helper that abstracts the logic of building the various search queries.
 */
public class TitaniamQueryBuilder {

    private static final Log log = Log.getLogger(TitaniamQueryBuilder.class);
    public static final char ASTERISK = '*';

    private Map<String, IndexFragmentFieldMapper> fragmentedFieldMapperMap;
    private CryptoEngine cryptoEngine;
    private int maxChars;
    private int maxIndexFragments;

    public TitaniamQueryBuilder(Map<String, IndexFragmentFieldMapper> fragmentedFieldMapperMap,
                                CryptoEngine cryptoEngine,
                                int maxChars,
                                int maxIndexFragments) {
        this.fragmentedFieldMapperMap = fragmentedFieldMapperMap;
        this.cryptoEngine = cryptoEngine;
        this.maxChars = maxChars;
        this.maxIndexFragments = maxIndexFragments;
    }

    @Inject
    public void setCryptoEngine(CryptoEngine cryptoEngine) {
        this.cryptoEngine = cryptoEngine;
    }

    /**
     * Generates a prefix query.
     *
     * @param prefixField - the fragment field with prefix (_0, _rev, _1, _2 etc.,)
     * @param clearText
     * @param method
     * @param context
     * @return
     */
    public Query prefixQuery(String prefixField,
                             String clearText,
                             MultiTermQuery.RewriteMethod method,
                             QueryShardContext context) {

        log.debug("prefixQuery: {}:{} ", prefixField, clearText);

        CryptoResult tangledContent = cryptoEngine.encrypt(clearText);
        log.debug(tangledContent);

        String tangledValue = MapperUtils.isReverseFieldName(prefixField) ?
                tangledContent.getReverse() : tangledContent.getForward();

        //?? why would encrypted value's cases be altered??
        tangledValue = tangledValue.toLowerCase();

        if (fragmentedFieldMapperMap != null && fragmentedFieldMapperMap.containsKey(prefixField)) {

            IndexFragmentFieldMapper fragmentedFieldMapper = fragmentedFieldMapperMap.get(prefixField);

            IndexFragmentFieldType fragmentFieldType = fragmentedFieldMapper.fieldType();

            return fragmentFieldType.fragmentedPrefixQuery(tangledValue, method, context);

        } else {
            // TODO: Need to handle if original field value is null or empty
            return null;
        }
    }

    public Query termQuery(String fieldName, Object value, QueryShardContext context) {

        log.debug("termQuery: {}", fieldName);

        if(fieldName==null || value==null || context==null) {
            return null;
        }

        if (!(value instanceof BytesRef) || Strings.isNullOrEmpty(fieldName)) {
            // TODO: what to do here? Is this possible??
            return null;
        }

        String clearText = ((BytesRef) value).utf8ToString();

        CryptoResult tangledValue = cryptoEngine.encrypt(clearText);

        if(tangledValue==null) {
            return null;
        }

        String tangledLowercase = tangledValue.getForward().toLowerCase();

        String generateForwardFieldName = MapperUtils.generateForwardFieldName(fieldName);

        log.debug("[termQuery] {}: {} -> {}", context.getShardId(), generateForwardFieldName, tangledLowercase);

        return new TermQuery(new Term(generateForwardFieldName, indexedValueForSearch(tangledLowercase)));
    }

    /**
     *
     * @param fieldName
     * @param value
     * @param method
     * @param context
     * @return
     */
    public Query wildCardQuery(String fieldName, String value, MultiTermQuery.RewriteMethod method, QueryShardContext context) {

        //Impl Note:
        log.debug("wildCardQuery: {} ", value);

        //TODO: What happens for ? or other wildcard chars?
        WildCardSearchType searchType = parseSearchType(value);
        String clearText = CharMatcher.is('*').removeFrom(value);

        log.debug("type:{}, value:{}", searchType, value);

        if (WildCardSearchType.STARTS_WITH == searchType) {

            String prefixField = MapperUtils.generateForwardFieldName(fieldName);
            return prefixQuery(prefixField, clearText, method, context);

        } else if (WildCardSearchType.ENDS_WITH == searchType) {

            String prefixField = MapperUtils.generateReverseFieldName(fieldName);
            return prefixQuery(prefixField, clearText, method, context);

        } else if (WildCardSearchType.TERM == searchType) {

            return termQuery(fieldName, clearText, context);

        } else {

            //TODO: Why do you need the searchType here? It is always going to be
            //TODO: CONTAINS
            //TODO: This does not take into account when the max string length can be
            // increased from current hardcoded 32 to any arbitrary length.
            String[] wildCardSearchTerms = cryptoEngine.encryptFor1toNPositions(clearText, searchType, maxIndexFragments);

            if(log.isDebugEnabled()) {
                log.debug("wildcard terms by position: {}", Arrays.asList(wildCardSearchTerms));
            }

            return generateWildCardQuery(fieldName, wildCardSearchTerms);
        }
    }

    public BooleanQuery generateWildCardQuery(String fieldName, String[] wildCardSearchTerms) {

        log.debug("generateWildCardQuery - parentField: {}", fieldName);

        BooleanQuery.Builder builder = new BooleanQuery.Builder();

        for (int i = 0; i < wildCardSearchTerms.length; i++) {

            Query fragmentQuery = generateSplitQueryOnFragmentIndex(fieldName,
                    wildCardSearchTerms[i],
                    i + 1);

            //TODO: This will leave a hanging SHOULD in the end. Is that okay??
            if(fragmentQuery!=null) {
                builder.add(fragmentQuery, BooleanClause.Occur.SHOULD);
            }
        }

        // any one of the "SHOULD" match will result in the document
        // getting selected.
        builder.setMinimumNumberShouldMatch(1);

        BooleanQuery wildcardQuery = builder.build();

        log.debug("final WildcardQuery: {}", wildcardQuery);

        return wildcardQuery;
    }

    /**
     * Given a user input query fragment, generates the logical
     * query parts that can be used to query against the index.
     *
     * @param parentFieldName
     * @param encryptedValue
     * @param fieldPosition
     * @return
     */
    public Query generateSplitQueryOnFragmentIndex(String parentFieldName, String encryptedValue, int fieldPosition) {

        log.debug("generateSplitQueryOnFragmentIndex: {}, {}", parentFieldName, fieldPosition);

        // validations
        if(Strings.isNullOrEmpty(parentFieldName) || Strings.isNullOrEmpty(encryptedValue)) {
            return null;
        }

        String[] splitValues = MapperUtils.splitToNChar(encryptedValue, maxChars, maxChars);

        if(splitValues==null || splitValues.length==0) {
            return null;
        }

        if(splitValues.length==1) {

            log.debug("generateSplitQueryOnFragmentIndex - only one fragment. creating TermQuery instead of Boolean-MUST");

            return new TermQuery(new Term(
                    MapperUtils.generateIndexFragmentName(
                            parentFieldName,
                            String.valueOf(fieldPosition)),
                    indexedValueForSearch(splitValues[0])));
        }

        // check if the boolean query formed with splitValues has
        // index available.
        if(fieldPosition + splitValues.length > maxIndexFragments) {
            //this will result in a fragment going out of bounds.
            // those types of queries do not make any sense.
            // ex: (+social_32:666666666666666666 +social_33:666666)
            log.debug("fieldPosition + splitValues.length > maxIndexFragments: {}, {}, {}", fieldPosition, splitValues.length, maxIndexFragments);
            return null;
        }

        // Build the expression.
        BooleanQuery.Builder andQueryBuilder = new BooleanQuery.Builder();

        int fieldSuffix = fieldPosition;

        for (String splitValue : splitValues) {

            String fragmentedFieldName = MapperUtils.generateIndexFragmentName(
                    parentFieldName,
                    String.valueOf(fieldSuffix));

            Query query = new TermQuery(new Term(fragmentedFieldName, indexedValueForSearch(splitValue)));

            log.debug("MUST: {}", query);

            andQueryBuilder.add(query, BooleanClause.Occur.MUST);

            // the field to bool with will have the string length
            // apart
            fieldSuffix = fieldSuffix + findDistanceToNextFragment(splitValue.length());

        }

        return andQueryBuilder.build();

    }

    public int findDistanceToNextFragment(int currentFragmentLength) {

        if(currentFragmentLength % CRYPTO_FACTOR!=0) {
            // This will break only when the crypto algo has changed.
            // This is in place to prevent bugs lying around when that happens.
            // tests should capture it.
            throw new IllegalStateException("Fragment length, not a multiple of: "+CRYPTO_FACTOR);
        }

        return currentFragmentLength / CRYPTO_FACTOR;
    }

    /**
     * Given a string, it will return the search type based on
     * placement of '*'.
     *
     * Common types are taken into account. Not all corner cases
     * work.
     *
     * @param value
     * @return
     */
    public WildCardSearchType parseSearchType(String value) {

        if(Strings.isNullOrEmpty(value)) {
            return WildCardSearchType.TERM;
        }

        //TODO: Is there an option of escaping the wildcard? Ex:"rain\*"
        //TODO: Also, what happens if there is irregular *s? Ex:"*rain*s"

        int asteriskCnt = CharMatcher.is(ASTERISK).countIn(value);

        if (asteriskCnt == 1) {

            if (value.charAt(0) == ASTERISK) {
                return WildCardSearchType.ENDS_WITH;
            } else if (value.charAt(value.length() - 1) == ASTERISK) {
                return WildCardSearchType.STARTS_WITH;
            }

        } else if (asteriskCnt==0) {

            return WildCardSearchType.TERM;
        }

        return WildCardSearchType.CONTAINS;

    }

    /**
     * This is copy/pasted from <code>TermBasedFieldType</code>
     *
     * @param value
     * @return
     */
    protected BytesRef indexedValueForSearch(Object value) {
        return BytesRefs.toBytesRef(value);
    }
}
