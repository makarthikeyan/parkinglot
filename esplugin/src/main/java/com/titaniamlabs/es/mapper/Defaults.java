package com.titaniamlabs.es.mapper;

import org.elasticsearch.index.mapper.MappedFieldType;
import org.elasticsearch.index.mapper.TextFieldMapper;

/**
 * Copy/pasted from TextFieldMapper.Defaults
 */
public class Defaults {

    public static final double FIELDDATA_MIN_FREQUENCY = 0;
    public static final double FIELDDATA_MAX_FREQUENCY = Integer.MAX_VALUE;
    public static final int FIELDDATA_MIN_SEGMENT_SIZE = 0;
    public static final int INDEX_PREFIX_MIN_CHARS = 6;
    public static final int CRYPTO_FACTOR = 6;
    public static final int INDEX_PREFIX_MAX_CHARS = 18;
    public static final int INDEX_PREFIX_MAX_FRAGMENTS = 32;

    public static final CryptoFieldType FIELD_TYPE = new CryptoFieldType();

    static {
        FIELD_TYPE.freeze();
    }

    /**
     * The default position_increment_gap is set to 100 so that phrase
     * queries of reasonably high slop will not match across field values.
     */
    public static final int POSITION_INCREMENT_GAP = 100;

}
