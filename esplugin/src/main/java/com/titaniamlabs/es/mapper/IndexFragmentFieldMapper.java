package com.titaniamlabs.es.mapper;

import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexableField;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.mapper.FieldMapper;
import org.elasticsearch.index.mapper.MappedFieldType;
import org.elasticsearch.index.mapper.Mapper;
import org.elasticsearch.index.mapper.ParseContext;

import java.io.IOException;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Represents the index fragments that get created during indexing of
 * a potential field.
 */
public class IndexFragmentFieldMapper extends FieldMapper {

    public IndexFragmentFieldMapper(IndexFragmentFieldType fieldType, Settings indexSettings) {
        super(fieldType.name(), fieldType, fieldType, indexSettings, MultiFields.empty(), CopyTo.empty());
    }

    /**
     * This method is a no-op. All the parsing of the index values are performed earlier in the
     * {@link CryptoFieldMapper}.
     *
     * @param context
     * @param fields
     * @throws IOException
     */
    @Override
    protected void parseCreateField(ParseContext context, List<IndexableField> fields) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected String contentType() {
        return IndexFragmentFieldType.TYPE_NAME;
    }

//    @Override
//    public void forEach(Consumer<? super Mapper> action) {
//
//    }

    @Override
    public IndexFragmentFieldType fieldType() {
        return (IndexFragmentFieldType) super.fieldType();
    }

    @Override
    public Spliterator<Mapper> spliterator() {
        return null;
    }

    public Field createField(String value) {

        return new Field(name(), value, fieldType());

    }

    @Override
    public String toString() {
        return fieldType().toString();
    }
}
