package com.titaniamlabs.es.mapper;

import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.search.Query;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.analysis.AnalyzerScope;
import org.elasticsearch.index.analysis.NamedAnalyzer;
import org.elasticsearch.index.mapper.StringFieldType;
import org.elasticsearch.index.mapper.TextFieldMapper;
import org.elasticsearch.index.query.QueryShardContext;

import java.io.IOException;
import java.util.Objects;

/**
 * Copy/pasted from TextFieldMapper.PrefixFieldType
 */
public class PrefixFieldType extends StringFieldType {

    //Impl Note: Where is this class used??

    final int minChars;
    final int maxChars;

    PrefixFieldType(String name, int minChars, int maxChars) {
        setTokenized(true);
        setOmitNorms(true);
        setIndexOptions(IndexOptions.DOCS);
        setName(name);
        this.minChars = minChars;
        this.maxChars = maxChars;
    }

    PrefixFieldType(PrefixFieldType clone) {
        super(clone);
        this.minChars = clone.minChars;
        this.maxChars = clone.maxChars;
    }

    public PrefixFieldType setAnalyzer(NamedAnalyzer delegate) {

        setIndexAnalyzer(new NamedAnalyzer(delegate.name(), AnalyzerScope.INDEX,
                new PrefixWrappedAnalyzer(delegate.analyzer(), minChars, maxChars)));
        return this;
    }

    boolean accept(int length) {
        return length >= minChars && length <= maxChars;
    }

    void doXContent(XContentBuilder builder) throws IOException {
        builder.startObject("index_prefixes");
        builder.field("min_chars", minChars);
        builder.field("max_chars", maxChars);
        builder.endObject();
    }

    @Override
    public PrefixFieldType clone() {

        return new PrefixFieldType(this);
    }

    @Override
    public String typeName() {
        return "prefix";
    }

    @Override
    public String toString() {
        return super.toString() + ",prefixChars=" + minChars + ":" + maxChars;
    }

    @Override
    public Query existsQuery(QueryShardContext context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PrefixFieldType that = (PrefixFieldType) o;
        return minChars == that.minChars &&
                maxChars == that.maxChars;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), minChars, maxChars);
    }
}
