package com.titaniamlabs.es.mapper;

import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.log.Log;
import com.titaniamlabs.util.MapperUtils;
import org.apache.lucene.index.IndexOptions;
import org.elasticsearch.Version;
import org.elasticsearch.index.mapper.*;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

import static com.titaniamlabs.util.MapperUtils.generateIndexFragmentName;
import static com.titaniamlabs.util.MapperUtils.*;

public class CryptoFieldMapperBuilder extends FieldMapper.Builder<CryptoFieldMapperBuilder,CryptoFieldMapper> {

    private static final Log log = Log.getLogger(CryptoFieldMapperBuilder.class);

    private int minPrefixChars;
    private int maxPrefixChars;
    private int maxIndexFragments;
    private int positionIncrementGap;
    private PrefixFieldType prefixFieldType;
    private CryptoEngine cryptoEngine;

    public CryptoFieldMapperBuilder(String name, MappedFieldType fieldType, MappedFieldType defaultFieldType) {

        super(name, fieldType, defaultFieldType);
        this.maxIndexFragments = Defaults.INDEX_PREFIX_MAX_FRAGMENTS;
        builder = this;

        log.debug("new CryptoFieldMapperBuilder()");
    }

    @Inject
    public void setCryptoEngine(CryptoEngine cryptoEngine) {
        this.cryptoEngine = cryptoEngine;
    }

    /**
     * Factory method used to create the actual field mapper instance, with all user
     * input values initialized correctly.
     *
     * @param context
     * @return
     */
    @Override
    public CryptoFieldMapper build(Mapper.BuilderContext context) {

        log.debug("Begin build: {}", name());

        doValidateMapping();

        setupFieldType(context);

        Map<String, IndexFragmentFieldMapper> fragmentFields = null;

        if(isInitializeFragments()) {
            fragmentFields = buildFragmentMappers(context);
        }

        fieldType().setFragmentedFieldMapperMap(fragmentFields);

        CryptoFieldMapper cryptoFieldMapper = new CryptoFieldMapper(name,
                fieldType(),
                defaultFieldType,
                positionIncrementGap,
                minPrefixChars,
                maxPrefixChars,
                fragmentFields,
                context.indexSettings(),
                multiFieldsBuilder.build(this, context),
                maxIndexFragments,
                copyTo
                );

        cryptoFieldMapper.setCryptoEngine(cryptoEngine);
        initializeQueryBuilder(fragmentFields);

        return cryptoFieldMapper;
    }

    /**
     * This initializes the query builder that has dependency on a bunch of
     * parameters that are initialized here in the builder.
     *
     * @param fragmentFields
     */
    public void initializeQueryBuilder(Map<String, IndexFragmentFieldMapper> fragmentFields) {

        TitaniamQueryBuilder queryBuilder = new TitaniamQueryBuilder(
                fragmentFields,
                this.cryptoEngine,
                maxPrefixChars,
                maxIndexFragments);

        fieldType().setQueryBuilder(queryBuilder);
    }

    /**
     * Builds all the mappers for the each index fragment that is created.
     * @param context
     */
    public Map<String, IndexFragmentFieldMapper> buildFragmentMappers(Mapper.BuilderContext context) {

        Map<String, IndexFragmentFieldMapper> fragmentFields = new HashMap<>();
        String parentFieldName = name() ;

        for(int i=0;i<maxIndexFragments;i++) {
            //index fragment name
            String fragmentName = generateIndexFragmentName(parentFieldName, String.valueOf(i));

            IndexFragmentFieldMapper fragmentedFieldMapper = buildFragmentMapper(
                    i,
                    parentFieldName,
                    fragmentName,
                    context);

            fragmentFields.put(fragmentName, fragmentedFieldMapper);

            log.debug("added fragmentName: {}", fragmentName);
        }

        // reverse fields
        String fragmentName = generateIndexFragmentName(parentFieldName,
                MapperUtils.REV_SUFFIX);

        fragmentFields.put(fragmentName,
                buildFragmentMapper(
                        -1,
                        parentFieldName,
                        fragmentName,
                        context)
        );

        log.debug("added fragmentName: {}", fragmentName);

        return fragmentFields;
    }

    public void initAnalyzer(IndexFragmentFieldType fragmentFieldType) {

        //Impl Note: Forward field values are neither tokenized during document save,
        // nor should be tokenized during search/retrieval.
        if(isForwardOrReverseName(fragmentFieldType.name())) {
            fragmentFieldType.setTokenized(false);
        } else {
            fragmentFieldType.setAnalyzer(fieldType.indexAnalyzer());
        }
    }


    /**
     * Builds the fragment mapper for each subfield.
     *
     *
     * @param fieldPosition
     * @param parentFieldName
     * @param fragmentName
     * @param context
     * @return
     */
    public IndexFragmentFieldMapper buildFragmentMapper(int fieldPosition, String parentFieldName, String fragmentName, Mapper.BuilderContext context) {

        IndexFragmentFieldType fragmentedFieldType =
                new IndexFragmentFieldType(parentFieldName,
                        fragmentName,
                        minPrefixChars,
                        maxPrefixChars);

        initAnalyzer(fragmentedFieldType);

        if (context.indexCreatedVersion().onOrAfter(Version.CURRENT)) {
            if (fieldType.indexOptions() == IndexOptions.DOCS_AND_FREQS) {
                fragmentedFieldType.setIndexOptions(IndexOptions.DOCS);
            } else {
                fragmentedFieldType.setIndexOptions(fieldType.indexOptions());
            }
        } else if (fieldType.indexOptions() == IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS) {
            fragmentedFieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
        }

        if (fieldType.storeTermVectorOffsets()) {
            fragmentedFieldType.setStoreTermVectorOffsets(true);
        }

        return new IndexFragmentFieldMapper(
                fragmentedFieldType,
                context.indexSettings()
        );

    }

    /**
     * Returns true if user specified values are there in the mapping configuration
     * @return
     */
    public boolean isInitializeFragments() {
        return this.minPrefixChars>-1;
    }

    /**
     * Ensures the configuration is consistent
     */
    public void doValidateMapping() {

        if(!fieldType().isSearchable()) {
            throw new IllegalArgumentException("Cannot set index_prefixes on unindexed field [" + name() + "]");
        }

        if (fieldType().isIndexPhrases()) {
//            if (!fieldType().isSearchable()) {
//                throw new IllegalArgumentException("Cannot set index_phrases on unindexed field [" + name() + "]");
//            }
            if (fieldType().indexOptions().compareTo(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS) < 0) {
                throw new IllegalArgumentException("Cannot set index_phrases on field [" + name() + "] if positions are not enabled");
            }
        }

    }

    public void doValidateIndexPrefix() {

        if (minPrefixChars > maxPrefixChars) {
            throw new IllegalArgumentException("min_chars [" + minPrefixChars + "] must be less than max_chars [" + maxPrefixChars + "]");
        }
        if (minPrefixChars < 1) {
            throw new IllegalArgumentException("min_chars [" + minPrefixChars + "] must be greater than zero");
        }
        if (maxPrefixChars >= maxIndexFragments) {
            throw new IllegalArgumentException("max_chars [" + maxPrefixChars + "] must be less than "+maxIndexFragments);
        }
    }

    /**
     * Ensures all the values set through the configuration is valid.
     */
    public void doValidate() {

        doValidateIndexPrefix();
        doValidateMapping();
    }

    @Override
    public CryptoFieldType fieldType() {
        return (CryptoFieldType) super.fieldType();
    }

    //==================== configuration specified in mapping ======================//
    /**
     * This can be specified in the type configuration.
     * @param maxIndexFragments
     */
    public void setMaxIndexFragments(int maxIndexFragments) {
        this.maxIndexFragments = maxIndexFragments;
    }

    public CryptoFieldMapperBuilder positionIncrementGap(int positionIncrementGap) {

        if (positionIncrementGap < 0) {
            throw new MapperParsingException("[positions_increment_gap] must be positive, got " + positionIncrementGap);
        }

        this.positionIncrementGap = positionIncrementGap;

        return this;
    }

    /**
     * Copy/pated from TextFieldMapper.Builder
     * @param fielddata
     * @return
     */
    public CryptoFieldMapperBuilder fielddata(boolean fielddata) {
        fieldType().setFielddata(fielddata);
        return this;
    }
    public CryptoFieldMapperBuilder indexPhrases(boolean indexPhrases) {
        fieldType().setIndexPhrases(indexPhrases);
        return this;
    }

    @Override
    public CryptoFieldMapperBuilder docValues(boolean docValues) {
        if (docValues) {
            throw new IllegalArgumentException("[text] fields do not support doc values");
        }
        return super.docValues(docValues);
    }

    public CryptoFieldMapperBuilder eagerGlobalOrdinals(boolean eagerGlobalOrdinals) {
        fieldType().setEagerGlobalOrdinals(eagerGlobalOrdinals);
        return this;
    }

    public CryptoFieldMapperBuilder fielddataFrequencyFilter(double minFreq, double maxFreq, int minSegmentSize) {
        fieldType().setFielddataMinFrequency(minFreq);
        fieldType().setFielddataMaxFrequency(maxFreq);
        fieldType().setFielddataMinSegmentSize(minSegmentSize);
        return this;
    }

    public CryptoFieldMapperBuilder indexPrefixes(int minChars, int maxChars) {

        this.minPrefixChars = minChars;
        this.maxPrefixChars = maxChars;

        doValidateIndexPrefix();

        return this;
    }

    public int getMinPrefixChars() {
        return minPrefixChars;
    }

    public int getMaxPrefixChars() {
        return maxPrefixChars;
    }

    public int getMaxIndexFragments() {
        return maxIndexFragments;
    }

    public void setMinPrefixChars(int minPrefixChars) {
        this.minPrefixChars = minPrefixChars;
    }

    public void setMaxPrefixChars(int maxPrefixChars) {
        this.maxPrefixChars = maxPrefixChars;
    }
}
