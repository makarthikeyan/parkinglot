package com.titaniamlabs.es.search;

import com.titaniamlabs.log.Log;
import org.elasticsearch.common.io.stream.NamedWriteableRegistry;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.util.concurrent.ThreadContext;
import org.elasticsearch.transport.*;

import java.io.IOException;

public class TitaniamTransportInterceptor implements TransportInterceptor {

    private static final Log log = Log.getLogger(TitaniamTransportInterceptor.class);

    public TitaniamTransportInterceptor(NamedWriteableRegistry namedWriteableRegistry, ThreadContext threadContext) {
    }

    @Override
    public <T extends TransportRequest> TransportRequestHandler<T> interceptHandler(String action, String executor,
                                                                                    boolean forceExecution,
                                                                                    TransportRequestHandler<T> actualHandler) {
        log.trace("interceptHandler");

        return new TitaniamTransportRequestHandler<>(actualHandler);
    }

    @Override
    public AsyncSender interceptSender(final AsyncSender sender) {
        return new AsyncSender() {
            @Override
            public <T extends TransportResponse> void sendRequest(Transport.Connection connection, String action, TransportRequest request,
                                                                  TransportRequestOptions options,
                                                                  final TransportResponseHandler<T> handler) {
                sender.sendRequest(connection, action, request, options, new TransportResponseHandler<T>() {

                    @Override
                    public T read(StreamInput in) throws IOException {
                        return handler.read(in);
                    }

                    @Override
                    public void handleResponse(T response) {
                        handler.handleResponse(response);
                    }

                    @Override
                    public void handleException(TransportException exp) {
                        handler.handleException(exp);
                    }

                    @Override
                    public String executor() {
                        return handler.executor();
                    }
                });
            }
        };
    }

}
