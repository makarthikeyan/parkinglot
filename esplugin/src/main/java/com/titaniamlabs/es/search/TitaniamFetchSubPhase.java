package com.titaniamlabs.es.search;

import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.es.mapper.CryptoFieldType;
import com.titaniamlabs.log.Log;
import org.elasticsearch.common.io.stream.BytesStreamOutput;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.mapper.MappedFieldType;
import org.elasticsearch.index.mapper.MapperService;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.FetchSubPhase;
import org.elasticsearch.search.internal.SearchContext;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Responsible for untangling search result on the way out.
 */
public class TitaniamFetchSubPhase implements FetchSubPhase {

    private static final Log log = Log.getLogger(TitaniamFetchSubPhase.class);

    private CryptoEngine cryptoEngine;

    @Inject
    public void setCryptoEngine(CryptoEngine cryptoEngine) {
        this.cryptoEngine = cryptoEngine;
    }

    @Override
    public void hitExecute(SearchContext context, HitContext hitContext) throws IOException {

        log.debug("hitExecute");

        SearchHit searchHit = hitContext.hit();

        Map<String, Object> sourceAsMap = searchHit.getSourceAsMap();

        XContentBuilder builder = untangleDocumentSource(sourceAsMap, context);

        searchHit.sourceRef(builder.bytes());
    }

    /**
     * Given the soure, decrypts the encrypted values.
     *
     * @param sourceAsMap - source values as map
     *
     * @return json representation of the new source
     */
    public XContentBuilder untangleDocumentSource(Map<String, Object> sourceAsMap, SearchContext context) throws IOException {

        //TODO: This does not take care of nested structure - i.e entry.value() is another map
        //TODO: Also, the name is independent of hierarchy - "ssn" at any level can be marked either as
        // tangled or not, as opposed to parent SSN tangled vs child SSN not-tangled.

        log.debug("untangleDocumentSource");

        XContentBuilder builder = XContentFactory.jsonBuilder(new BytesStreamOutput() );

        log.debug("");
        Map<String, Object> decryptedSourceMap = handleMap(sourceAsMap, context);

        builder.value(decryptedSourceMap);

        return builder;
    }

    /**
     * Decrypts the Map, accounting for any value for the entry, including another Map, List and String.
     *
     * @param sourceAsMap
     * @param context
     * @return
     */
    public Map<String, Object> handleMap(Map<String, Object> sourceAsMap, SearchContext context) {

        Map<String, Object> decryptedSourceMap = new HashMap<>();

        for (Map.Entry<String, Object> entry : sourceAsMap.entrySet()) {

            Object result = handleByValueType(entry.getKey(), entry.getValue(), context);

            decryptedSourceMap.put(entry.getKey(), result);

        }

        return decryptedSourceMap;
    }

    /**
     * Checks the type of value and calls the appropriate method for Map, List or SimpleValue
     *
     * @param fieldName
     * @param value
     * @param context
     * @return
     */
    public Object handleByValueType(String fieldName, Object value, SearchContext context) {

        Object returnValue;

        if(value instanceof Map) {

            log.debug("Map found for field: {}", fieldName);

            // recursion
            // if there is a map within the map, then, recursively untagle it.
            returnValue = handleMap((Map<String, Object>) value,
                    context);

        } else if(value instanceof List) {

            log.debug("List found for field: {}", fieldName);

            // recursion
            List<Object> values = (List<Object>) value;

            returnValue = handleList(fieldName, values, context);

        } else {

            returnValue = handleSimpleValue(fieldName, value, context);
        }

        return returnValue;
    }

    /**
     * Recursive routine that handles Lists. List's can be of any type, including
     * another List, Map or a simple values.
     *
     * @param fieldName
     * @param values
     * @param context
     * @return
     */
    public List<Object> handleList(String fieldName, List<Object> values, SearchContext context) {

        List<Object> decryptedList = new ArrayList<>();

        for(Object value : values) {

            Object result = handleByValueType(fieldName, value, context);

            decryptedList.add(result);
        }

        return decryptedList;
    }

    public Object handleSimpleValue(String fieldName, Object value, SearchContext context) {

        MapperService mapperService = context.mapperService();
        MappedFieldType mappedFieldType = mapperService.fullName(fieldName);

        if (mappedFieldType instanceof CryptoFieldType) {

            log.debug("tangled_text for field: {} ", fieldName);

            // TODO:
            //  This always assumes that crypto fields are simple Strings.
            //  Reverse this to check for field type first to be crypto.
            //  If crypto, then, we need to delegate decryptig to the CryptoFieldType
            //  OR the CryptoFieldMapper itself, since it is no longer straightforward.
            // TODO:
            //  This should be done even for Simple Strings, that way CryptoField(Type|Mapper)
            //  would be the only place that has knowledge on decrypting the value, instead
            //  of it spread all over the place.
            String decryptedValue = cryptoEngine.decrypt(String.valueOf(value), true);

            //writeLogs(context, hitContext, fieldName, value);

            return decryptedValue;

        } else {

            log.debug("simple non-tangled value for field: {} ", fieldName);

            return value;
        }
    }
}
