package com.titaniamlabs.es.search;

import com.titaniamlabs.log.Log;
import org.apache.lucene.search.Query;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.index.query.QueryShardContext;
import org.elasticsearch.index.query.WildcardQueryBuilder;

import java.io.IOException;

/**
 * Primary aim of this wrapper is to expose protected methods in the
 * underlying <tt>WildcardQueryBuilder</tt>, so that we can
 * avoid copy/pasting of logic.
 *
 * Any real logic should be in parent class or the caller. This class
 * should be just a proxy.
 *
 */
public class WildcardWrapper extends WildcardQueryBuilder {

    private static final Log log = Log.getLogger(WildcardWrapper.class);

    public WildcardWrapper(String fieldName, String value) {
        super(fieldName, value);
    }

    public WildcardWrapper(StreamInput in) throws IOException {
        super(in);
    }

    public WildcardWrapper(WildcardQueryBuilder delegate) {
        super(delegate.fieldName(), delegate.value());
        super.rewrite(delegate.rewrite());
    }

    @Override
    public Query doToQuery(QueryShardContext context) throws IOException {

        log.debug("doToQuery");

        return super.doToQuery(context);
    }
}
