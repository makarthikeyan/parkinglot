package com.titaniamlabs.es.search;

import com.titaniamlabs.log.Log;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.internal.ShardSearchTransportRequest;
import org.elasticsearch.tasks.Task;
import org.elasticsearch.transport.TransportChannel;
import org.elasticsearch.transport.TransportRequest;
import org.elasticsearch.transport.TransportRequestHandler;

import java.util.ArrayList;
import java.util.List;

public class TitaniamTransportRequestHandler<T extends TransportRequest> implements TransportRequestHandler<T> {

    private static final Log log = Log.getLogger(TitaniamTransportRequestHandler.class);

    private TransportRequestHandler<T> actualHandler;
    private List<Long> processedReqs;

    public TitaniamTransportRequestHandler(TransportRequestHandler<T> actualHandler) {

        log.trace("TitaniamTransportRequestHandler(TransportRequestHandler<T> actualHandler)");

        this.actualHandler = actualHandler;
        this.processedReqs = new ArrayList<>();
    }

    @Override
    public void messageReceived(T request, TransportChannel channel, Task task) throws Exception {

        log.trace("messageReceived(T request, TransportChannel channel, Task task): {}", processedReqs.size());

        // TODO: Also, why is ArrayList used for lookups??
        if (request instanceof ShardSearchTransportRequest && !processedReqs.contains(task.getParentTaskId().getId())) {
            //TODO: Is this a memory leak??
            processedReqs.add(task.getParentTaskId().getId());
            SearchSourceBuilder source = ((ShardSearchTransportRequest) request).source();
            QueryBuilder query = ((ShardSearchTransportRequest) request).source().query();
            QueryBuilder convertedQuery = convertQueryBuilder(query);
            source.query(convertedQuery);
        }

        actualHandler.messageReceived(request, channel, task);
    }

    public QueryBuilder convertQueryBuilder(QueryBuilder query) {
        if (query instanceof BoolQueryBuilder) {
            return convertBoolQueryBuilder(query);
        } else if (query instanceof WildcardQueryBuilder) {
            return new TitaniamWildcardQueryBuilder((WildcardQueryBuilder) query);
        } else {
            return query;
        }
    }

    public QueryBuilder convertBoolQueryBuilder(QueryBuilder query) {
        BoolQueryBuilder boolQueryBuilder = (BoolQueryBuilder) query;
        List<QueryBuilder> filterQueries = convertQueryBuilders(boolQueryBuilder.filter());
        boolQueryBuilder.filter().clear();
        for (QueryBuilder filterQuery : filterQueries) {
            boolQueryBuilder.filter(filterQuery);
        }

        List<QueryBuilder> shouldQueries = convertQueryBuilders(boolQueryBuilder.should());
        boolQueryBuilder.should().clear();
        for (QueryBuilder shouldQuery : shouldQueries) {
            boolQueryBuilder.should(shouldQuery);
        }

        List<QueryBuilder> mustQueries = convertQueryBuilders(boolQueryBuilder.must());
        boolQueryBuilder.must().clear();
        for (QueryBuilder mustQuery : mustQueries) {
            boolQueryBuilder.must(mustQuery);
        }

        List<QueryBuilder> mustNotQueries = convertQueryBuilders(boolQueryBuilder.mustNot());
        boolQueryBuilder.mustNot().clear();
        for (QueryBuilder mustNotQuery : mustNotQueries) {
            boolQueryBuilder.mustNot(mustNotQuery);
        }

        return boolQueryBuilder;
    }

    public List<QueryBuilder> convertQueryBuilders(List<QueryBuilder> queryList) {
        List<QueryBuilder> newQueryList = new ArrayList<>();
        for (QueryBuilder query : queryList) {
            newQueryList.add(convertQueryBuilder(query));
        }

        return newQueryList;
    }

    @Override
    public void messageReceived(T request, TransportChannel channel) throws Exception {

        log.trace("messageReceived(T request, TransportChannel channel)");

        actualHandler.messageReceived(request, channel);
    }
}
