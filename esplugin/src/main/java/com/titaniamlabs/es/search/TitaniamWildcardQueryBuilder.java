package com.titaniamlabs.es.search;

import com.titaniamlabs.es.mapper.CryptoFieldType;
import com.titaniamlabs.log.Log;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.elasticsearch.common.ParseField;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.lucene.BytesRefs;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.index.mapper.MappedFieldType;
import org.elasticsearch.index.query.AbstractQueryBuilder;
import org.elasticsearch.index.query.MultiTermQueryBuilder;
import org.elasticsearch.index.query.QueryShardContext;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.elasticsearch.index.query.support.QueryParsers;

import java.io.IOException;
import java.util.Objects;

public class TitaniamWildcardQueryBuilder extends AbstractQueryBuilder<TitaniamWildcardQueryBuilder> implements MultiTermQueryBuilder {

    private static final Log log = Log.getLogger(TitaniamWildcardQueryBuilder.class);

    public static final String NAME = "titaniam_wildcard";

    private static final ParseField WILDCARD_FIELD = new ParseField("wildcard");
    private static final ParseField VALUE_FIELD = new ParseField("value");
    private static final ParseField REWRITE_FIELD = new ParseField("rewrite");

    private WildcardWrapper wildcardQueryBuilder;

    /**
     * Called by the ES system to instantiate.
     *
     * @param wildcardQueryBuilder
     */
    public TitaniamWildcardQueryBuilder(WildcardQueryBuilder wildcardQueryBuilder) {

        log.debug("TitaniamWildcardQueryBuilder(WildcardQueryBuilder wildcardQueryBuilder)");

        this.wildcardQueryBuilder = new WildcardWrapper(wildcardQueryBuilder);
    }

    public TitaniamWildcardQueryBuilder(WildcardWrapper wildcardQueryBuilder) {

        log.debug("TitaniamWildcardQueryBuilder(WildcardWrapper wildcardQueryBuilder)");

        this.wildcardQueryBuilder = wildcardQueryBuilder;
    }

    /**
     * Implements the wildcard search query. Supported wildcards are <tt>*</tt>, which
     * matches any character sequence (including the empty one), and <tt>?</tt>,
     * which matches any single character. Note this query can be slow, as it
     * needs to iterate over many terms. In order to prevent extremely slow WildcardQueries,
     * a Wildcard term should not start with one of the wildcards <tt>*</tt> or
     * <tt>?</tt>.
     *
     * @param fieldName The field name
     * @param value     The wildcard query string
     */
    public TitaniamWildcardQueryBuilder(String fieldName, String value) {

        log.debug("TitaniamWildcardQueryBuilder(String fieldName, String value)");

        this.wildcardQueryBuilder = new WildcardWrapper(fieldName, value);
    }

    /**
     * Read from a stream.
     */
    public TitaniamWildcardQueryBuilder(StreamInput in) throws IOException {

        log.debug("TitaniamWildcardQueryBuilder(StreamInput in)");

        this.wildcardQueryBuilder = new WildcardWrapper(in);
    }

    @Override
    protected void doWriteTo(StreamOutput out) throws IOException {
        out.writeString(wildcardQueryBuilder.fieldName());
        out.writeString(wildcardQueryBuilder.value());
        out.writeOptionalString(wildcardQueryBuilder.rewrite());
    }

    public String fieldName() {
        return wildcardQueryBuilder.fieldName();
    }

    public String value() {
        return wildcardQueryBuilder.value();
    }

    public TitaniamWildcardQueryBuilder rewrite(String rewrite) {
        this.wildcardQueryBuilder.rewrite(rewrite);
        return this;
    }

    public String rewrite() {
        return this.wildcardQueryBuilder.rewrite();
    }

    @Override
    public String getWriteableName() {
        return NAME;
    }

    @Override
    protected void doXContent(XContentBuilder builder, Params params) throws IOException {
        builder.startObject(NAME);
        builder.startObject(wildcardQueryBuilder.fieldName());
        builder.field(WILDCARD_FIELD.getPreferredName(), wildcardQueryBuilder.value());
        if (wildcardQueryBuilder.rewrite() != null) {
            builder.field(REWRITE_FIELD.getPreferredName(), wildcardQueryBuilder.rewrite());
        }
        printBoostAndQueryName(builder);
        builder.endObject();
        builder.endObject();
    }

    public static TitaniamWildcardQueryBuilder fromXContent(XContentParser parser) throws IOException {

        log.debug("fromXContent");

        WildcardQueryBuilder wildcardQueryBuilder = WildcardQueryBuilder.fromXContent(parser);

        return new TitaniamWildcardQueryBuilder(new WildcardWrapper(wildcardQueryBuilder));
    }

    @Override
    protected Query doToQuery(QueryShardContext context) throws IOException {

        log.debug("doToQuery");

        //Impl Note: This is a copy of WildcardQueryBuilder.doToQuery
        MappedFieldType fieldType = context.fieldMapper(wildcardQueryBuilder.fieldName());

        if(fieldType instanceof CryptoFieldType) {
            log.debug("delegating wildcard query generation to CryptoFieldType");
            return ((CryptoFieldType) fieldType).wildCardQuery(wildcardQueryBuilder.value(), null, context);

        } else {
            log.debug("default wildcard");
            return this.wildcardQueryBuilder.doToQuery(context);
        }
    }

    @Override
    protected int doHashCode() {
        return Objects.hash(wildcardQueryBuilder.fieldName(), wildcardQueryBuilder.value(), wildcardQueryBuilder.rewrite());
    }

    @Override
    protected boolean doEquals(TitaniamWildcardQueryBuilder other) {
        return Objects.equals(wildcardQueryBuilder.fieldName(), other.wildcardQueryBuilder.fieldName()) &&
                Objects.equals(wildcardQueryBuilder.value(), other.wildcardQueryBuilder.value()) &&
                Objects.equals(wildcardQueryBuilder.rewrite(), other.wildcardQueryBuilder.rewrite());
    }

}
