package com.titaniamlabs.es.ingest;

import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.crypto.CryptoResult;
import com.titaniamlabs.log.Log;
import org.elasticsearch.ingest.AbstractProcessor;
import org.elasticsearch.ingest.IngestDocument;

import javax.inject.Inject;
import java.util.List;

public class TitaniamProcessor extends AbstractProcessor {

    private static final Log log = Log.getLogger(TitaniamProcessor.class);

    public static final String TYPE = "titaniam_ingest_processor";
    public static final String FIELD = "field";

    private CryptoEngine cryptoEngine;

    @Inject
    public void setCryptoEngine(CryptoEngine cryptoEngine) {
        this.cryptoEngine = cryptoEngine;
    }

    private String[] fieldsToTangle;

    public TitaniamProcessor(String tag, List<String> tangleFieldsAsList) {
        super(tag);

        if(tangleFieldsAsList!=null) {
            fieldsToTangle = tangleFieldsAsList.toArray(new String[0]);
        } else {
            fieldsToTangle = new String[0];
        }
    }

    @Override
    public void execute(IngestDocument ingestDocument) {

        assert cryptoEngine != null : "Dependencies not initialized";

        if(ingestDocument == null) {
            log.debug("document is null. strange..");
            return;
        }

        for(String field : fieldsToTangle) {

            //TODO: How to you assert that the field indeed has only string class
            String clearText = ingestDocument.getFieldValue(field, String.class, true);

            //TODO: clearText will be null for nested documents.
            // For nested documents, the field then goes as-is, since
            // This part returns null.
            if(clearText!=null) {
                //TODO: handle return objects
                CryptoResult result = cryptoEngine.encrypt(clearText);

                log.debug("{}: {} => {}",field, clearText, result.getShuffled());

                ingestDocument.setFieldValue(field, result.getShuffled());
            }
        }
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
