package com.titaniamlabs.es.ingest;

import com.titaniamlabs.crypto.CryptoEngine;
import com.titaniamlabs.crypto.CryptoModule;
import org.elasticsearch.common.inject.Guice;
import org.elasticsearch.common.inject.Injector;
import org.elasticsearch.ingest.ConfigurationUtils;
import org.elasticsearch.ingest.Processor;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class ProcessorFactory implements Processor.Factory {

    private CryptoEngine cryptoEngine;

    @Inject
    public void setCryptoEngine(CryptoEngine cryptoEngine) {
        this.cryptoEngine = cryptoEngine;
    }

    @Override
    public Processor create(Map<String, Processor.Factory> processorFactories, String tag, Map<String, Object> config) throws Exception {

        List<String> fields = ConfigurationUtils.readList(TitaniamProcessor.TYPE, tag, config, TitaniamProcessor.FIELD);

        TitaniamProcessor processor = new TitaniamProcessor(tag, fields);
        processor.setCryptoEngine(cryptoEngine);

        return processor;
    }
}
