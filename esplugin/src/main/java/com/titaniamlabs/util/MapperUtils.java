package com.titaniamlabs.util;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Set of common semantic utility methods to avoid code
 * duplication across the board.
 *
 * This class has no dependency on any of the other packages
 * in any of titaniam code.
 */
public class MapperUtils {

    public static final String FORWARD_SUFFIX = "0";
    public static final String REV_SUFFIX = "rev";

    /**
     * Generates the name of the fragment - based on the naming convention
     * @param parentFieldName
     * @param suffix
     * @return
     */
    public static String generateIndexFragmentName(String parentFieldName, String suffix) {

        return parentFieldName + "_"+(suffix);
    }

    /**
     * Generates name with <code>FORWARD_SUFFIX</code>>
     * @param parentFieldName
     * @return
     */
    public static String generateForwardFieldName(String parentFieldName) {
        return generateIndexFragmentName(parentFieldName, FORWARD_SUFFIX);
    }


    /**
     * Generates name with <code>REV_SUFFIX</code>
     * @param parentFieldName
     * @return
     */
    public static String generateReverseFieldName(String parentFieldName) {
        return generateIndexFragmentName(parentFieldName, REV_SUFFIX);
    }

    /**
     * Returns true if the name ends with either of the <code>FORWARD_SUFFIX</code> or
     * <code>REV_SUFFIX</code>
     *
     * @param name
     * @return
     */
    public static boolean isForwardOrReverseName(String name) {
        return isForwardFieldName(name) || isReverseFieldName(name);
    }

    /**
     * Returns true if the fieldname like a forward field name.
     * @param name
     * @return
     */
    public static boolean isForwardFieldName(String name) {

        //Impl Note: Trying to avoid string concatenation.
        return name!=null &&
                (
                        (
                                (name.length() >FORWARD_SUFFIX.length()+1) &&
                                name.endsWith(FORWARD_SUFFIX) &&
                                (name.charAt(name.length() - FORWARD_SUFFIX.length()-1) == '_')
                        )
                );
    }

    /**
     * Returns true if the fieldname is like a reverse field name.
     * @param name
     * @return
     */
    public static boolean isReverseFieldName(String name) {

        //Impl Note: Trying to avoid string concatenation.
        return name!=null &&
                (
                        (
                                (name.length() > REV_SUFFIX.length()+1) &&
                                        name.endsWith(REV_SUFFIX) &&
                                        (name.charAt(name.length() - REV_SUFFIX.length()-1) == '_')
                        )

                );
    }


    /**
     * Given a string, it splits into groups of minChars
     * @param forwardText
     * @param jumpSize
     * @param splitSize
     * @return
     */
    public static String[] splitToNChar(String forwardText, int jumpSize, int splitSize) {

        if(Strings.isNullOrEmpty(forwardText)) {
            return null;
        }

        if(jumpSize<=0 || splitSize<=0) {
            return new String[]{forwardText};
        }

        List<String> parts = new ArrayList<>();

        int length = forwardText.length();
        for (int i = 0; i < length; i += jumpSize) {
            parts.add(forwardText.substring(i, Math.min(length, i + splitSize)));
        }
        return parts.toArray(new String[0]);
    }

    /**
     * Returns the lowercase version of the string, accounting
     * for null strings.
     *
     * @param input
     * @return
     */
    public static String toLowercase(String input) {
        return input!=null ? input.toLowerCase() : input;
    }


}
