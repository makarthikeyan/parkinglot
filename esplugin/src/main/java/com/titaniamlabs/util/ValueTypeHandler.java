package com.titaniamlabs.util;

import org.elasticsearch.search.internal.SearchContext;

import java.util.List;
import java.util.Map;

public interface ValueTypeHandler {

    public Map<String, Object> handleMap(Map<String, Object> source);

    public List<Object> handleList(String fieldName, List<Object> values);

    public Object handleSimpleValue(String fieldName, Object value);
}
