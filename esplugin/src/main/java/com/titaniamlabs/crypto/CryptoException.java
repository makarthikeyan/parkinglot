package com.titaniamlabs.crypto;

import com.titaniamlabs.log.Log;

/**
 * Represents an exception during the cryptographic functions.
 * This is an unchecked exception.
 */
public class CryptoException extends RuntimeException {

    private static final Log log = Log.getLogger(CryptoException.class);

    public CryptoException() {
    }

    public CryptoException(String message) {
        super(message);
    }

    public CryptoException(String message, Throwable cause) {
        super(message, cause);
    }

    public CryptoException(Throwable cause) {
        super(cause);
        log.error("CryptoException. Cause:",cause);
    }

    public CryptoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        log.error(message,cause);
    }
}
