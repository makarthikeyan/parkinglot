package com.titaniamlabs.crypto;

import com.titaniamlabs.api.ISearchApi;
import com.titaniamlabs.api.SearchApi;
import com.titaniamlabs.api.SearchType;
import com.titaniamlabs.engine.api.core.TitaniamApi;
import com.titaniamlabs.log.Log;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.inject.Inject;

import java.util.Arrays;

/**
 * Represents the native engine that is used for encryption/decryption.
 */
public class NativeCryptoEngine implements CryptoEngine {

    private static final Log log = Log.getLogger(NativeCryptoEngine.class);

    private ISearchApi searchApi;

    public NativeCryptoEngine() {
    }

    //Impl Note: @Inject annotation class must be from elasticsearch and not javax.
    //else, the elastic Guice will not inject dependency and silently fail.
    @Inject
    public void setSearchApi(ISearchApi searchApi) {

        log.debug("setting ISearchApi: {}", searchApi);

        this.searchApi = searchApi;
    }

    /**
     * Encrypts the input.
     *
     * @param clearText - the string to be tangled.
     * @return
     */
    @Override
    public CryptoResult encrypt(String clearText) {

        assert searchApi!=null : "NativeCryptoEngine not initialized";

        if(Strings.isNullOrEmpty(clearText)) {
            log.warn("CryptoResult - encrypt. Input null/empty.");
            return new CryptoResult("","","");
        }

        CryptoResult result = null;

        try {
            log.debug("entangle - api: {}", searchApi);

            String[] tangledValues = searchApi.entangle(clearText, true, true);

            if(tangledValues!=null && tangledValues.length==3) {

                result = new CryptoResult(tangledValues[0], tangledValues[1], tangledValues[2]);

                log.debug("entangle successful. {}", result);

            } else {

                log.error("entangle failed.");

                throw new CryptoException("Unexpected result from search api - "+Arrays.toString(tangledValues));
            }
        } catch (Exception e) {

            log.error("entangle failed.");

            throw new CryptoException(e);
        }

        return result;
    }

    @Override
    public String decrypt(String encryptedText, boolean shuffled) {

        try {

            log.debug("decrypt - input: {}", encryptedText);

            return searchApi.untangle(encryptedText, shuffled);

        } catch (Exception e) {

            throw new CryptoException(e);
        }
    }

    @Override
    public String[] encryptFor1toNPositions(String clearText, WildCardSearchType searchType, int nthPosition) {

        try {

            log.debug("generateEncryptedSearchTerms: {}, {}, {}", clearText, searchType, nthPosition);

            //TODO: nthPosition - is currently dropped. Need to provide support in the native engine.
            return searchApi.getWildCardSearchTerms(clearText, mapSearchType(searchType));

        } catch (Exception e) {

            throw new CryptoException(e);
        }
    }

    public SearchType mapSearchType(WildCardSearchType searchType) {

        switch (searchType) {
            case CONTAINS:
                return SearchType.Contains;
            case ENDS_WITH:
                return SearchType.EndsWith;
            case STARTS_WITH:
                return SearchType.StartsWith;
            default:
                throw new IllegalStateException("not expected");
        }

    }
}
