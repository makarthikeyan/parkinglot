package com.titaniamlabs.crypto;

/**
 * Interface that represents the basic cryptography functions of the
 * underlying plugin.
 */
public interface CryptoEngine {

    /**
     * Performs cryptography on the input clear text.
     * @param clearText - the string to be tangled.
     * @return tangled text that is no longer human readable.
     */
    public CryptoResult encrypt(String clearText);

    /**
     * Reverse cryptography, where it makes the non-human readable text into clear text.
     *
     * @param tangledText - non-human readable string.
     * @param shuffled
     * @return clear text
     */
    public String decrypt(String tangledText, boolean shuffled);

    /**
     * Given a cleartext and the search type, breaks the text into
     * encrypted parts.
     *
     * @param clearText
     * @param searchType
     * @param nthPosition - the N representing Nth position.
     * @return
     */
    public String[] encryptFor1toNPositions(String clearText, WildCardSearchType searchType, int nthPosition);
}
