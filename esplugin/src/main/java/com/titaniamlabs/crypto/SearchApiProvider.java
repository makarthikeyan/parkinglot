package com.titaniamlabs.crypto;

import com.titaniamlabs.api.ISearchApi;
import com.titaniamlabs.api.SearchApi;
import com.titaniamlabs.engine.api.core.TitaniamApi;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.inject.Provider;

/**
 * This is a workaround since SearchApi does not provide a default
 * constructor and requires TitaniamApi to instantiate.
 */
public class SearchApiProvider implements Provider<ISearchApi> {

    private TitaniamApi titaniamApi;

    @Override
    public ISearchApi get() {
        return new SearchApi(titaniamApi);
    }

    @Inject
    public void setTitaniamApi(TitaniamApi titaniamApi) {
        this.titaniamApi = titaniamApi;
    }
}
