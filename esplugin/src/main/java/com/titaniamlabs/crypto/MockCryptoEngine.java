package com.titaniamlabs.crypto;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Should be completely removed from the <code>src/main</code> and
 * moved to <code>src/test</code> once we figure out how to run
 * the JNI code on all dev machines.
 */
public class MockCryptoEngine implements CryptoEngine {

    public static final int MAX_FRAG_LENGTH = 3;

    /**
     * Does mock encryption. Takes every letter and replicates it
     * 6 times in the resulting string.
     *
     * @param clearText - the string to be tangled.
     * @return
     */
    @Override
    public CryptoResult encrypt(String clearText) {

        //abc
        if(Strings.isNullOrEmpty(clearText)) {
            return null;
        }

        StringBuilder builder = new StringBuilder();

        for (char c: clearText.toCharArray()) {

            for(int i=0;i<6;i++) {
                builder.append(c);
            }
        }

        //aaaaaabbbbbbcccccc
        //bbbbbbccccccdddddd
        //ccccccbbbbbbaaaaaa

        return new CryptoResult(builder.toString(), shuffle(builder.toString()), builder.reverse().toString());
    }

    /**
     * Picks every 6th char of the string and returns it.
     *
     * @param tangledText - non-human readable string.
     * @param shuffled
     * @return
     */
    @Override
    public String decrypt(String tangledText, boolean shuffled) {

        if(Strings.isNullOrEmpty(tangledText)) {
            return null;
        }

        if(tangledText.length() %6 !=0) {
            throw new CryptoException("Invalid format for the encrypted text: "+tangledText);
        }

        StringBuilder builder = new StringBuilder();

        for(int i=0; i<tangledText.length();i=i+6) {
            builder.append(tangledText.charAt(i));
        }

        return shuffled ? unshuffle(builder.toString()) : builder.toString();
    }

    @Override
    public String[] encryptFor1toNPositions(String clearText, WildCardSearchType searchType, int nthPosition) {

        if(Strings.isNullOrEmpty(clearText)) {
            return new String[0];
        }

        List<String> result = new ArrayList<>();

        //TODO: Magic number 32
        // In the mock implementation, the encrypted version is the same irrespective
        // of the position!!
        for(int i = 0; i< nthPosition; i++) {

            CryptoResult cryptoResult = encrypt(clearText);
            result.add(cryptoResult.getForward());
        }

        return result.toArray(new String[0]);
    }

    public String shuffle(String original) {

        StringBuilder builder = new StringBuilder();

        for(char c : original.toCharArray()) builder.append(++c);

        return builder.toString();
    }

    public String unshuffle(String original) {

        StringBuilder builder = new StringBuilder();

        for(char c : original.toCharArray()) builder.append(--c);

        return builder.toString();
    }
}
