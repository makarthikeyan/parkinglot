package com.titaniamlabs.crypto;

public class CryptoResult {

    private String forward;
    private String shuffled;
    private String reverse;

    public CryptoResult(String forward, String shuffled, String reverse) {
        this.forward = forward;
        this.shuffled = shuffled;
        this.reverse = reverse;
    }

    public String getForward() {
        return forward;
    }

    public String getShuffled() {
        return shuffled;
    }

    public String getReverse() {
        return reverse;
    }

    @Override
    public String toString() {
        return "CryptoResult{" +
                "forward='" + forward + '\'' +
                ", shuffled='" + shuffled + '\'' +
                ", reverse='" + reverse + '\'' +
                '}';
    }
}

