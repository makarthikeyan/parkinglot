package com.titaniamlabs.crypto;

/**
 * This class should not be here. Once we remove the dependency on
 * <code>searchType</code> as a parameter in <code>CryptoEngine</code>
 * interface.
 *
 * Should be moved to <code>es</code> package.
 */
public enum WildCardSearchType {

    STARTS_WITH,
    ENDS_WITH,
    CONTAINS,
    TERM
}
