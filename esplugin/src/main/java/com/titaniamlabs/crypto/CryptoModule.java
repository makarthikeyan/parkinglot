package com.titaniamlabs.crypto;

import com.titaniamlabs.api.ISearchApi;
import com.titaniamlabs.engine.api.core.ITitaniamApi;
import com.titaniamlabs.engine.api.core.TitaniamApi;
import com.titaniamlabs.log.Log;
import org.elasticsearch.common.inject.AbstractModule;

/**
 * Guice module for IOC.
 *
 * Using Guice is not effective in elastic. First 1) it
 * has lots of security conflicts when you run in actual
 * elastic env 2) Elasticsearch has copied Guice into it's
 * codebase and has inferior functionality.
 *
 * In general, IOC/DI has not been fully implemented in the
 * plugin.
 *
 * Next Steps: 1) Use another IOC framework OR 2) Make it work
 * independent of Elasticsearch.
 *
 * <code>crypto</code> package is a top level package that has
 * no direct dependency on elastic.
 */
public class CryptoModule extends AbstractModule {

    private static final Log log = Log.getLogger(CryptoModule.class);

    public static final String TITANIAM_CRYPTOENGINE = "titaniam.cryptoengine";

    @Override
    protected void configure() {

        log.debug("CryptoModule.configure");

        // Hack Alert! This needs to be changed back once JNI code works
        bind(ITitaniamApi.class).to(TitaniamApi.class);
        bind(ISearchApi.class).toProvider(SearchApiProvider.class);

        //TODO: This needs to be cleaned up. MockCryptoEngine should never be in the
        // prod code. Make a mock engine in native code that will be loaded for
        // testing purposes.
        if("mock".equalsIgnoreCase(System.getProperty(TITANIAM_CRYPTOENGINE))) {

            log.warn("configure - Using MockCryptoEngine");

            bind(CryptoEngine.class).to(MockCryptoEngine.class);

        } else {

            //This should be the default. In production, only
            // this should be seen. If you see MockCryptoEngine in prod,
            // stop and call 911 immediately!
            log.warn("configure - Using NativeCryptoEngine");

            bind(CryptoEngine.class).to(NativeCryptoEngine.class);
        }

    }
}
