package com.titaniamlabs.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.spi.AbstractLogger;

import java.io.*;
import java.nio.Buffer;

/**
 * Purely for debugging purposes only, when trying to test with elastic.
 * This works around the problem of controlling logging through elastic's own
 * settings.
 */
public class DirectFileLog {
//public class DirectFileLog extends AbstractLogger {
//
//    private String className;
//
//    private static final PrintStream printStream;
//
//    public static final String LOG_FILE = "/Users/karthik/dev/titanium/makarthikeyan/parkinglot/esplugin/logs/plugin.log";
//
//    static {
//
//        try {
//            FileOutputStream fos = new FileOutputStream(LOG_FILE, false);
//            printStream = new PrintStream(new BufferedOutputStream(fos));
//        } catch (FileNotFoundException e) {
//            throw new IllegalStateException(e);
//        }
//    }
//
//
//    public DirectFileLog(Class klass) {
//        this.className = klass.getName();
//    }
//
//    @Override
//    public String getName() {
//        return this.className;
//    }
//
//    @Override
//    public boolean isTraceEnabled() {
//        return true;
//    }
//
//    private void log(String level, String msg, Throwable t, Object...params) {
//
//        synchronized (printStream) {
//
//            printStream.print("[");
//            printStream.print(System.currentTimeMillis());
//            printStream.print("]");
//
//            printStream.print(" [");
//            printStream.print(Thread.currentThread().getName());
//            printStream.print("]");
//
//            printStream.print(" [");
//            printStream.print(level);
//
//            printStream.print(" - ");
//            printStream.print(this.className);
//            printStream.print("]");
//
//            if(msg!=null) {
//                printStream.print(msg);
//            }
//
//            if(t!=null) {
//                t.printStackTrace(printStream);
//            }
//
//            if(params!=null) {
//                for(int i=0; i<params.length;i++) {
//                    printStream.print(" [");
//                    printStream.print(i+1);
//                    printStream.print("]");
//                    printStream.print(String.valueOf(params[i]));
//                }
//            }
//
//            printStream.println();
//            printStream.flush();
//        }
//    }
//    @Override
//    public void trace(String msg) {
//        log("TRACE", msg, null) ;
//    }
//
//    @Override
//    public void trace(String format, Object arg) {
//        log("TRACE", format, null, arg) ;
//    }
//
//    @Override
//    public void trace(String format, Object arg1, Object arg2) {
//        log("TRACE", format, null, arg1, arg2) ;
//    }
//
//    @Override
//    public void trace(String format, Object... arguments) {
//        log("TRACE", format, null, arguments) ;
//    }
//
//    @Override
//    public void trace(String msg, Throwable t) {
//        log("TRACE", msg, t) ;
//    }
//
//    @Override
//    public boolean isTraceEnabled(Marker marker) {
//        return true;
//    }
//
//    @Override
//    public void trace(Marker marker, String msg) {
//    }
//
//    @Override
//    public void trace(Marker marker, String format, Object arg) {
//
//    }
//
//    @Override
//    public void trace(Marker marker, String format, Object arg1, Object arg2) {
//
//    }
//
//    @Override
//    public void trace(Marker marker, String format, Object... argArray) {
//
//    }
//
//    @Override
//    public void trace(Marker marker, String msg, Throwable t) {
//
//    }
//
//    @Override
//    public boolean isDebugEnabled() {
//        return true;
//    }
//
//    @Override
//    public void debug(String msg) {
//        log("DEBUG", msg, null) ;
//    }
//
//    @Override
//    public void debug(String format, Object arg) {
//        log("DEBUG", format, null, arg) ;
//    }
//
//    @Override
//    public void debug(String format, Object arg1, Object arg2) {
//        log("DEBUG", format, null, arg1, arg2) ;
//    }
//
//    @Override
//    public void debug(String format, Object... arguments) {
//        log("DEBUG", format, null, arguments) ;
//    }
//
//    @Override
//    public void debug(String msg, Throwable t) {
//        log("DEBUG", msg, null) ;
//    }
//
//    @Override
//    public boolean isDebugEnabled(Marker marker) {
//        return false;
//    }
//
//    @Override
//    public void debug(Marker marker, String msg) {
//
//    }
//
//    @Override
//    public void debug(Marker marker, String format, Object arg) {
//
//    }
//
//    @Override
//    public void debug(Marker marker, String format, Object arg1, Object arg2) {
//
//    }
//
//    @Override
//    public void debug(Marker marker, String format, Object... arguments) {
//
//    }
//
//    @Override
//    public void debug(Marker marker, String msg, Throwable t) {
//
//    }
//
//    @Override
//    public boolean isInfoEnabled() {
//        return true;
//    }
//
//    @Override
//    public void info(String msg) {
//        log("INFO", msg, null) ;
//    }
//
//    @Override
//    public void info(String format, Object arg) {
//        log("INFO", format, null, arg) ;
//    }
//
//    @Override
//    public void info(String format, Object arg1, Object arg2) {
//        log("INFO", format, null, arg1, arg2) ;
//    }
//
//    @Override
//    public void info(String format, Object... arguments) {
//        log("INFO", format, null, arguments) ;
//    }
//
//    @Override
//    public void info(String msg, Throwable t) {
//        log("INFO", msg, null) ;
//    }
//
//    @Override
//    public boolean isInfoEnabled(Marker marker) {
//        return false;
//    }
//
//    @Override
//    public void info(Marker marker, String msg) {
//
//    }
//
//    @Override
//    public void info(Marker marker, String format, Object arg) {
//
//    }
//
//    @Override
//    public void info(Marker marker, String format, Object arg1, Object arg2) {
//
//    }
//
//    @Override
//    public void info(Marker marker, String format, Object... arguments) {
//
//    }
//
//    @Override
//    public void info(Marker marker, String msg, Throwable t) {
//
//    }
//
//    @Override
//    public boolean isWarnEnabled() {
//        return true;
//    }
//
//    @Override
//    public void warn(String msg) {
//        log("WARN", msg, null) ;
//    }
//
//    @Override
//    public void warn(String format, Object arg) {
//        log("WARN", format, null, arg) ;
//    }
//
//    @Override
//    public void warn(String format, Object... arguments) {
//        log("WARN", format, null, arguments) ;
//    }
//
//    @Override
//    public void warn(String format, Object arg1, Object arg2) {
//        log("WARN", format, null, arg1, arg2) ;
//    }
//
//    @Override
//    public void warn(String msg, Throwable t) {
//        log("WARN", msg, t) ;
//    }
//
//    @Override
//    public boolean isWarnEnabled(Marker marker) {
//        return false;
//    }
//
//    @Override
//    public void warn(Marker marker, String msg) {
//
//    }
//
//    @Override
//    public void warn(Marker marker, String format, Object arg) {
//
//    }
//
//    @Override
//    public void warn(Marker marker, String format, Object arg1, Object arg2) {
//
//    }
//
//    @Override
//    public void warn(Marker marker, String format, Object... arguments) {
//
//    }
//
//    @Override
//    public void warn(Marker marker, String msg, Throwable t) {
//
//    }
//
//    @Override
//    public boolean isErrorEnabled() {
//        return true;
//    }
//
//    @Override
//    public void error(String msg) {
//        log("ERROR", msg, null) ;
//    }
//
//    @Override
//    public void error(String format, Object arg) {
//        log("ERROR", format, null, arg) ;
//    }
//
//    @Override
//    public void error(String format, Object arg1, Object arg2) {
//        log("ERROR", format, null, arg1, arg2) ;
//    }
//
//    @Override
//    public void error(String format, Object... arguments) {
//        log("ERROR", format, null, arguments) ;
//    }
//
//    @Override
//    public void error(String msg, Throwable t) {
//        log("ERROR", msg, t) ;
//    }
//
//    @Override
//    public boolean isErrorEnabled(Marker marker) {
//        return false;
//    }
//
//    @Override
//    public void error(Marker marker, String msg) {
//
//    }
//
//    @Override
//    public void error(Marker marker, String format, Object arg) {
//
//    }
//
//    @Override
//    public void error(Marker marker, String format, Object arg1, Object arg2) {
//
//    }
//
//    @Override
//    public void error(Marker marker, String format, Object... arguments) {
//
//    }
//
//    @Override
//    public void error(Marker marker, String msg, Throwable t) {
//
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, Message message, Throwable t) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, CharSequence message, Throwable t) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, Object message, Throwable t) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Throwable t) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object... params) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0, Object p1) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0, Object p1, Object p2) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8) {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled(Level level, Marker marker, String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9) {
//        return false;
//    }
//
//    @Override
//    public void logMessage(String fqcn, Level level, Marker marker, Message message, Throwable t) {
//
//    }
//
//    @Override
//    public Level getLevel() {
//        return null;
//    }
}
