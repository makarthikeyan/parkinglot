package com.titaniamlabs.log;


import org.apache.logging.log4j.LogManager;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;

/**
 * Simple wrapper over the underlying log mechanics. All classes should use this
 * to shield against the choice of underlying log implementation.
 *
 * Impl Note: slf4j is not integrating well with the log4j. We should move away
 * from log4j so that the plugin can independently control the logging -- or not.
 *
 */
public class Log {

    private Logger internalLogger;

    public static Log getLogger(Class klass) {

        Logger logger = LogManager.getLogger(klass);
//        Logger logger = new DirectFileLog(klass);
        return new Log(logger);
    }

    private Log(Logger internalLogger) {
        this.internalLogger = internalLogger;
    }

    /**
     *
     * @return True if logger is enabled to be at DEBUG level.
     */
    public boolean isDebugEnabled() {
        return internalLogger.isDebugEnabled();
    }

    public boolean isTraceEnabled() {
        return internalLogger.isTraceEnabled();
    }

    /* =================debug variants======================== */
    // Alert! elastic sets rootLogger to INFO. So, nothing in debug
    // will show up
    public void trace(Object msg) {
        this.internalLogger.trace(msg);
    }

    public void trace(String format, Object arg) {
        this.internalLogger.trace(format, arg);
    }

    public void trace(String format, Object arg1, Object arg2) {

        this.internalLogger.trace(format,arg1,arg2);
    }

    public void trace(String format, Object... arguments) {

        this.internalLogger.trace(format, arguments);
    }

    public void trace(String msg, Throwable t) {
        this.internalLogger.trace(msg, t);
    }
    /* =================debug variants======================== */
    // Alert! elastic sets rootLogger to INFO. So, nothing in debug
    // will show up
    public void debug(Object msg) {
        this.internalLogger.debug(msg);
    }

    public void debug(String format, Object arg) {
        this.internalLogger.debug(format, arg);
    }

    public void debug(String format, Object arg1, Object arg2) {

        this.internalLogger.debug(format,arg1,arg2);
    }

    public void debug(String format, Object... arguments) {

        this.internalLogger.debug(format, arguments);
    }

    public void debug(String msg, Throwable t) {
        this.internalLogger.debug(msg, t);
    }

    //=========== error variants =============//
    public void error(Object msg) {
        this.internalLogger.error(msg);
    }

    public void error(String format, Object arg) {
        this.internalLogger.error(format, arg);
    }

    public void error(String format, Object arg1, Object arg2) {

        this.internalLogger.error(format,arg1,arg2);
    }

    public void error(String format, Object... arguments) {

        this.internalLogger.error(format, arguments);
    }

    public void error(String msg, Throwable t) {
        this.internalLogger.error(msg, t);
    }

    //=========== warn variants =============//
    public void warn(Object msg) {
        this.internalLogger.warn(msg);
    }

    public void warn(String format, Object arg) {
        this.internalLogger.warn(format, arg);
    }

    public void warn(String format, Object arg1, Object arg2) {

        this.internalLogger.warn(format,arg1,arg2);
    }

    public void warn(String format, Object... arguments) {

        this.internalLogger.warn(format, arguments);
    }

    public void warn(String msg, Throwable t) {
        this.internalLogger.warn(msg, t);
    }

    //================ entry exit tracers===========//

    //TODO: Add other variants for info/warn as well as MDC
    public Serializable traceEntry(String format, Object...params) {
        return this.internalLogger.traceEntry(format, params);
    }

    public <R> R traceExit(R result) {
        return this.internalLogger.traceExit(result);
    }

}
