## Titaniam Elastic Plugin
* This repo includes the titaniam elastic plugin code.
* Elasticsearch version supported: 6.2.2

## Getting Started
1. Checkout the tree
1. Build dependencies - checkout `papaya-java-api` 
   1. install [apache maven](https://maven.apache.org/install.html)
   1. <code>cd TitaniamApi</code>
      * <code>mvn install</code> under the TitaniamApi directory
   1. <code>cd TitaniamCoreApi</code>
      * <code>mvn install</code> under the TitaniamCoreApi directory
1. Unit-tests: <code>./gradlew clean test</code>
1. Integration test with embedded elastic: <code>./gradlew dumbledore</code>

## Code organization
* <code>src/main</code>: production code
* <code>src/test</code>: unit tests
* <code>src/func-test</code>: Functional/Integration tests

## Build script
* Familiarize with the <code>build.gradle</code>
* It contains custom tasks for creating plugin jar as well as plugin zip

### Building for deployment
* `./gradlew dumbledore` - will build and test the build.
* once successful, pickup the deployable plugin zip file from `build/dist/`

## Functional Testing
* TODO: Specify java versions supported
* TODO: Include instructions for building dependent projects 
* Refer to the documentation [here](src/func-test/resources/README.md)

## TODOs
1. **[done]** `TitaniamFetchSubPhase` - does not take care of nested json structure.
1. **[done]** Support for nested documents in `TitaniamProcessor` as well as `TitaniamSearchSubPhase`
1. Deployable plugin, that includes the native libs and other artifacts
1. Static code analysis/code support to ensure sensitive fields are never logged in production code.
1. Remove <code>MockCryptoEngine</code> from functional testing
1. Rename <code>type</code> to <code>tangled_keyword</code> from <code>tangled_text</code>
1. Remove 32 character limit
1. Remove 6 & 18 limit specification for the <code>index_prefix</code>
1. Wildcard - getSearchTerms JNI code to take maxChars as parameter, so that we can search for occurrences beyond 32.
1. writeLogs - for gathering deep usage logs
1. Tech - remove ES Guice and replace it with neutral IOC  
